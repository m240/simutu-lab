<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Landing Route


Route::prefix('simutu')->group(function () {
    // prefix for deploy
});

//Testing route hanya untuk debug
Route::prefix('test')->group(function () {
    Route::get('ilab_user', 'TestController@testIlabUser');
    Route::get('class_category', 'TestController@classCategory');
    Route::get('active_class', 'TestController@activeClass');
});

Route::get('/', 'Praktikkan\Auth\LoginController@index')->name('landing');

// Admin Route
Route::prefix('adm1n')->namespace('Admin')->group(function () {

    Route::prefix('auth')->namespace('Auth')->group(function () {
        Route::get('logout', 'LoginController@logout')->name('adm1n.logout');
        Route::get('login', 'LoginController@index')->name('adm1n.login.get');
        Route::post('login', 'LoginController@login')->name('adm1n.login.post');
    });

    Route::prefix('dashboard')->middleware('auth.admin')->group(function () {
        //Route admin dashboard isi disini
        Route::get('', 'DashboardController@index')->name('adm1n.dashboard.index');
        Route::get('/setting', 'DashboardController@setting')->name('adm1n.dashboard.setting');
        Route::post('/setting', 'DashboardController@setting_submit')->name('adm1n.setting.update_info');
        Route::post('/password', 'DashboardController@password_submit')->name('adm1n.update.password');

        //Periode start
        Route::get('periode', 'PeriodePenilaianController@index')->name('adm1n_dashboard_periode_idx');
        Route::post('/periode/aktifkan', 'PeriodePenilaianController@aktif')->name('adm1n_dashboard_periode_aktifkan');
        Route::post('/periode/nonaktifkan', 'PeriodePenilaianController@nonAktif')->name('adm1n_dashboard_periode_nonaktifkan');
        Route::post('/periode/hapus', 'PeriodePenilaianController@hapus')->name('adm1n_dashboard_periode_hapus');
        Route::post('/periode/tambah', 'PeriodePenilaianController@tambah')->name('adm1n_dashboard_periode_tambah');
        Route::post('/periode/edit', 'PeriodePenilaianController@edit')->name('adm1n_dashboard_periode_edit');
        Route::post('/periode/non-aktif-sebagian', 'PeriodePenilaianController@nonAktifSebagian')->name('adm1n_nonaktif_sebagian_periode');
        Route::post('/periode/aktif-sebagian', 'PeriodePenilaianController@aktifSebagian')->name('adm1n_aktif_sebagian_periode');
        Route::get('/periode/temp-comments/{id}', 'PeriodePenilaianController@get_temp_comment')->name('adm1n_periode_temp_comments');
        //Periode end

        //Kriteria start
        Route::get('kriteria', 'KriteriaController@index')->name('adm1n_dashboard_kriteria');
        Route::post('kriteria/tambah', 'KriteriaController@tambah')->name('adm1n_dashboard_kriteria_tambah');
        Route::post('kriteria/{id}/edit', 'KriteriaController@edit')->name('adm1n_dashboard_kriteria_edit_idx');
        Route::post('kriteria/hapus', 'KriteriaController@hapus')->name('adm1n_dashboard_kriteria_hapus');
        //Kriteria end

        //Detail Kriteria start
        Route::get('kriteria/detail/{id}', 'DetailKriteriaController@index')->name('admin_detail_kriteria_idx');
        Route::post('kriteria/detail/{id}/tambah', 'DetailKriteriaController@tambah')->name('adm1n_tambah_detail_kriteria');
        Route::post('kriteria/detail/{id}/edit', 'DetailKriteriaController@edit')->name('adm1n_edit_detail_kriteria');
        Route::post('kriteria/detail/hapus', 'DetailKriteriaController@hapus')->name('adm1n_dashboard_kriteria_detail_hapus');
        //Detail Kriteria end

        //Predikat start
        Route::get('predikat', 'PredikatController@idx')->name('adm1n_dashboard_predikat');
        Route::post('predikat/tambah', 'PredikatController@tambah')->name('adm1n_dashboard_predikat_tambah');
        Route::post('predikat/{id}/edit', 'PredikatController@edit')->name('admin_edit_predikat_idx');
        Route::post('predikat/hapus', 'PredikatController@hapus')->name('adm1n_dashboard_predikat_hapus');
        //predikat end

        //Detail Predikat start
        Route::get('predikat/detail/{id}', 'DetailPredikatController@index_detail')->name('predikat_detail_byId');
        Route::get('predikat/detail/{id}/tambah', 'DetailPredikatController@tambah_idx')->name('adm1n_predikat_tambah');
        Route::post('predikat/detail/{id}/tambah/simpan', 'DetailPredikatController@tambah_detail')->name('adm1n_tambah_detail_predikat_ok');
        Route::get('predikat/detail/{id_P}/edit/{id_C}', 'DetailPredikatController@edit_idx')->name('adm1n_predikat_detail_edit_idx');
        Route::post('predikat/detail/{id}/edit/{id_C}/simpan', 'DetailPredikatController@edit_detail')->name('adm1n_simpan_edit_detail_predikat');
        Route::post('predikat/detail/hapus', 'DetailPredikatController@hapus_detail')->name('adm1n_dashboard_detail_predikat_hapus');
        // Detail Predikat end
    });

    Route::prefix('feedback')->middleware('auth.admin')->group(function () {
        Route::get('proses/{id}', 'PeriodePenilaianController@processPeriodePenilaian')->name('proses_periode_penilaian');
        
        Route::get('modul', 'FeedbackModulController@index')->name('admin_feedback_modul_idx');
        Route::get('modul/{semester}/detail', 'FeedbackModulController@idx')->name('admin_feedback_modul');
        Route::get('modul/{semester}/{id}/detail', 'FeedbackModulController@detail')->name('admin_feedback_modul_detail');


        Route::get('asisten-instruktur', 'FeedbackAsistenInstrukturController@idx')->name('admin_feedback_asisten_instruktur_idx');
        Route::get('asisten-instruktur/{id}/detail', 'FeedbackAsistenInstrukturController@detail')->name('admin_detail_feedback_asisten_instruktur_idx');
        Route::get('asisten-instruktur/{id}/detail/{usr}', 'FeedbackAsistenInstrukturController@detailuser')->name('admin_detail_feedback_asisten_instruktur');
        Route::get('fasilitas-lab', 'FeedbackFasilabController@idx')->name('admin_feedback_fasilab_idx');
        Route::get('fasilitas-lab/{id}/detail', 'FeedbackFasilabController@detail')->name('admin_feedback_fasilab_detail');
    });

    Route::prefix('evaluasi')->middleware('auth.admin')->group(function () {
        // EVALUASI PRAKTIKUM START
        Route::get('praktikum', 'EvaluasiPraktikumController@index')->name('admin_evaluasi_praktikum');
        Route::post('praktikum/{id}/semester/export', 'EvaluasiPraktikumController@semesterExport')->name('admin_evaluasi_praktikum_semester_export');
        Route::get('praktikum/{id}/semester', 'EvaluasiPraktikumController@semester')->name('admin_evaluasi_praktikum_semester');
        Route::get('praktikum/{id}/semester/{idcategory}/category', 'EvaluasiPraktikumController@category')->name('admin_evaluasi_praktikum_semester_category');
        Route::get('praktikum/{id}/semester/{idcategory}/category/{idclass}/classes', 'EvaluasiPraktikumController@classes')->name('admin_evaluasi_praktikum_semester_category_class');
        // EVALUASI PRAKTIKUM END

        // EVALUASI MODUL START
        Route::get('modul', 'EvaluasiModulController@index')->name('admin_evaluasi_modul');
        Route::get('modul/{id}/semester', 'EvaluasiModulController@semester')->name('admin_evaluasi_modul_semester');
        Route::get('modul/{id}/semester/{idcategory}/category', 'EvaluasiModulController@category')->name('admin_evaluasi_modul_semester_category');
        Route::post('modul/export', 'EvaluasiModulController@exportModul')->name('admin_evaluasi_modul_export');
        // EVALUASI PRAKTIKUM END

        // EVALUASI KELAS START
        Route::get('/kelas', 'EvaluasiKelasController@index')->name('admin_evaluasi_kelas_index');
        Route::post('/kelas', 'EvaluasiKelasController@filter')->name('admin_evaluasi_kelas_index_filter');
        Route::get('/kelas/{id}', 'EvaluasiKelasController@post')->name('admin_evaluasi_kelas_post');
        Route::POST('/kelas/export', 'EvaluasiKelasController@export')->name('admin_evaluasi_kelas_export');
        // EVALUASI KELAS END

        // EVALUASI SEMESTER START
        Route::get('/semester', 'EvaluasiSemesterController@index')->name('admin_evaluasi_semester_index');
        Route::post('/semester', 'EvaluasiSemesterController@filter')->name('admin_evaluasi_semester_filter');
        Route::post('/semester/export', 'EvaluasiSemesterController@export')->name('admin_evaluasi_semester_export');
        // EVALUASI SEMESTER END
    });

    Route::prefix('praktikum')->middleware('auth.admin')->group(function () {
        Route::get('/periode', 'PraktikumAsistenController@index')->name('admin_praktikum_periode');
        Route::get('/periode/{id}', 'PraktikumAsistenController@show')->name('admin_praktikum_periode_detail');
        Route::get('/periode/{id}/asisten/{idasisten}/user/{iduser}', 'PraktikumAsistenController@detail')->name('admin_praktikum_periode_detail_asisten');
        Route::post('/allassisten/export', 'PraktikumAsistenController@exportAllAssistant')->name('admin_praktikum_allasisten');
        Route::post('/detailassisten/export', 'PraktikumAsistenController@exportDetailAssistant')->name('admin_praktikum_detailasisten');

        Route::get('instruktur', 'PraktikumInstrukturController@index')->name('instruktur_index');
        Route::get('instruktur/{id}', 'PraktikumInstrukturController@detail')->name('instruktur_detail');
        Route::get('instruktur/detail/{idclass}/instructor/{ins}/user/{id}', 'PraktikumInstrukturController@instructordetail')->name('instruktur_user_detail');
        Route::post('/allinstruktur/export', 'PraktikumInstrukturController@exportallInstruktur')->name('exportallInstruktur');
        Route::post('/detailinstruktur/export', 'PraktikumInstrukturController@exportDetailInstructor')->name('exportdetailnstruktur');
    });

    Route::prefix('praktikum/online')->middleware('auth.admin')->group(function () {
        Route::get('/', 'PraktikumOnlineController@index')->name('admin_praktikum_online');
        Route::get('/category/{id}', 'PraktikumOnlineController@category_list')->name('admin_praktikum_online_class');
        Route::get('/list-class/{id}', 'PraktikumOnlineController@class_list')->name('admin_praktikum_online_class_list');
        Route::get('/recap-class/{id}', 'PraktikumOnlineController@generate_recap')->name('admin_praktikum_online_class_recap');
        Route::get('/list-class/{id_sem}/task/{id_class}', 'PraktikumOnlineController@class_list_task')->name('admin_praktikum_online_class_task');
    });
});

// Instruktur / Asisten Routes
Route::prefix('instruktur')->group(function () {

    Route::prefix('auth')->namespace('Auth')->group(function () { });

    // silahkan buat kernel, config, dan auth guard khusus instruktur / asisten
    Route::prefix('dashboard')->middleware('')->group(function () { });
});

// User Routes
Route::prefix('frontend')->namespace('Praktikkan')->group(function () {

    Route::prefix('auth')->namespace('Auth')->group(function () {
        Route::get('logout', 'LoginController@logout')->name('praktikkan.logout');
        Route::post('login', 'LoginController@login')->name('praktikkan.login.post');
    });

    // silahkan buat kernel, config, dan auth guard khusus praktikkan
    Route::prefix('dashboard')->middleware('auth')->group(function () {
        Route::get('', 'DashboardController@index')->name('praktikkan.dashboard.index');
        Route::get('print_bukti', 'DashboardController@print_bukti')->name('praktikkan.dashboard.bukti');
    });

    //Feedback modul disini
    Route::prefix('feedback/modul')->middleware('auth')->group(function () {
        Route::get('', 'FeedbackModulController@index')->name('praktikan_feedback_modul_idx');
        Route::get('detail/predikat/{id_predikat}/', 'FeedbackModulController@detail_feedback_modul_idx')->name('praktikan_isi_feedback_modul_idx');
        Route::post('detail/predikat/{id_predikat}/tambah', 'FeedbackModulController@tambah_detail_feedback_modul')->name('praktikan_isi_feedback_modul_simpan');
    });

    Route::prefix('feedback/asisten')->middleware('auth')->group(function () {
        Route::get('', 'FeedbackAsistenController@index')->name('praktikan_feedback_asisten_idx');
        Route::get('isi-feedback/{id_class}', 'FeedbackAsistenController@form_kriteria_penilaian')->name('praktikan_isi_feedback_asisten');
        Route::post('isi-penilaian/{id_class}', 'FeedbackAsistenController@submit_form_penilaian')->name('submit_form_penilaian_asisten');
    });

    Route::prefix('feedback/instruktur')->middleware('auth')->group(function () {
        Route::get('', 'FeedbackInstrukturController@index')->name('praktikan_feedback_instruktur_idx');
        Route::get('detail/{id_class}/{id_instruktur}/', 'FeedbackInstrukturController@form_kriteria_penilaian')->name('praktikan_feedback_instruktur_form');
        Route::post('isi-penilaian/{id_class}/{id_instruktur}', 'FeedbackInstrukturController@submit_form_penilaian')->name('praktikan_feedback_instruktur_form_submit');
    });

    Route::prefix('feedback/lab')->middleware('auth')->group(function () {
        Route::get('', 'FeedbackLabController@form_kriteria_penilaian')->name('praktikan_feedback_lab_idx');
        Route::post('isi-penilaian', 'FeedbackLabController@submit_form_penilaian')->name('praktikan_feedback_lab_submit');
    });

    Route::prefix('perform/feedback')->middleware('auth')->group(function () {
        Route::get('', 'FeedbackFrontController@index')->name('frontend_feedback_index');
        Route::get('detail-feedback/{id_periode}', 'FeedbackFrontController@detail_feedback')->name('frontend_feedback_detail');
    });
});
