var isNotProcessingStatus = true;
$(document).on("click", ".btn-status-nonaktif", function () {
    if (isNotProcessingStatus) {
        isNotProcessingStatus = false;
        $(this).next().toggle();
        $(this).prev().hide();
        $(this).hide();
        data = $(this).data("id");
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
        $.ajax({
            type: "post",
            url: route.active,
            data: {
                id: $(this).data("id"),
            },
            success: function () {
                aktifToNon(data);
            },
        });
    }
});

function aktifToNon(id) {
    $("#kolom_status" + id)
        .find("div")
        .remove();
    $("#kolom_status" + id).append(
        '<div class="row justify-content-center"><div class="btn-group btn-group-sm" role="group" aria-label="Small"><button type="button" class="btn btn-outline-success btn-status-aktif" data-id="' +
            id +
            '">Yes</button> <div class="spinner-border spinner-border-sm text-primary" role="status" style="display: none;"> <span class="sr-only">Loading...</span> </div> <button type="button" class="btn btn-danger">No</button> </div> </div>'
    );
    isNotProcessingStatus = true;
}

$(document).on("click", ".btn-status-aktif", function () {
    if (isNotProcessingStatus) {
        isNotProcessingStatus = false;
        $(this).next().toggle();
        $(this).next().next().hide();
        $(this).hide();
        data = $(this).data("id");
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
        $.ajax({
            type: "post",
            url: route.nonactive,
            data: {
                id: $(this).data("id"),
            },
            success: function () {
                // nonToAktif(data);
                location.reload();
            },
        });
    }
});

function nonToAktif(id) {
    $("#kolom_status" + id)
        .find("div")
        .remove();
    $("#kolom_status" + id).append(
        '<div class="row justify-content-center"> <div class="btn-group btn-group-sm" role="group" aria-label="Small"> <button type="button" class="btn btn-success">Yes</button> <button type="button" class="btn btn-outline-danger btn-status-nonaktif" data-id="' +
            id +
            '">No</button> <div class="spinner-border spinner-border-sm text-primary" role="status" style="display: none;"> <span class="sr-only">Loading...</span> </div> </div> </div>'
    );
    isNotProcessingStatus = true;
}

$(".btn-hapus-periode").on("click", function () {
    data = $(this).data("id");
    Swal.fire({
        title: "Yakin ingin menghapus periode?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        cancelButtonColor: "#3085d6",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Batal",
    }).then((result) => {
        if (result.value) {
            One.loader("show");
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    ),
                },
            });
            $.ajax({
                type: "post",
                url: route.delete,
                data: {
                    id: $(this).data("id"),
                },
                success: function () {
                    afterDelete();
                },
            });
        }
    });
});

$("#form-tambah-periode").on("submit", function () {
    if (isNaN($("#tahun_tambah").val())) {
        event.preventDefault();
        $("#err_tahun_tambah").show();
    } else {
        $("#err_tahun_tambah").hide();
    }
});

$("#modal-edit-periode").on("show.bs.modal", function (event) {
    var button = $(event.relatedTarget);
    var modal = $(this);
    modal.find("#id").val(button.data("id"));
    modal.find("#nama").val(button.data("nama"));
    modal.find("#tahun_edit").val(button.data("tahun"));
    var semester = button.data("semester");
    if (semester == "ganjil") {
        document.getElementById("ganjil_edit").checked = true;
        document.getElementById("genap_edit").checked = false;
    } else {
        document.getElementById("genap_edit").checked = true;
        document.getElementById("ganjil_edit").checked = false;
    }
    var arrJenis = ["asisten", "instruktur", "fasil_lab", "modul"];
    var selectedJenis = arrJenis.indexOf(button.data("jenis"));
    document.getElementById("jenis_penilaianE").selectedIndex = selectedJenis;
    $("#kriteriaE").val(button.data("kritid"));
    $("#select2-kriteriaE-container").children().html(button.data("kritnama"));
    if (selectedJenis == "0" || selectedJenis == "1") {
        $("#predikatE").val(button.data("predid"));
        $("#select2-predikatE-container")
            .children()
            .html(button.data("prednama"));
        $("#blok_predikat_tambahE").show();
        $("#blok_classCategory_tambahE").hide();
    } else if (selectedJenis == "3") {
        $("#classcategoryE").val(button.data("clasid"));
        $("#select2-classcategoryE-container")
            .children()
            .html(button.data("clasnama"));
        $("#blok_classCategory_tambahE").show();
        $("#blok_predikat_tambahE").hide();
    } else {
        $("#blok_classCategory_tambahE").hide();
        $("#blok_predikat_tambahE").hide();
    }
});

$("#form-edit-periode").on("submit", function () {
    if (isNaN($("#tahun_edit").val())) {
        event.preventDefault();
        $("#err_tahun_edit").show();
    } else {
        $("#err_tahun_edit").hide();
    }
});

$(".btn-proc").on("click", function () {
    event.preventDefault();
    Swal.fire({
        title: "Yakin Memproses Periode Penilaian?",
        text: "Setelah Diproses Tidak Dapat Dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Lanjutkan",
        cancelButtonText: "Batal",
    }).then((result) => {
        if (result.value) {
            One.loader("show");
            window.location.replace($(this).attr("href"));
        }
    });
});

$("#non-aktif-sekumpulan").on("click", function () {
    if ($("input[name^='cek_row']:checked").length == 0) {
        Swal.fire("Belum ada periode penilaian yang dipilih!", "", "error");
    } else {
        let chekced = $("input[name^='cek_row']:checked");
        let idx = [];
        for (let i = 0; i < chekced.length; i++) {
            idx.push($(chekced[i]).data("id"));
        }
        One.loader("show");
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
        $.ajax({
            type: "post",
            url: route.nonactive_sebagian,
            data: {
                data: idx,
            },
            success: function () {
                afterDelete();
            },
        });
    }
});

$("#aktif-sekumpulan").on("click", function () {
    if ($("input[name^='cek_row']:checked").length == 0) {
        Swal.fire("Belum ada periode penilaian yang dipilih!", "", "error");
    } else {
        let chekced = $("input[name^='cek_row']:checked");
        let idx = [];
        for (let i = 0; i < chekced.length; i++) {
            idx.push($(chekced[i]).data("id"));
        }
        One.loader("show");
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
        $.ajax({
            type: "post",
            url: route.active_sebagian,
            data: {
                data: idx,
            },
            success: function () {
                afterDelete();
            },
        });
    }
});
