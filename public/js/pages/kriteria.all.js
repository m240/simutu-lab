$(document).on('click','#btn-tambah-kriteria',function(){
    if( persen < 1){
        Swal.fire(
            'Persentase Sudah Mencapai 100',
            'Tidak Dapat Menambahkan Kriteria.',
            'error'
        )
    }else{
        $('#modal-tambah-kriteria').modal('show');
    }
});

$(".btn-hapus-kriteria").on(
"click",
function () {
    data = $(this).data('id')
    $('#id_form_delete_kriteria_here').val(data)
    Swal.fire({
    title: 'Yakin ingin menghapus kriteria?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#d33',
    cancelButtonColor: '#3085d6',
    confirmButtonText: 'Ya, hapus!',
    cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.value) {
            One.loader('show')
            document.getElementById('form_delete_kriteria_here').submit()
        }else{
            event.preventDefault()
        }
    })
    }
);


