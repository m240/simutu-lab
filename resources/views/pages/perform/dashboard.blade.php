@extends('layouts.praktikkan')

@section('title-page', 'Dashboard')

@section('css_before')
<link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick.css') }}">
<link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick-theme.css') }}">
@endsection

@section('content')
<!-- Hero -->

@if (session('status'))
<div class="alert alert-warning alert-dismissable" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">×</span>
</button>
<div class="font-size-sm font-w600 text-uppercase text-muted">{{ session('status') }}</div>
</div>
@endif

<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Dashboard</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="">Dashboard</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<div class="block mx-3">
    <div class="bg-image overflow-hidden" style="background-image: url('{{'https://infotech.umm.ac.id/assets/frontside/images/sliders/slide_2.png'}}');">
    <div class="bg-primary-dark-op" style="height: 250px;">
        <div class="content content-narrow content-full">
            <div
                class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h1 class="font-w600 text-white mb-0 invisible" data-toggle="appear">Dashboard</h1>
                    <h2 class="h4 font-w400 text-white-75 mb-0 invisible" data-toggle="appear" data-timeout="250">
                       Sistem Informasi Manajemen Mutu Laboratorium Informatika
                    </h2>
                </div>
            </div>            
        </div>
    </div>
</div>
</div>

<div class="content content-narrow">
    
</div>
<!-- END Page Content -->
@endsection

@section('js_after')
<script src="{{asset('js/plugins/chart.js/Chart.bundle.min.js')}}"></script>
<script src="{{asset('js/plugins/slick-carousel/slick.min.js')}}"></script>
@endsection