@extends('layouts.praktikkan')

@section('title-page', 'Feedback Praktikum')

@section('css_before')
<link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick.css') }}">
<link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick-theme.css') }}">
<link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
@endsection

@section('content')
<!-- Hero -->

@if (session('status'))
<div class="alert alert-warning alert-dismissable" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <div class="font-size-sm font-w600 text-uppercase text-muted">{{ session('status') }}</div>
</div>
@endif

<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Feedback Praktikum</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="">Feedback</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<!-- Dynamic Table Periode Full Pagination -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title">Periode Penilaian</h3>
    </div>
    <div class="block-content block-content-full">
        <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
            <thead>
                <tr>
                    <th class="text-center" style="width: 5%;">No</th>
                    <th class="d-sm-table-cell" style="width: 50%;">Nama Periode</th>
                    <th class="d-sm-table-cell" style="width: 15%;">Semester</th>
                    <th class="d-sm-table-cell" style="width: 15%;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                ?>
                @foreach ($data as $d)
                <tr>
                    <td class="text-center font-size-sm">{{$i}}</td>
                    <td class="font-w600 font-size-sm">
                        {{$d->nama_penilaian}}
                    </td>
                    <td class="font-w600 font-size-sm">
                        {{ucfirst($d->semester)}}
                    </td>
                    <td>
                        <div class="row justify-content-center">
                            <a type="button" href="{{route('frontend_feedback_detail',['id_periode'=>$d->id])}}" class="btn btn-sm btn-rounded btn-secondary" data-toggle="tooltip" data-placement="top" data-original-title="List {{$d->jenis_penilaians == "asisten"? "Asisten":"Instruktur"}}">
                                <i class="fa fa-arrow-right text-light"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END Dynamic Table Full Pagination -->
@endsection

@section('js_after')
<script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
<script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
@if(Session::get('berhasil'))
<script>
    Swal.fire(
        '{{Session::get('
        berhasil ')}}',
        '',
        'success'
    )
</script>
@endif
@endsection