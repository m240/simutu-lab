@extends('layouts.praktikkan')

@section('title-page', 'Feedback Praktikum')

@section('css_before')
<link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick.css') }}">
<link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick-theme.css') }}">
<link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
@endsection

@section('content')
<!-- Hero -->

@if (session('status'))
<div class="alert alert-warning alert-dismissable" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <div class="font-size-sm font-w600 text-uppercase text-muted">{{ session('status') }}</div>
</div>
@endif


<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Feedback
                {{$data['periode']->jenis_penilaians == "asisten" ? "Asisten" : "Instruktur"}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">Performa
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx"
                            href="{{route('frontend_feedback_index')}}">Feedback</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx"
                            href="#">Detail Feedback</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>


<div class="content content-full">
    <div class="row">
        <div class="col-md-6">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Penilaian yang didapat</h3>
                </div>
                <div class="block-content">
                    <p>Nilai Akumulasi: {{$data['data_user_proc']->total_nilai_user}}</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Predikat</h3>
                </div>
                <div class="block-content">
                    <p><img class="img-avatar img-avatar-thumb" id="view" src="{{asset($data['predikat']->logo)}}"> {{$data['predikat']->nama_predikat}}</p>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Visualisasi Penilaian</h3>
                </div>
                <div class="block-content">
                    <canvas id="myPieChart" height="100"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Dynamic Table Periode Full Pagination -->
<div class="block">
    <div class="block-header">
        <div class="d-block">
            <h3 class="block-title">Detail Kriteria Penilaian
                {{$data['periode']->jenis_penilaians == "asisten" ? " Asisten" : " Instruktur"}}</h3>
        </div>
        <button class="btn btn-primary btn-sm"
            onclick="exportTableToCSV('Detail Kriteria {{$data['periode']->nama_penilaian}}.csv','tabel_kriteria')">Export
            CSV</button>
    </div>
    <div class="block-content block-content-full">
        <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
        <table id="tabel_kriteria"
            class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
            <thead>
                <tr>
                    <th class="text-center" style="width: 5%;">No</th>
                    <th class="d-sm-table-cell" style="width: 50%;">Deskripsi Kriteria</th>
                    <th class="d-sm-table-cell" style="width: 15%;">Persentase</th>
                    <th class="d-sm-table-cell" style="width: 15%;">Rata-Rata Nilai</th>
                    <th class="d-sm-table-cell" style="width: 15%">Nilai</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0;
                ?>
                @foreach ($data['detail_kriteria'] as $d)
                <tr>
                    <td class="text-center font-size-sm">{{$i+1}}</td>
                    <td class="font-w600 font-size-sm">
                        {{$d->desc}}
                    </td>
                    <td class="font-w600 font-size-sm ">
                        <div class="justify-content-center row">
                            {{$d->persentase}}
                        </div>
                    </td>
                    <td class="font-w600 font-size-sm">
                        <div class="justify-content-center row">
                            {{$data['nilai'][$i]}}
                        </div>
                    </td>
                    <td class="font-w600 font-size-sm">
                        <div class="justify-content-center row">
                            {{100*$data['nilai'][$i]/$d->persentase}}
                        </div>
                    </td>
                </tr>
                <?php
                    $i++;
                ?>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END Dynamic Table Full Pagination -->

<!-- Dynamic Table Periode Full Pagination -->
<div class="block shadow">
    <div class="block-header">
        <div class="d-block">
            <h3 class="block-title">Kritik Saran</h3>
        </div>
        <button class="btn btn-primary btn-sm"
            onclick="exportTableToCSV('Kritik Saran {{$data['periode']->nama_penilaian}}.csv','tabel_msg')">Export
            CSV</button>
    </div>
    <div class="block-content block-content-full">
        <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
        <table id="tabel_msg" class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
            <thead>
                <tr>
                    <th class="text-center" style="width: 5%;">No</th>
                    <th class="d-sm-table-cell" style="width: 15%;">Foto</th>
                    <th class="d-sm-table-cell" style="width: 80%;">Komentar</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0;
                ?>
                @foreach ($data['msg'] as $d)
                <tr>
                    <td class="text-center font-size-sm">{{$i+1}}</td>
                    <td class="font-w600 font-size-sm kolom-foto">
                        <div class="justify-content-center row">
                            <img class="img-avatar" style="width:50px; height:auto;" src="{{$data['user'][$i]}}"
                                alt="Foto User" height="50">
                            <p style="display: none;">{{$data['nim'][$i]}}</p>
                        </div>
                    </td>
                    <td class="font-w600 font-size-sm">
                        {{$d->message}}
                    </td>
                </tr>

                <?php
                    $i++;
                ?>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('js_after')
<script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
<script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('js/plugins/chart.js/Chart.min.js')}}"></script>
<script>
        var ctx = document.getElementById('myPieChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels : @json($desc_vis),
                datasets: [{
                    label: 'Perolehan Nilai',
                    data: @json($nilai_vis),
                    backgroundColor:'rgba(255, 206, 86, 0.2)',
                    borderColor:'rgba(255, 99, 132, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            suggestedMin: 0,
                            suggestedMax: 100
                        }
                    }]
                }
            }
        });
</script>
@if(Session::get('berhasil'))
<script>
    Swal.fire(
        '{{Session::get('berhasil')}}',
        '',
        'success'
    )
</script>
@endif
@endsection