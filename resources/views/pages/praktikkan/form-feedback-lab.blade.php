@extends('layouts.praktikkan')

@section('title-page', 'Detail Feedback Laboratorium')
<link rel="stylesheet" href="{{asset('js/plugins/ion-rangeslider/css/ion.rangeSlider.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/slick-carousel/slick.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/slick-carousel/slick-theme.css')}}">
@section('css_before')
<style>
    @media(min-width:769) {
        #btnOk {
            width: 80%;
            height: 40%;
        }
    }


    @media(max-width:768) {
        #btnOk {
            width: 30%;
            height: 80%;
        }
    }
</style>
@endsection

@section('content')
<div id="page-loader" class="show"></div>
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">            
            <h1 class="flex-sm-fill h3 my-2">Pengisian Feedback Laboratorium</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{route('praktikan_feedback_lab_idx')}}">Feedback Lab</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="block mx-3">
    <div class="bg-image overflow-hidden" style="background-image: url('{{'https://infotech.umm.ac.id/assets/frontside/images/sliders/slide_1.png'}}');">
    <div class="bg-primary-dark-op" style="height: 250px;">
        <div class="content content-narrow content-full">
            <div
                class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h1 class="font-w600 text-white mb-0 invisible" data-toggle="appear">Laboratorium Informatika</h1>
                    <h2 class="h4 font-w400 text-white-75 mb-0 invisible" data-toggle="appear" data-timeout="250">
                        @if($data['status_isi']==0)
                            Silahkan mengisi penilaian
                        @elseif($data['status_isi']==1)
                            Feedback penilaian laboratorium belum diaktifkan
                        @else
                            Terimakasih anda sudah mengisi penilaian sebelumnya
                        @endif
                    </h2>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="content">
    <h2 class="content-heading">Hubungi Kami Lebih Lanjut Melalui</h2>
    <div class="row">
        <div class="col-md-4">
            <div class="block block-bordered">
                <div class="block-header bg-flat">
                    <h3 class="block-title text-white">Nomor WhatsApp</h3>
                </div>
                <div class="block-content">
                    <p><i class="fab fa-whatsapp"></i><a href="https://api.whatsapp.com/send?phone={{$admin->nomor_wa}}"> {{$admin->nomor_wa}}</a></p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="block block-bordered">
                <div class="block-header bg-smooth">
                    <h3 class="block-title text-white">Email</h3>
                </div>
                <div class="block-content">
                    <p><i class="far fa-envelope"></i><a href="mailto:{{$admin->email}}"> {{$admin->email}}</a></p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="block block-bordered">
                <div class="block-header bg-info">
                    <h3 class="block-title text-white">Nomor Telepon</h3>
                </div>
                <div class="block-content">
                    <p><i class="fa fa fa-phone-square"></i><a href="tel:{{$admin->nomor_telp}}"> {{$admin->nomor_telp}}</a></p>
                </div>
            </div>
        </div>        
    </div>
</div>

@if($data['status_isi']==0)
    <div class="block bg-white mx-3">
        <div class="block-content block-content-full bg-dark">
            <form id="formfeedback" method="POST"
                action="{{route('praktikan_feedback_lab_submit')}}">
                @csrf
                <?php $i = 1 ?>
                @foreach($data['kriterias'] as $d)
                <div class="row block block-rounded mx-1">
                    {{-- <div class="bg-light mt-2 py-2"> --}}
                        <div class="col-12 col-sm-12 col-md-12 col-xl-6 col-lg-6 my-2 d-flex align-items-center">
                            {{$d->desc}}
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-xl-6 col-lg-6 my-2">
                            <div class="form-group p-4 rounded-lg shadow mb-0">
                                <label class="mb-4 text-dark">Point <span class="text-danger">*</span>:</label>
                                <input type="text" class="js-rangeslider" id="point{{$i}}" name="point{{$d->id}}"
                                    data-grid="true" data-from="5" data-values="1, 2, 3, 4, 5">
                            </div>
                        </div>

                    {{-- </div> --}}
                    {{-- <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        </div> --}}
                    {{-- <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <div role="separator" class="dropdown-divider"></div>
                    </div> --}}
                </div>
                <?php $i++ ?>
                @endforeach

                <div class="row">
                    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="kritnsar" class="text-light">Kritik dan Saran (Opsional):</label>
                            <textarea class="form-control" id="kritnsar" name="kritnsar" rows="4"
                                placeholder="Kritik dan Saran.."></textarea>
                        </div>
                    </div>
                    <div
                        class="col-xl-2 col-lg-2 col-md-2 col-xs-4 d-flex align-items-end justify-content-center mb-4 text-center">
                        <button class="btn btn-lg btn-primary" type="submit" id="btnOk">Submit</button>
                    </div>
            </form>
        </div>
    </div>
    </div>`
@endif

@endsection

@section('js_after')

<script src="{{asset('js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js')}}"></script>
<script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('js/plugins/slick-carousel/slick.min.js')}}"></script>
<script>
    jQuery(function(){ One.helpers(['rangeslider']); });

    @if($data['status_isi']==0)
    $('#formfeedback').on('submit',function(){
        var i = 1
        for(; i <= {{$i-1}}; i++){
            if($('#point'+i).val() == 0){
                event.preventDefault()
                Swal.fire(
                    'Point Penilaian Tidak Dapat <br> Bernilai 0',
                    '',
                    'error'
                )
            }
        }
    });
    @endif

</script>
@endsection
