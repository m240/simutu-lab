<li class="nav-main-item">
    <a class="nav-main-link{{ request()->is('frontend/dashboard') ? ' active' : '' }}"
        href="{{route('praktikkan.dashboard.index')}}">
        <i class="nav-main-link-icon si si-cursor"></i>
        <span class="nav-main-link-name">Dashboard</span>
    </a>
</li>
@if(Auth::User()->code_user() != 2)
<li class="nav-main-heading">Feedback</li>
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('frontend/feedback/modul*') ? 'active' : ''}}"
        href="{{route('praktikan_feedback_modul_idx')}}">
        <i class="nav-main-link-icon fa fa-tasks"></i>
        <span class="nav-main-link-name">Modul</span>
    </a>
</li>
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('frontend/feedback/asisten*') ? 'active' : ''}}"
     href="{{route('praktikan_feedback_asisten_idx')}}">
        <i class="nav-main-link-icon si si-users"></i>
        <span class="nav-main-link-name">Asisten</span>
    </a>
</li>
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('frontend/feedback/instruktur*') ? 'active' : ''}}"
         href="{{route('praktikan_feedback_instruktur_idx')}}">
        <i class="nav-main-link-icon fa fa-chalkboard-teacher"></i>
        <span class="nav-main-link-name">Instruktur</span>
    </a>
</li>
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('frontend/feedback/lab*') ? 'active' : ''}}" 
    href="{{route('praktikan_feedback_lab_idx')}}">
        <i class="nav-main-link-icon fa fa-hotel"></i>
        <span class="nav-main-link-name">Laboratorium</span>
    </a>
</li>
@endif
@if(Auth::User()->code_user() != 0)
<li class="nav-main-heading">Performa</li>
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('frontend/perform/feedback*') ? 'active' : ''}}"
        href="{{route('frontend_feedback_index')}}">
        <i class="nav-main-link-icon si si-users"></i>
        <span class="nav-main-link-name">Feedback Praktikum</span>
    </a>
</li>
<!-- @if(Auth::User()->code_user() == 1)
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('frontend/perform/asisten*') ? 'active' : ''}}"
        href="">
        <i class="nav-main-link-icon si si-users"></i>
        <span class="nav-main-link-name">Asisten</span>
    </a>
</li>
@elseif(Auth::User()->code_user() == 2)
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('frontend/perform/instruktur*') ? 'active' : ''}}" 
    href="">
        <i class="nav-main-link-icon fa fa-chalkboard-teacher"></i>
        <span class="nav-main-link-name">Instruktur</span>
    </a>
</li>
@endif -->
@endif