@extends('layouts.praktikkan')

@section('title-page', 'Feedback Modul')

@section('css_before')

@endsection

@section('content')
<div id="page-loader" class="show"></div>
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Pengisian Feedback Modul Praktikum</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{route('praktikan_feedback_modul_idx')}}">Feedback Modul</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>


<div class="block mx-3">
    <div class="bg-image overflow-hidden" style="background-image: url('{{asset('media/modul.png')}}');">
        <div class="bg-primary-dark-op" style="height: 250px;">
            <div class="content content-narrow content-full">
                <div
                    class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-left">
                    <div class="flex-sm-fill">
                        <h1 class="font-w600 text-white mb-0 invisible" data-toggle="appear">Modul Praktikum</h1>
                        <h2 class="h4 font-w400 text-white-75 mb-0 invisible" data-toggle="appear" data-timeout="250">
                            @if(count($data['periode_penilaian']) == 0)
                            Penilaian modul praktikum belum ada yang diaktifkan
                            @else
                            Isi feedback modul pendamping selama melakukan kegiatan praktikum
                            @endif
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if(count($data['periode_penilaian']) != 0)
<div class="block bg-white">
    <div class="block-header">
        <h3 class="block-title">List Kelas Praktikum</h3>
    </div>
    <div class="block-content">
        <div class="row row-deck justify-content-center p-2">
            <?php $i = 0 ?>
            @foreach($data['periode_penilaian'] as $d)
            <div class="col-xl-4 col-md-4 col-sm-12 col-xs-12">
                <div class="block block-rounded bg-dark">
                    <div class="block-content block-content-full text-light">
                        <div class="item item-2x item-rounded bg-body text-primary mx-auto">
                            <i class="fa fa-2x fa-book"></i>
                        </div>
                        <br>
                        <div class="row align-items-center mx-1 ">
                            {{$data['nama_kelas'][$d->ilab_class_category_id]}}
                            @if($d->active == '1' && !in_array($d->id,$data['udh_isi']))
                            <div class="ml-auto">
                                <a href="{{route('praktikan_isi_feedback_modul_idx',['id_predikat'=>$d->id])}}"
                                    class="btn btn-sm btn-rounded btn-light" data-toggle="tooltip" data-placement="top"
                                    title="" data-original-title="Detail Kriteria">
                                    <i class="fa fa-fw fa-arrow-right text-dark"></i>
                                </a>
                            </div>
                            @endif
                        </div>
                        <div role="separator" class="dropdown-divider"></div>
                        @if(in_array($d->id,$data['udh_isi']))
                        <span class="text-success">Sudah Mengisi</span>
                        @else
                        <span class="text-danger">Belum Mengisi</span>
                        @endif
                        <br>
                        Status Feedback
                        @if($d->active == '1')
                        <span class="text-success">Aktif</span>
                        @else
                        <span class="text-danger">Tidak Aktif</span>
                        @endif
                        <br>
                        Jumlah Kriteria : {{$data['jml_kriteria'][$i++]}}
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endif

@endsection

@section('js_after')

@endsection
