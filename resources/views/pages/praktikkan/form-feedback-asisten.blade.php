@extends('layouts.praktikkan')

@section('title-page', 'Detail Feedback Asisten')
<link rel="stylesheet" href="{{asset('js/plugins/ion-rangeslider/css/ion.rangeSlider.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@section('css_before')
<style>
    @media(min-width:769) {
        #btnOk {
            width: 80%;
            height: 40%;
        }
    }


    @media(max-width:768) {
        #btnOk {
            width: 30%;
            height: 80%;
        }
    }
</style>
@endsection

@section('content')
<div id="page-loader" class="show"></div>
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Pengisian Feedback Asisten </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{route('praktikan_feedback_asisten_idx')}}">Feedback Asisten</a>
                    </li>                    
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="#">Pengisian Penilaian</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="block bg-white mx-3">
    <div class="block-content block-content-full bg-dark">
        <h4 class="flex-sm text-white h4 my-2">Asisten Praktikum {{$data['class']->full_name}} </h4>
        <div id="faq1" class="mb-5" role="tablist" aria-multiselectable="true">
            <!-- looping asisten -->
            <?php $complete_fill = true ?>
            <form action="{{route('submit_form_penilaian_asisten',['id_class'=>$data['class_id']])}}" method="POST">
            @csrf
            @foreach($data['asistens'] as $asisten)
            <?php $photo = 'https://krs.umm.ac.id/Poto/'.substr($asisten->user_name,0,4).'/'.$asisten->user_name.'.JPG'?>
            <div class="block block-bordered mb-1">
                <div class="block-header block-header-default" role="tab" id="faq1_h{{$asisten->id}}">                    
                    <a class="text-muted collapsed" data-toggle="collapse" data-parent="#faq1" 
                    href="#faq1_q{{$asisten->id}}" aria-expanded="false" aria-controls="faq1_q{{$asisten->id}}">
                        <!-- nama asisten -->
                        <img class="rounded" src="{{$photo}}" alt="Avatar" style="width: 60px; margin-right: 20px;">
                        {{ $asisten->full_name}} : {{ $asisten->user_name}} :
                        <span class="text-{{ $asisten->sudah_isi==1 ? 'success' : 'danger'}}">{{$asisten->sudah_isi==1? 'Sudah Mengisi':'Belum Mengisi'}}</span>
                    </a>
                </div>
                <div id="faq1_q{{$asisten->id}}" class="collapse" role="tabpanel" 
                    aria-labelledby="faq1_h{{$asisten->id}}" data-parent="#faq1" style="">
                    @if($asisten->sudah_isi==0)
                    <div class="block-content">                        
                        <!-- isi kriteria feedback -->
                        <?php $i = 0 ?>
                        @foreach($data['kriterias'] as $d)
                        <div class="row block block-rounded mx-1">
                            <div class="col-12 col-sm-12 col-md-12 col-xl-6 col-lg-6 my-2 d-flex align-items-center">
                                {{$d->desc}}
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-xl-6 col-lg-6 my-2">
                                <div class="form-group p-4 rounded-lg shadow mb-0">
                                    <label class="mb-4 text-dark">Point <span class="text-danger">*</span>:</label>
                                    <input type="text" class="js-rangeslider" id="point{{$i}}" name="asisten[{{$asisten->id}}][point{{$d->id}}]" value="5"
                                        data-grid="true" data-from="5" data-values="1, 2, 3, 4, 5">
                                </div>
                            </div>
                        </div>
                        <?php $i++ ?>
                        @endforeach
                        <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea class="form-control" id="kritnsar" name="asisten[{{$asisten->id}}][kritnsar]" rows="4"
                                    placeholder="Kritik dan Saran ..."></textarea>
                            </div>
                        </div>
                        </div>
                    </div>
                    <?php $complete_fill = false ?>                      
                    @endif
                </div>
            </div>   
            @endforeach
            <br>
            @if(!$complete_fill)
                <div class="row float-right">
                <div class="col-md-6">
                    <button class="btn btn-lg btn-primary" type="submit" id="btnOk">Submit</button>
                </div> 
                </form>
                </div>
            @endif    
        </div>
    </div>
</div>

@endsection

@section('js_after')

<script src="{{asset('js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js')}}"></script>
<script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<script>
    jQuery(function(){ One.helpers(['rangeslider']); });
</script>
@endsection
