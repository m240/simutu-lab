@extends('layouts.praktikkan')

@section('title-page', 'Dashboard')

@section('css_before')
<link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick.css') }}">
<link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick-theme.css') }}">
@endsection

@section('content')
<!-- Hero -->

@if (session('status'))
<div class="alert alert-warning alert-dismissable" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">×</span>
</button>
<div class="font-size-sm font-w600 text-uppercase text-muted">{{ session('status') }}</div>
</div>
@endif

<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Dashboard</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="">Dashboard</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<div class="block mx-3">
    <div class="bg-image overflow-hidden" style="background-image: url('{{'https://infotech.umm.ac.id/assets/frontside/images/sliders/slide_2.png'}}');">
    <div class="bg-primary-dark-op" style="height: 250px;">
        <div class="content content-narrow content-full">
            <div
                class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h1 class="font-w600 text-white mb-0 invisible" data-toggle="appear">Dashboard</h1>
                    <h2 class="h4 font-w400 text-white-75 mb-0 invisible" data-toggle="appear" data-timeout="250">
                       Sistem Informasi Manajemen Mutu Laboratorium Informatika
                    </h2>
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
                        <a class="btn btn-primary px-4 py-2 js-click-ripple-enabled" data-toggle="click-ripple"
                        href="{{route('praktikkan.dashboard.bukti')}}" target="_blank" rel="noopener noreferrer"
                        style="overflow: hidden; position: relative; z-index: 1;">
                            <i class="fa fa-print"></i> Print Bukti Pengisian
                        </a>
                    </span>
                </div>
            </div>            
        </div>
    </div>
</div>
</div>

<div class="content content-narrow">
    <div class="row">
        <div class="col-6 col-md-3 col-lg-6 col-xl-3">
            <a class="block block-rounded block-link-pop border-left border-primary border-4x"
                href="javascript:void(0)">
                <div class="block-content block-content-full">
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Modul</div>                    
                    <div role="separator" class="dropdown-divider"></div>
                    <p>Total Modul : {{$jml_modul}}</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-3 col-lg-6 col-xl-3">
            <a class="block block-rounded block-link-pop border-left border-primary border-4x"
                href="javascript:void(0)">
                <div class="block-content block-content-full">
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Asisten</div>                    
                    <div role="separator" class="dropdown-divider"></div>
                    <p>Total Asisten : {{$jml_asisten}}</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-3 col-lg-6 col-xl-3">
            <a class="block block-rounded block-link-pop border-left border-primary border-4x"
                href="javascript:void(0)">
                <div class="block-content block-content-full">
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Instruktur</div>                    
                    <div role="separator" class="dropdown-divider"></div>
                    <p>Total Instruktur : {{$jml_instruktur}}</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-3 col-lg-6 col-xl-3">
            <a class="block block-rounded block-link-pop border-left border-primary border-4x"
                href="javascript:void(0)">
                <div class="block-content block-content-full">
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Laboratorium</div>                    
                    <div role="separator" class="dropdown-divider"></div>
                    <p>Total Kriteria : {{$jml_lab}}</p>
                </div>
            </a>
        </div>
    </div>
    <div class="row row-deck">
        <div class="col-lg-12">
            <div class="block block-mode-loading-oneui">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Status Pengisian Modul Praktikum</h3>                    
                </div>
                <div class="block-content block-content-full">
                    <table class="table table-striped table-hover table-borderless table-vcenter font-size-sm mb-0">
                        <thead>
                            <tr class="text-uppercase">
                                <th class="font-w700">No.</th>
                                <th class="d-none d-sm-table-cell font-w700">Modul</th>
                                <th class="font-w700">Status</th>
                            </tr>
                        </thead>
                        <?php $no=1 ?>
                        @foreach($moduls['periode_penilaian'] as $d)
                            <tbody>
                                <tr>
                                    <td>
                                        <span class="font-w600">{{$no++}}</span>
                                    </td>
                                    <td class="d-none d-sm-table-cell">
                                        <span class="font-size-sm text-muted"> {{$moduls['nama_kelas'][$d->ilab_class_category_id]}}</span>
                                    </td>
                                    <td>
                                        @if(in_array($d->id,$moduls['udh_isi']))
                                            <span class="font-w600 text-success">sudah mengisi</span>
                                        @else
                                            <span class="font-w600 text-danger">belum mengisi</span>
                                        @endif
                                    </td>
                                </tr>                                
                            </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-deck">
        <div class="col-lg-12">
            <div class="block block-mode-loading-oneui">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Status Pengisian Asisten</h3>                    
                </div>
                <div class="block-content block-content-full">
                    <table class="table table-striped table-hover table-borderless table-vcenter font-size-sm mb-0">
                        <thead>
                            <tr class="text-uppercase">
                                <th class="font-w700">No.</th>
                                <th class="font-w700">NIM</th>
                                <th class="d-none d-sm-table-cell font-w700">Nama</th>
                                <th class="font-w700">Kelas</th>
                                <th class="font-w700">Status</th>
                            </tr>
                        </thead>
                        <?php $no=1 ?>
                        @foreach($asistens as $kelas)
                           @foreach($kelas['data_asisten'] as $asisten)
                           <tbody>
                                <tr>
                                    <td>
                                        <span class="font-w600">{{$no++}}</span>
                                    </td>                                    
                                    <td>
                                        <span class="font-w600">{{$asisten->user_name}}</span>
                                    </td>
                                    <td>
                                        <span class="font-w600">{{$asisten->full_name}}</span>
                                    </td>
                                    <td class="d-none d-sm-table-cell">
                                        <span class="font-size-sm text-muted">{{$kelas->full_name}}</span>
                                    </td>
                                    <td>
                                        @if($asisten->sudah_isi == 0)
                                            <span class="font-w600 text-danger">belum mengisi</span>
                                        @elseif($asisten->sudah_isi == 1)
                                            <span class="font-w600 text-warning">belum aktif</span>
                                        @elseif($asisten->sudah_isi == 2)
                                            <span class="font-w600 text-success">sudah mengisi</span>
                                        @endif
                                    </td>
                                    </td>
                                </tr>                                
                            </tbody>
                           @endforeach
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-deck">
        <div class="col-lg-12">
            <div class="block block-mode-loading-oneui">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Status Pengisian Instruktur</h3>                    
                </div>
                <div class="block-content block-content-full">
                    <table class="table table-striped table-hover table-borderless table-vcenter font-size-sm mb-0">
                        <thead>
                            <tr class="text-uppercase">
                                <th class="font-w700">No.</th>
                                <th class="font-w700">NIP / NIDN</th>
                                <th class="d-none d-sm-table-cell font-w700">Instruktur</th>
                                <th class="font-w700">Status</th>
                            </tr>
                        </thead>
                        <?php $no=1 ?>
                        @foreach($instrukturs as $instruktur)
                            <tbody>
                                <tr>
                                    <td>
                                        <span class="font-w600">{{$no++}}</span>
                                    </td>
                                    <td>
                                        <span class="font-w600">{{$instruktur->ilab_user_instructor()->first()->nip}}</span>
                                    </td>
                                    <td class="d-none d-sm-table-cell">
                                        <span class="font-size-sm text-muted">{{$instruktur->full_name}}</span>
                                    </td>
                                    <td>
                                        @if($instruktur->sudah_isi == 0)
                                            <span class="font-w600 text-danger">belum mengisi</span>
                                        @elseif($instruktur->sudah_isi == 1)
                                            <span class="font-w600 text-warning">belum aktif</span>
                                        @elseif($instruktur->sudah_isi == 2)
                                            <span class="font-w600 text-success">sudah mengisi</span>
                                        @endif
                                    </td>
                                </tr>                                
                            </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-deck">
        <div class="col-lg-12">
            <div class="block block-mode-loading-oneui">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Status Pengisian Laboratorium</h3>                    
                </div>
                <div class="block-content block-content-full">
                    <table class="table table-striped table-hover table-borderless table-vcenter font-size-sm mb-0">
                        <thead>
                            <tr class="text-uppercase">
                                <th class="font-w700">No.</th>
                                <th class="font-w700">Nama</th>
                                <th class="font-w700">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <span class="font-w600">1. </span>
                                </td>
                                <td class="d-none d-sm-table-cell">
                                    <span class="font-size-sm text-muted">
                                        @if($lab['sudah_isi'] == 1)
                                            Fasilitas Laboratorium Informatika
                                        @else
                                            {{$lab['nama']}}
                                        @endif
                                    </span>
                                </td>
                                <td>
                                    @if($lab['sudah_isi'] == 0)
                                        <span class="font-w600 text-danger">belum mengisi</span>
                                    @elseif($lab['sudah_isi'] == 1)
                                        <span class="font-w600 text-warning">belum aktif</span>
                                    @elseif($lab['sudah_isi'] == 2)
                                        <span class="font-w600 text-success">sudah mengisi</span>
                                    @endif
                                </td>
                            </tr>                                
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->
@endsection

@section('js_after')
<script src="{{asset('js/plugins/chart.js/Chart.bundle.min.js')}}"></script>
<script src="{{asset('js/pages/admin_dashboard.js') }}"></script>
<script src="{{asset('js/plugins/slick-carousel/slick.min.js')}}"></script>
@endsection