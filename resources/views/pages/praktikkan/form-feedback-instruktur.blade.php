@extends('layouts.praktikkan')

@section('title-page', 'Detail Feedback Instruktur')
<link rel="stylesheet" href="{{asset('js/plugins/ion-rangeslider/css/ion.rangeSlider.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@section('css_before')
<style>
    @media(min-width:769) {
        #btnOk {
            width: 80%;
            height: 40%;
        }
    }


    @media(max-width:768) {
        #btnOk {
            width: 30%;
            height: 80%;
        }
    }
</style>
@endsection

@section('content')
<div id="page-loader" class="show"></div>
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Pengisian Feedback Instruktur {{ $data['instruktur']->full_name}}</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{route('praktikan_feedback_instruktur_idx')}}">Feedback Instruktur</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="#">Pengisian Penilaian</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="block bg-white mx-3">
    <div class="block-content block-content-full bg-dark">
        <form id="formfeedback" method="POST"
            action="{{route('praktikan_feedback_instruktur_form_submit',[ 'id_class' => $data['class_id'], 'id_instruktur'=>$data['instruktur']->id])}}">
            @csrf
            <?php $i = 1 ?>
            @foreach($data['kriterias'] as $d)
            <div class="row block block-rounded mx-1">
                {{-- <div class="bg-light mt-2 py-2"> --}}
                    <div class="col-12 col-sm-12 col-md-12 col-xl-6 col-lg-6 my-2 d-flex align-items-center">
                        {{$d->desc}}
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-xl-6 col-lg-6 my-2">
                        <div class="form-group p-4 rounded-lg shadow mb-0">
                            <label class="mb-4 text-dark">Point <span class="text-danger">*</span>:</label>
                            <input type="text" class="js-rangeslider" id="point{{$i}}" name="point{{$d->id}}"
                                data-grid="true" data-from="5" data-values="1, 2, 3, 4, 5">
                        </div>
                    </div>

                {{-- </div> --}}
                {{-- <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    </div> --}}
                {{-- <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                    <div role="separator" class="dropdown-divider"></div>
                </div> --}}
            </div>
            <?php $i++ ?>
            @endforeach

            <div class="row">
                <div class="col-xl-10 col-lg-10 col-md-10 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="kritnsar" class="text-light">Kritik dan Saran (Opsional):</label>
                        <textarea class="form-control" id="kritnsar" name="kritnsar" rows="4"
                            placeholder="Kritik dan Saran.."></textarea>
                    </div>
                </div>
                <div
                    class="col-xl-2 col-lg-2 col-md-2 col-xs-4 d-flex align-items-end justify-content-center mb-4 text-center">
                    <button class="btn btn-lg btn-primary" type="submit" id="btnOk">Submit</button>
                </div>
        </form>
    </div>
</div>
</div>

@endsection

@section('js_after')

<script src="{{asset('js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js')}}"></script>
<script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<script>
    jQuery(function(){ One.helpers(['rangeslider']); });

    $('#formfeedback').on('submit',function(){
        var i = 1
        for(; i <= {{$i-1}}; i++){
            if($('#point'+i).val() == 0){
                event.preventDefault()
                Swal.fire(
                    'Point Penilaian Tidak Dapat <br> Bernilai 0',
                    '',
                    'error'
                )
            }
        }
    });

</script>
@endsection
