@extends('layouts.praktikkan')

@section('title-page', 'Feedback Instruktur')

@section('css_before')

@endsection

@section('content')


@if (session('status'))
<div class="alert alert-warning alert-dismissable" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">×</span>
</button>
<div class="font-size-sm font-w600 text-uppercase text-muted">{{ session('status') }}</div>
</div>
@endif


<div id="page-loader" class="show"></div>
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Pengisian Feedback Instruktur Praktikum</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{route('praktikan_feedback_instruktur_idx')}}">Feedback Instruktur</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="block mx-3">
    <div class="bg-image overflow-hidden" style="background-image: url('{{asset('media/instruktur.png')}}');">
    <div class="bg-primary-dark-op" style="height: 250px;">
        <div class="content content-narrow content-full">
            <div
                class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h1 class="font-w600 text-white mb-0 invisible" data-toggle="appear">Instruktur Laboratorium</h1>
                    <h2 class="h4 font-w400 text-white-75 mb-0 invisible" data-toggle="appear" data-timeout="250">
                        @if($status == 0)
                            Mohon maaf penilaian instruktur masih belum diaktifkan
                        @else
                            Pilih sesuai dengan instruktur kelas praktikum masing-masing
                        @endif
                    </h2>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


@if($status == 1)
<div class="block bg-white">
    <div class="block-header">
        <h3 class="block-title">List Instruktur Praktikum</h3>
    </div>
    <div class="block-content">
        <div class="row row-deck justify-content-center p-2">
            <?php $i = 0 ?>
            @foreach($data['class_active'] as $class)
            <div class="col-xl-4 col-md-4 col-sm-12 col-xs-12">
                <div class="block block-rounded bg-dark">
                    <div class="block-content block-content-full text-light">
                        <div class="item item-2x item-rounded bg-warning text-white mx-auto">
                            <i class="si fa-2x si-user"></i>
                        </div>
                        <br>
                        <div class="row align-items-center mx-1 ">
                        {{$class->full_name}}
                        <br>                        
                        {{$class['instructor']->full_name}}
                        </div>
                        <div role="separator" class="dropdown-divider"></div>
                        Status : <span class="text-{{ $class['sudah_isi'] == 1 ? 'success' : 'danger'}}">{{ $class['sudah_isi'] == 1 ? "Sudah Mengisi" : "Belum Mengisi"}} </span>
                        @if($class['sudah_isi'] == 0)
                        <div class="ml-auto float-right">
                            <a href="{{route('praktikan_feedback_instruktur_form',[ 'id_class' => $class->id, 'id_instruktur'=>$class['instructor']->id])}}"
                                class="btn btn-sm btn-rounded btn-light" data-toggle="tooltip" data-placement="top"
                                title="" data-original-title="Detail Penilaian">
                                <i class="fa fa-fw fa-arrow-right text-dark"></i>
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endif

@endsection

@section('js_after')

@endsection
