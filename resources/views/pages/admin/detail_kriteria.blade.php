@extends('layouts.backend')
@section('title-page','Detail Kriteria')

@section('css_before')
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/ion-rangeslider/css/ion.rangeSlider.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
<div id="page-loader" class="show"></div>
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Detail Kriteria</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{route('adm1n.dashboard.index')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{route('adm1n_dashboard_kriteria')}}">Kriteria</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        Detail Kriteria
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="block">
    <div class="block-header block-header-default bg-primary-dark-op d-flex">
        {{-- <div>
            <a type="button" href="/adm1n/dashboard/kriteria" class="btn btn-dark mr-2 mb-2 mt-2"><i
                    class="fa fa-arrow-left mr-1"></i>Kembali</a>
        </div> flex-grow-1 --}}
        <div class=" text-center">
            <h3 class="block-title text-light">Detail Kriteria {{$data['kriteria']->nama_kriteria}}</h3>
        </div>
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-6 col-md-3 col-lg-6 col-xl-3">
        <div class="block block-rounded block-link-pop border-left border-primary border-4x">
            <div class="block-content block-content-full">
                <div class="font-size-sm font-w600 text-uppercase text-muted">Persentase</div>
                <div class="font-size-h2 font-w400 text-dark">{{100-$data['persentase']}} %</div>
            </div>
        </div>
    </div>
    <div class="col-6 col-md-3 col-lg-6 col-xl-3">
        <div class="block block-rounded block-link-pop border-left border-primary border-4x">
            <div class="block-content block-content-full">
                <div class="font-size-sm font-w600 text-uppercase text-muted">Detail Kategori</div>
                <div class="font-size-h2 font-w400 text-dark">{{count($data['detail'])}}</div>
            </div>
        </div>
    </div>
</div>

<!-- Dynamic Table Keriteria Full Pagination -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title">List Kriteria</h3>
        <button type="button" class="btn btn-sm btn-success mr-1" id="btn-tambah-kriteria">
            <i class="fa fa-fw fa-plus mr-1"></i> Tambah Kriteria
        </button>
    </div>
    <div class="block-content block-content-full">
        <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
        <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
            <thead>
                <tr>
                    <th class="text-center" style="width: 80px;">No</th>
                    <th class="d-sm-table-cell" style="width: 80%;">Deskripsi</th>
                    <th class="d-sm-table-cell" style="width: 10%;">Persentase</th>
                    <th class="d-sm-table-cell" style="width: 10%;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 1;
                ?>
                @foreach ($data['detail'] as $d)
                <tr id="baris{{$d->id}}">
                    <td class="text-center font-size-sm">{{$i}}</td>
                    <td class="font-w600 font-size-sm">
                        {{Str::limit($d->desc,200)}}
                    </td>
                    <td>
                        {{$d->persentase}}
                    </td>
                    <td>
                        <div class="row justify-content-center">
                            <div data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                <button type="button" class="btn btn-sm btn-rounded btn-success ml-1 mr-1"
                                    data-id="{{$d->id}}" data-desc="{{$d->desc}}" data-pers="{{$d->persentase}}"
                                    data-toggle="modal" data-target="#modal-edit-kriteria">
                                    <i class="fa fa-fw fa-pencil-alt"></i>
                                </button>
                            </div>
                            <button type="button" class="btn btn-sm btn-rounded btn-danger ml-1 btn-hapus-kriteria"
                                data-id="{{$d->id}}" data-toggle="tooltip" data-placement="top" title=""
                                data-original-title="Hapus">
                                <i class="fa fa-fw fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <?php
                    $i++;
                ?>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<form id="form_delete_kriteria_here" action="{{route('adm1n_dashboard_kriteria_detail_hapus')}}" method="POST">
    @csrf
    <input type="hidden" name="id" id="id_form_delete_kriteria_here">
</form>
<!-- END Dynamic Table Full Pagination -->

<!-- Modal Tambah -->
<div class="modal fade" id="modal-tambah-kriteria" tabindex="-1" role="dialog" aria-labelledby="modal-block-popout"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-popout" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Tambah Kriteria</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content font-size-sm">
                    {{-- form input --}}
                    <form id="form-tambah-kriteria"
                        action="{{route('adm1n_tambah_detail_kriteria',['id'=>$data['id']])}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="descT">Deskripsi Kriteria <span class="text-danger">*</span></label>
                            <textarea class="form-control" name="desc" id="descT" placeholder="Deskripsi Kriteria"
                                rows="10" required=""></textarea>
                        </div>
                        <div class="form-group mb-5" id="persbox">
                            <label class="mb-4">Persentase</label>
                            <input type="text" class="js-rangeslider" name="pers" value="1" data-min="1"
                                data-max="{{$data['persentase']}}">
                        </div>
                </div>
                <div class="block-content block-content-full text-right border-top">
                    <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check mr-1"></i>Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Modal Tambah -->

<!-- Modal Edit -->
<div class="modal fade" id="modal-edit-kriteria" tabindex="-1" role="dialog" aria-labelledby="modal-block-popout"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-popout" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Edit Kriteria</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content font-size-sm">
                    {{-- form input --}}
                    <form id="form-edit-kriteria" method="POST" action="">
                        @csrf
                        <input type="hidden" class="form-control" id="idE" name="id">
                        <div class="form-group">
                            <label for="desc">Deskripsi Kriteria <span class="text-danger">*</span></label>
                            <textarea class="form-control" name="desc" id="desc" placeholder="Deskripsi Kriteria"
                                rows="10" required=""></textarea>
                        </div>
                        <div class="form-group">
                            <label for="persE">Persentase</label>
                            <input type="text" readonly="" class="form-control" id="persE" name="">
                        </div>
                </div>
                <div class="block-content block-content-full text-right border-top">
                    <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check mr-1"></i>Simpan</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Modal Edit -->
@endsection

@section('js_after')
<!-- Page JS Plugins -->
<script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/buttons.print.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js')}}"></script>
<script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<script>
    jQuery(function(){ One.helpers(['rangeslider']); });
</script>

<!-- Page JS Code -->
<script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
<script src="{{asset('js/pages/kriteria.all.js')}}"></script>
<script>
    var persen = '{{$data['persentase']}}';
    $("#modal-edit-kriteria").on("show.bs.modal", function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        let id = button.data('id');
        let url = "{{route('adm1n_edit_detail_kriteria',':__id')}}";
        url = url.replace(':__id', id);
        $("#form-edit-kriteria").attr("action",url);
        $("#desc").val(button.data('desc'));
        $('#persE').val(button.data('pers'))
    });
</script>

@if(Session::get('berhasil'))
<script>
    Swal.fire(
        '{{Session::get('berhasil')}}',
        '',
        'success'
    )
</script>
@endif
@endsection
