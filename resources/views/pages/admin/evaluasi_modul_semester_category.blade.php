@extends('layouts.backend')
@section('title-page','Praktikum Asisten')

@section('css_before')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
    <div id="page-loader" class="show"></div>
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">{{$classCategory -> category_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item" aria-current="page">Evaluasi
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx"
                               href="{{route('admin_evaluasi_modul')}}">Modul</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx"
                               href="{{route('admin_evaluasi_modul_semester', ['id' => $idx])}}">Category</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">{{$classCategory -> category_name}}
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- Dynamic Table Periode Full Pagination -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">{{$semester -> category_name}}</h3>
        </div>

        <div class="row row-cols-1 row-cols-md-2 d-flex justify-content-center mt-5">
            <div class="col mb-4">
                <div class="card">
                    <canvas id="myChart"></canvas>
                </div>
            </div>
        </div>

        <div class="block-content d-flex justify-content-end mt-2" style="padding-top: 0;">
            <form action="{{route("admin_evaluasi_modul_export")}}" method="post">
                @csrf
                <input type="hidden" name="title" value="{{$classCategory -> category_name}}">
                <input type="hidden" name="semester" value="{{$semester -> category_name}}">
                @foreach ($classArr as $k => $class)
                    <input type="hidden" name="name_modul[]" value="{{$class['name']}}">
                    <input type="hidden" name="value_modul[]" value="{{round($class['value'], 2)}}">
                @endforeach
                <button type="submit" class="btn btn-primary btn-sm" style="color: white">
                    Export
                    CSV
                </button>
            </form>
        </div>

        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination"
                   id="tablePraktikum">
                <thead>
                <tr>
                    <th class="text-center" style="width: 5%;">No</th>
                    <th class="text-center">Nama Modul</th>
                    <th class="text-center">Rata - rata</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1;?>
                @foreach ($classArr as $k => $class)
                    <tr>
                        <td class="text-center font-size-sm">{{$i}}</td>
                        <td class="font-w600 font-size-sm text-center">
                            {{$class['name']}}
                        </td>
                        <td class="font-size-sm text-center">
                            {{round($class['value'], 2)}}
                        </td>
                    </tr>
                    <?php $i++;?>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full Pagination -->
@endsection

@section('js_after')
    <script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
    <script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    <script src="{{asset('js/plugins/chart.js/Chart.min.js')}}"></script>


    <script>
        $(document).ready(function () {
            oTable = $('#tablePraktikum').dataTable()
            {
                order: [[0, "asc"]]
                $('#exampleFormControlSelect1').on('change', function () {
                    var selectedValue = $(this).val();

                    oTable.fnFilter(selectedValue); //Exact value, column, reg
                });
            }
        })

        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: {!! json_encode($className) !!},
                datasets: [{
                    label: "Average Grade Task",
                    data: {!! json_encode($classValue) !!},
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
            }
        });
    </script>

@endsection
