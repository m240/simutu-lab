@extends('layouts.backend')
@section('title-page','Predikat')

@section('css_before')
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
<div id="page-loader" class="show"></div>
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Predikat</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{route('adm1n.dashboard.index')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        Predikat
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>


<!-- <div class="alert alert-warning alert-dismissable" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <div class="font-size-sm font-w600 text-uppercase text-muted">DISARANKAN UNTUK TIDAK MENGHAPUS PREDIKAT JIKA SUDAH DIGUNAKAN DALAM PERIODE LAIN</div>
</div> -->

<!-- Dynamic Table Predikat Full Pagination -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title">List Predikat</h3>
        <button type="button" class="btn btn-sm btn-success mr-1" data-toggle="modal" data-target="#modal-tambah">
            <i class="fa fa-fw fa-plus mr-1"></i> Tambah Predikat
        </button>
    </div>
    <div class="block-content block-content-full">
        <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
        <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination js-table-sections">
            <thead>
                <tr>
                    <th class="text-center" style="width: 60px;">No</th>
                    <th class="d-sm-table-cell">Nama</th>
                    <th class="d-sm-table-cell" style="width: 10%;">Detail Predikat</th>
                    <th class="d-sm-table-cell" style="width: 10%;">Periode Penilaian</th>
                    <th class="d-sm-table-cell" style="width: 15%;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; ?>
                @foreach($data['predikat'] as $d)
                <tr id="baris{{$d->id}}">
                    <td>{{$i}}</td>
                    <td>{{$d['nama_predikat']}}</td>
                    <td>{{$data['jml_detail'][$i-1]}}</td>
                    <td>{{$data['jml_periode'][$i-1]}}</td>
                    <td>
                        <div class="row justify-content-center">
                            <a type="button" href="{{route('predikat_detail_byId',['id'=>$d->id])}}"
                                class="btn btn-sm btn-rounded btn-info mr-1" data-toggle="tooltip" data-placement="top"
                                title="" data-original-title="Detail Predikat">
                                <i class="fa fa-fw fa-star text-light"></i>
                            </a>
                            <div data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                <button type="button" class="btn btn-sm btn-rounded btn-success ml-1 mr-1"
                                    data-toggle="modal" data-target="#modal-edit" data-id="{{$d->id}}"
                                    data-nama="{{$d->nama_predikat}}">
                                    <i class="fa fa-fw fa-pencil-alt"></i>
                                </button>
                            </div>
                            <button type="button" class="btn btn-sm btn-rounded btn-danger ml-1 btn-hapus"
                                data-id="{{$d->id}}" data-toggle="tooltip" data-placement="top" title=""
                                data-original-title="Hapus">
                                <i class="fa fa-fw fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <?php $i++; ?>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END Dynamic Table Full Pagination -->

<!-- Modal Tambah -->
<div class="modal fade" id="modal-tambah" tabindex="-1" role="dialog" aria-labelledby="modal-block-popout"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-popout modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Tambah Predikat</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content font-size-sm">
                    {{-- form input --}}
                    <form id="form-tambah-predikat" action="{{route('adm1n_dashboard_predikat_tambah')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="nama">Nama Predikat <span class="text-danger">*</span></label>
                            <textarea type="text" class="form-control" name="nama" placeholder="Nama Predikat"
                                required></textarea>
                        </div>
                </div>
                <div class="block-content block-content-full text-right border-top">
                    <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus mr-1"></i>Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Modal Tambah -->

<!-- Modal Edit -->
<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="modal-block-popout"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-popout modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Edit Predikat</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content font-size-sm">
                    {{-- form input --}}
                    <form id="form-edit-predikat" action="" method="POST">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" id="id_edit" name="id">
                            <label for="nama">Nama Predikat <span class="text-danger">*</span></label>
                            <textarea type="text" class="form-control" id="nama_edit" name="nama"
                                placeholder="Nama Predikat" required></textarea>
                        </div>
                </div>
                <div class="block-content block-content-full text-right border-top">
                    <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check mr-1"></i>Simpan</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Modal Edit -->
@endsection

@section('js_after')
<!-- Page JS Plugins -->
<script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/buttons.print.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<!-- Page JS Code -->
<script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>

<script>
    var route = {hapus : '{{route('adm1n_dashboard_predikat_hapus')}}'}
    $('#form-tambah-predikat').on('submit',function(){
        One.loader('show')
    })
    $('#form-edit-predikat').on('submit',function(){
        One.loader('show')
    })

    $("#modal-edit").on("show.bs.modal", function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        let id = button.data('id');
        let url = "{{route('admin_edit_predikat_idx',':__id')}}";
        url = url.replace(':__id', id);
        $("#form-edit-predikat").attr("action",url);
        $('#nama_edit').html(button.data('nama'));
        $('#id_edit').val(button.data('id'));
    });

    $(document).on('click','.btn-hapus',function(){
        data = $(this).data('id')
        Swal.fire({
        title: 'Yakin ingin menghapus predikat?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Ya, hapus!',
        cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                One.loader('show')
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'post',
                    url: route.hapus,
                    data: {
                        'id':  $(this).data('id')
                    },
                    success: function(){
                        afterHapus()
                    }
                });
            }
        })
    })

    function afterHapus(){
        // $('#baris'+i).remove()
        // One.loader('hide')
        // Swal.fire(
        //     'Terhapus!',
        //     'Predikat telah terhapus.',
        //     'success'
        // )
        location.reload();
    }
</script>

@if(Session::get('berhasil'))
<script>
    Swal.fire(
        '{{Session::get('berhasil')}}',
        '',
        'success'
    )
</script>
@endif
@endsection
