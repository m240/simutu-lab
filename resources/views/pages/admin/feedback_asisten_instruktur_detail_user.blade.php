@extends('layouts.backend')
@section('title-page','Feedback Modul')

@section('css_before')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
<div id="page-loader" class="show"></div>
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Feedback
                {{$data['periode']->jenis_penilaians == "asisten" ? "Asisten" : "Instruktur"}}</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">Feedback
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx"
                            href="{{route('admin_feedback_asisten_instruktur_idx')}}">Asisten/Instruktur</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx"
                            href="{{route('admin_detail_feedback_asisten_instruktur_idx',['id'=>$data['periode']->id])}}">
                            List
                            {{$data['periode']->jenis_penilaians == "asisten" ? "Asisten" : "Instruktur"}}</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        Detail{{$data['periode']->jenis_penilaians == "asisten" ? " Asisten" : " Instruktur"}}
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<!-- Dynamic Table Periode Full Pagination -->
<div class="block">
    <div class="block-header">
        <div class="d-block">
            <h3 class="block-title">Detail Kriteria Penilaian
                {{$data['periode']->jenis_penilaians == "asisten" ? " Asisten" : " Instruktur"}}</h3>
            <h3 class="mb-2">{{$data['usernya']}}</h3>
            <h4>Total Nilai : {{$data['data_user_proc']->total_nilai_user}}</h4>
        </div>

        <button class="btn btn-primary btn-sm"
            onclick="exportTableToCSV('Detail Kriteria {{$data['periode']->nama_penilaian}}.csv','tabel_kriteria')">Export
            CSV</button>
    </div>
    <div class="block-content block-content-full">
        <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
        <table id="tabel_kriteria"
            class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
            <thead>
                <tr>
                    <th class="text-center" style="width: 5%;">No</th>
                    <th class="d-sm-table-cell" style="width: 50%;">Deskripsi Kriteria</th>
                    <th class="d-sm-table-cell" style="width: 15%;">Persentase</th>
                    <th class="d-sm-table-cell" style="width: 15%;">Rata-Rata Nilai</th>
                    <th class="d-sm-table-cell" style="width: 15%">Nilai</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0;
                ?>
                @foreach ($data['detail_kriteria'] as $d)
                <tr>
                    <td class="text-center font-size-sm">{{$i+1}}</td>
                    <td class="font-w600 font-size-sm">
                        {{$d->desc}}
                    </td>
                    <td class="font-w600 font-size-sm ">
                        <div class="justify-content-center row">
                            {{$d->persentase}}
                        </div>
                    </td>
                    <td class="font-w600 font-size-sm">
                        <div class="justify-content-center row">
                            {{$data['nilai'][$i]}}
                        </div>
                    </td>
                    <td class="font-w600 font-size-sm">
                        <div class="justify-content-center row">
                            {{100*$data['nilai'][$i]/$d->persentase}}
                        </div>
                    </td>
                </tr>
                <?php
                    $i++;
                ?>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END Dynamic Table Full Pagination -->

<!-- Dynamic Table Periode Full Pagination -->
<div class="block shadow">
    <div class="block-header">
        <div class="d-block">
            <h3 class="block-title">Kritik Saran</h3>
            <h3>{{$data['periode']->nama_penilaian}}</h3>
        </div>
        <button class="btn btn-primary btn-sm"
            onclick="exportTableToCSV('Kritik Saran {{$data['periode']->nama_penilaian}}.csv','tabel_msg')">Export
            CSV</button>
    </div>
    <div class="block-content block-content-full">
        <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
        <table id="tabel_msg" class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
            <thead>
                <tr>
                    <th class="text-center" style="width: 5%;">No</th>
                    <th class="d-sm-table-cell" style="width: 15%;">Foto</th>
                    <th class="d-sm-table-cell" style="width: 80%;">Komentar</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0;
                ?>
                @foreach ($data['msg'] as $d)
                <tr>
                    <td class="text-center font-size-sm">{{$i+1}}</td>
                    <td class="font-w600 font-size-sm kolom-foto">
                        <div class="justify-content-center row">
                            <img class="img-avatar" style="width:50px; height:auto;" src="{{$data['user'][$i]}}"
                                alt="Foto User" height="50">
                            <p style="display: none;">{{$data['nim'][$i]}}</p>
                        </div>
                    </td>
                    <td class="font-w600 font-size-sm">
                        {{$d->message}}
                    </td>
                </tr>

                <?php
                    $i++;
                ?>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END Dynamic Table Full Pagination -->
@endsection

@section('js_after')
<script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
<script src="{{asset('js/custom/table2csv.js')}}"></script>
<script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
@if(Session::get('berhasil'))
<script>
    Swal.fire(
        '{{Session::get('berhasil')}}',
        '',
        'success'
    )
</script>
@endif
@endsection
