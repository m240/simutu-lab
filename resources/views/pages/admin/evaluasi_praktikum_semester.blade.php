@extends('layouts.backend')
@section('title-page','Praktikum Asisten')

@section('css_before')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
    <div id="page-loader" class="show"></div>
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Evaluasi Praktikum Semester</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item" aria-current="page">Evaluasi
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx"
                               href="{{route('admin_evaluasi_praktikum')}}">Praktikum</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">Category
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- Dynamic Table Periode Full Pagination -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">{{$classCategories -> category_name}}</h3>
        </div>

        <div class="block-content">
            <div class="row row-cols-1 row-cols-md-2">
                <div class="col mb-4">
                    <div class="card">
                        <h5 class="card-title text-center">Average Keseluruhan Modul dari Semua Kategori</h5>
                        <canvas id="myLineChart"></canvas>
                    </div>
                </div>
                <div class="col mb-4">
                    <div class="card">
                        <h5 class="card-title text-center">Average Keseluruhan Tiap Category Kelas</h5>
                        <canvas id="myPieChart"></canvas>
                    </div>
                </div>
            </div>
            <hr/>
        </div>

        <div class="block-content d-flex justify-content-end mt-2" style="padding-top: 0;">
            <form action="{{route("admin_evaluasi_praktikum_semester_export", ['id' => $idx])}}" method="post">
                @csrf
                @foreach ($classCategories -> ilab_class_category as $category)
                    <input type="hidden" name="category_name[]" value="{{$category -> category_name}}">
                    <input type="hidden" name="category_count[]" value="{{$category -> ilab_class_count}}">
                    <input type="hidden" name="avg_modul[]" value="
                    @php($arrGrade = [])
                    @foreach($category -> ilab_class as $class)
                    @php($arrGrade[] = $class->ilab_task->where('final_exam', 0)->avg('avg_task_detail'))
                    @endforeach
                    @if(count($arrGrade)!= 0)
                    {{round(array_sum($arrGrade)/count($arrGrade),2)}}
                    @else
                    {{0}}
                    @endif">
                    <input type="hidden" name="avg_uap[]" value="
                    @php($arrGrade = [])
                    @foreach($category -> ilab_class as $class)
                    @php($arrGrade[] = $class->ilab_task->where('final_exam', 1)->avg('avg_task_detail'))
                    @endforeach
                    @if(count($arrGrade)!= 0)
                    {{round(array_sum($arrGrade)/count($arrGrade),2)}}
                    @else
                    {{0}}
                    @endif">
                @endforeach
                <button type="submit" class="btn btn-primary btn-sm" style="color: white">
                    Export
                    CSV
                </button>
            </form>
        </div>

        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination"
                   id="tablePraktikum">
                <thead>
                <tr>
                    <th class="text-center" style="width: 5%;">No</th>
                    <th class="text-center" style="width: 20%;">Category</th>
                    <th class="text-center" style="width: 15%;">Jumlah Kelas</th>
                    <th class="d-sm-table-cell text-center">Rata rata Nilai Modul</th>
                    <th class="d-sm-table-cell text-center">Rata rata Nilai UAP</th>
                    <th class="d-sm-table-cell text-center" style="width: 10%;">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($classCategories -> ilab_class_category as $category)
                    <tr>
                        <td class="text-center font-size-sm">{{$loop -> iteration}}</td>
                        <td class="font-w600 font-size-sm text-center">
                            {{$category -> category_name}}
                        </td>
                        <td class="font-size-sm text-center">
                            {{$category -> ilab_class_count}}
                        </td>
                        <td class="font-size-sm text-center">
                            @php($arrGrade = [])
                            @foreach($category -> ilab_class as $class)
                                @php($arrGrade[] = $class->ilab_task->where('final_exam', 0)->avg('avg_task_detail'))
                            @endforeach
                            @if(count($arrGrade)!= 0)
                                {{round(array_sum($arrGrade)/count($arrGrade),2)}}
                            @else
                                {{0}}
                            @endif
                        </td>
                        <td class="font-size-sm text-center">
                            @php($arrGrade = [])
                            @foreach($category -> ilab_class as $class)
                                @php($arrGrade[] = $class->ilab_task->where('final_exam', 1)->avg('avg_task_detail'))
                            @endforeach
                            @if(count($arrGrade)!= 0)
                                {{round(array_sum($arrGrade)/count($arrGrade),2)}}
                            @else
                                {{0}}
                            @endif
                        </td>
                        <td>
                            <div class="row justify-content-center">
                                <a type="button"
                                   href="{{route('admin_evaluasi_praktikum_semester_category',['id'=>$idx,'idcategory'=>$category->id])}}"
                                   class="btn btn-sm btn-rounded btn-secondary" data-toggle="tooltip"
                                   data-placement="top"
                                   data-original-title="Detail Penilaian">
                                    <i class="fa fa-arrow-right text-light"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full Pagination -->
@endsection

@section('js_after')
    <script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
    <script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    <script src="{{asset('js/plugins/chart.js/Chart.min.js')}}"></script>

    <script>
        const canvas = document.getElementById('myLineChart');
        new Chart(canvas, {
            type: 'line',
            data: {
                labels: {!! json_encode($arrGradeLabel) !!},
                datasets: [{
                    label: 'MODUL',
                    yAxisID: 'MODUL',
                    borderColor: '#3498db',
                    backgroundColor: '#3498db',
                    fill: false,
                    data: {!! json_encode($arrGradeAvg) !!}
                }, {
                    label: 'UAP',
                    yAxisID: 'UAP',
                    borderColor: '#e74c3c',
                    backgroundColor: '#e74c3c',
                    fill: false,
                    data: {!! json_encode($arrExamAvg) !!}
                }]
            },
            options: {
                responsive: true,
                hoverMode: 'index',
                stacked: false,
                scales: {
                    yAxes: [{
                        id: 'MODUL',
                        type: 'linear',
                        display: true,
                        position: 'left',
                    }, {
                        id: 'UAP',
                        type: 'linear',
                        display: true,
                        position: 'right',

                        gridLines: {
                            drawOnChartArea: false, // only want the grid lines for one axis to show up
                        },
                    }]
                }
            }
        });


        //var ctx2 = document.getElementById("myPieChart");
        new Chart(document.getElementById("myPieChart"), {
            type: 'pie',
            data: {
                labels: {!! json_encode($arrGradeLabel) !!},
                datasets: [{
                    label: "Population (millions)",
                    data: {!! json_encode($arrFinalAvg) !!},
                    backgroundColor: ['#1abc9c', '#2ecc71', '#3498db', '#9b59b6', '#34495e', '#f1c40f', '#e67e22',
                        '#e74c3c', '#ecf0f1', '#95a5a6', '#fd79a8', '#ff7675', '#fab1a0', '#ffeaa7', '#a29bfe'],
                    hoverBackgroundColor: ['#16a085', '#27ae60', '#2980b9', '#8e44ad', '#2c3e50', '#f39c12', '#d35400',
                        '#c0392b', '#bdc3c7', '#7f8c8d', '#e84393', '#d63031', '#e17055', '#fdcb6e', '#6c5ce7'],
                    hoverBorderColor: "rgba(234, 236, 244, 1)"
                }]
            },
            options: {
                legend: {
                    display: false
                },
                tooltips: {
                    enabled: true
                }
            }
        });

    </script>

@endsection
