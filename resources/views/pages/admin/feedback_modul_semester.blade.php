@extends('layouts.backend')
@section('title-page','Feedback Modul')

@section('css_before')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
    <div id="page-loader" class="show"></div>
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Feedback Modul Praktikum</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item" aria-current="page">Feedback
                        </li>
                        <li class="breadcrumb-item" aria-current="page">Modul
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- Dynamic Table Periode Full Pagination -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Pilih Periode Penilaian</h3>
        </div>
        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
                <thead>
                <tr>
                    <th class="text-center" style="width: 5%;">No</th>
                    <th class="text-center" style="width: 20%;">Tahun</th>
                    <th class="text-center" style="width: 10%;">Semester</th>
                    <th class="d-sm-table-cell" style="width: 65%;">Name</th>
                    <th class="d-sm-table-cell" style="width: 10%;">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($classCategories as $category)
                    <tr>
                        <td class="text-center font-size-sm">{{$loop -> iteration}}</td>
                        <td class="font-w600 font-size-sm text-center">
                            {{explode(' ', $category -> category_name)[2]}}
                        </td>
                        <td class="font-size-sm text-center">
                            {{explode(' ', $category -> category_name)[1]}}
                        </td>
                        <td class="font-size-sm">
                            {{$category -> category_name}}
                        </td>
                        <td>
                            <div class="row justify-content-center">
                                <a type="button" href="{{route('admin_feedback_modul',['semester'=>$category->id])}}"
                                   class="btn btn-sm btn-rounded btn-secondary" data-toggle="tooltip"
                                   data-placement="top"
                                   data-original-title="Detail Kategori">
                                    <i class="fa fa-arrow-right text-light"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full Pagination -->
@endsection

@section('js_after')
    <script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
    <script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
@endsection
