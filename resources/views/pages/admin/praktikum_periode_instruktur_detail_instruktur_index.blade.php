@extends('layouts.backend')
@section('title-page','Praktikum Asisten')

@section('css_before')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
    <div id="page-loader" class="show"></div>
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Detail Instruktur</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item" aria-current="page">Praktikum
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="{{route('admin_praktikum_periode')}}">Periode</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx"
                               href="{{route('instruktur_detail',$tahun)}}">Instruktur</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">Detail
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- Dynamic Table Periode Full Pagination -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">{{App\Model\Ilab\ilab_user::findOrfail($ins)->full_name}}</h3>
        </div>

        <!-- Donut Chart -->
        <div class="block-content">
            <div class="row row-cols-1 row-cols-md-2">
                <div class="col mb-4">
                    <div class="card">
                        <h5 class="card-title text-center">Average Penilaian Modul</h5>
                        <canvas id="myPieChart"></canvas>
                    </div>
                </div>
                <div class="col mb-4">
                    <div class="card">
                        <h5 class="card-title text-center">Average Penilaian UAP</h5>
                        <canvas id="myPieChart2"></canvas>
                    </div>
                </div>
            </div>
            <hr/>
        </div>

       
<div class="block-content d-flex justify-content-between mt-2" style="padding-top: 0;float: right;">
         
               <div style="width: 180px; padding-bottom: 40px;"></div>
                
               <form action="{{route('exportdetailnstruktur')}}" method="post">
                @csrf
                <input type="hidden" name="title" value="{{'Periode ' . $selectSemester . ' '. $selectTahun}}">
                
                @foreach ($data as $datas)
                    @if(count($datas -> ilab_user_student_class) != 0)
                       
                        <input type="hidden" name="kelas[]" value="{{$datas->full_name}}">
                        <input type="hidden" name="jumlah[]" value="{{count($datas->ilab_user_student_class)}}">

                        @foreach(App\Model\Ilab\ilab_presence::where('class_id',$datas->id)->get() as $key=>$a)

                                @foreach(App\Model\Ilab\ilab_presence_assistant_instructor::where('presence_id',$a->id)->where('user_id',$datas->ilab_user_instructor->ilab_user->id)->get() as $key=>$b)

                               
                                <input type="" hidden="" name="" value="{{$var+=$key+1}}">
                                 @endforeach

                           @endforeach
                           
                           <input type="hidden" name="kehadiran[]" value="{{$var}}">
                           <input type="" hidden="" name="" value="{{$var=0}}">


                        
                         @foreach(App\Model\Ilab\ilab_task::where('class_id',$datas->id)->where('deleted_at',null)->where('final_exam','0')->get() as $tes)  

                            @foreach(App\Model\Ilab\ilab_task_detail::where('task_id',$tes->id)->where('deleted_at',null)->where('grade','!=',0)->get() as $key=>$avg)
                            
                            <input type="" hidden="" name="" value=" {{$count = $key+1}} {{$var2 += $avg->grade }}">
                            @endforeach                                                                                       <input type="" hidden="" name="" value=" {{$temp += $count}}">
                            @endforeach
                            
                         <input type="hidden" name="avgModul[]" value="{{@round(($var2/$temp),2)}}">



                          @foreach(App\Model\Ilab\ilab_task::where('class_id',$datas->id)->where('deleted_at',null)->where('final_exam','1')->get() as $key=>$tes)
                            <input type="" hidden="" name="" value=" {{$var3=0}}{{$temp2=0}}">
                         @foreach(App\Model\Ilab\ilab_task_detail::where('task_id',$tes->id)->where('deleted_at',null)->where('grade','!=',0)->get() as $key=>$avg)
                        <input type="" hidden="" name="" value=" {{$count2 = $key+1}} {{$var3 += $avg->grade }}">
                        @endforeach 
                        <input type="" hidden="" name="" value=" {{$temp2 += $count2}}">

                        @endforeach
                        @if($var3 && $temp2 == 0)
                        <input type="hidden" name="avgUAP[]" value="0">
                        @else
                    <input type="hidden" name="avgUAP[]" value="{{@round(($var3/$temp2),2)}}">
                    @endif
                    
                    <input type="hidden" name="" value="{{$var3=0}}{{$temp2=0}}">
                          
                      
                    @endif
                @endforeach
                <button type="submit" class="btn btn-primary btn-sm" style="color: white">
                    Export
                    CSV
                </button>
            </form>
                
        </div>

        <div class="block-content block-content-full mt-2" style="padding-top: 0;">
            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->


            <table class="table table-bordered table-striped table-vcenter"
                   id="tableAssistants">
                <thead>
                <tr>
                    <th class="text-center" style="width: 5%;">No</th>
                    <th class="text-center">Kelas Instruktur</th>
                    <th class="text-center">Jumlah Praktikan</th>
                    <th class="text-center">Jumlah Kehadiran</th>
                    <th class="text-center">Average nilai modul</th>
                    <th class="text-center">Average Final Grade</th>
                </tr>
                </thead>
                <tbody>
                 <?php $i = 1;?>     
                 <input type="" hidden="" name="" value="{{$var=0}}{{$var2=0}}{{$var3=0}}{{$temp=0}}{{$temp2=0}}{{$count=0}}{{$count2=0}}">     
               @foreach($data as $datas)

               
                @if(count($datas->ilab_user_student_class) != 0)
                    <tr>
                        <td class="text-center font-size-sm">{{$i}}</td>
                        <td class="font-w600 text-center font-size-sm">
                             <input type="" hidden="" name="" value="{{$instructorClass[]=$datas->full_name}}">
                            {{$datas->full_name}}</td>
                        <td class="font-size-sm text-center">{{count($datas->ilab_user_student_class)}}</td>
                        <td class="font-size-sm text-center">

                           @foreach(App\Model\Ilab\ilab_presence::where('class_id',$datas->id)->get() as $key=>$a)

                                @foreach(App\Model\Ilab\ilab_presence_assistant_instructor::where('presence_id',$a->id)->where('user_id',$datas->ilab_user_instructor->ilab_user->id)->get() as $key=>$b)

                               
                                <input type="" hidden="" name="" value="{{$var+=$key+1}}">
                                 @endforeach

                           @endforeach
                           {{$var}}
                           <input type="" hidden="" name="" value="{{$var=0}}">
                     
                        </td>


                        <td class="font-size-sm text-center">
                           
                            @foreach(App\Model\Ilab\ilab_task::where('class_id',$datas->id)->where('deleted_at',null)->where('final_exam','0')->get() as $tes)  

                            @foreach(App\Model\Ilab\ilab_task_detail::where('task_id',$tes->id)->where('deleted_at',null)->where('grade','!=',0)->get() as $key=>$avg)
                            
                            <input type="" hidden="" name="" value=" {{$count = $key+1}} {{$var2 += $avg->grade }}">
                            @endforeach                                                                                       <input type="" hidden="" name="" value=" {{$temp += $count}}">
                            @endforeach
                            
                            {{@round(($var2/$temp),2)}}
                            <input type="" hidden="" name="" value=" {{$avgModul[]=round(($var2/$temp),2)}}">
                          
                        </td>




                        <td class="font-size-sm text-center">
                          
                           
                        @foreach(App\Model\Ilab\ilab_task::where('class_id',$datas->id)->where('deleted_at',null)->where('final_exam','1')->get() as $key=>$tes)
                                
                           <input type="" hidden="" name="" value=" {{$var3=0}}{{$temp2=0}}">
                        @foreach(App\Model\Ilab\ilab_task_detail::where('task_id',$tes->id)->where('deleted_at',null)->where('grade','!=',0)->get() as $key=>$avg)
                       
                     
                    
                            <input type="" hidden="" name="" value=" {{$count2 = $key+1}} {{$var3 += $avg->grade }}">
    
                        
                        @endforeach
                       
                            <input type="" hidden="" name="" value=" {{$temp2 += $count2}}">
                            {{round(($var3/$temp2),2)}}

                                <input type="" hidden="" name="" value=" {{$avgUAP[]=round(($var3/$temp2),2)}}">  
                                   
                                
                        @endforeach       
   
                        </td>
                    </tr>
                       <?php $i++;?>
                    @endif
               
               @endforeach



             
                </tbody>
            </table>
             @foreach($avgUAP as $key=>$a)
               <input type="" hidden="" name="" value="     {{$instructorUAP[] = $instructorClass[$key]}}
               {{$UAP[]=$avgUAP[$key]}}">
               @endforeach


              

         
          
        </div>
    </div>
    <!-- END Dynamic Table Full Pagination -->
@endsection

@section('js_after')
    <script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
    <script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    <script src="{{asset('js/plugins/chart.js/Chart.min.js')}}"></script>

    <script>
        $('#tableAssistants').DataTable({
            pageLength: 10,
            filter: true,
            deferRender: true,
            scrollCollapse: true,
            scroller: true
        });


        // Set new default font family and font color to mimic Bootstrap's default styling
        Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
        Chart.defaults.global.defaultFontColor = '#858796';

        // Pie Chart Example
        var ctx = document.getElementById("myPieChart");
        var myPieChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: {!! json_encode($instructorClass) !!},
                datasets: [{
                    data: {!! json_encode($avgModul) !!},
                    backgroundColor: ['#1abc9c', '#2ecc71', '#3498db', '#9b59b6', '#34495e', '#f1c40f', '#e67e22',
                        '#e74c3c', '#ecf0f1', '#95a5a6'],
                    hoverBackgroundColor: ['#16a085', '#27ae60', '#2980b9', '#8e44ad', '#2c3e50', '#f39c12', '#d35400',
                        '#c0392b', '#bdc3c7', '#7f8c8d'],
                    hoverBorderColor: "rgba(234, 236, 244, 1)",
                }],
            },
        });

        var ctx2 = document.getElementById("myPieChart2");
        var myPieChart = new Chart(ctx2, {
            type: 'doughnut',
            data: {
                labels: {!! json_encode($instructorUAP) !!},
                datasets: [{
                    data:{!! json_encode($avgUAP) !!},
                    backgroundColor: ['#1abc9c', '#2ecc71', '#3498db', '#9b59b6', '#34495e', '#f1c40f', '#e67e22',
                        '#e74c3c', '#ecf0f1', '#95a5a6'],
                    hoverBackgroundColor: ['#16a085', '#27ae60', '#2980b9', '#8e44ad', '#2c3e50', '#f39c12', '#d35400',
                        '#c0392b', '#bdc3c7', '#7f8c8d'],
                    hoverBorderColor: "rgba(234, 236, 244, 1)",
                }],
            },
        });

    </script>

@endsection
