@extends('layouts.backend')
@section('title-page','Feedback Modul')

@section('css_before')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
<div id="page-loader" class="show"></div>
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Feedback
                {{$data['periode']->jenis_penilaians == "asisten" ? "Asisten" : "Instruktur"}}</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">Feedback
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx"
                            href="{{route('admin_feedback_asisten_instruktur_idx')}}">Asisten/Instruktur</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">List
                        {{$data['periode']->jenis_penilaians == "asisten" ? "Asisten" : "Instruktur"}}
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<!-- Dynamic Table Periode Full Pagination -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title">List {{$data['periode']->nama_penilaian}}</h3>
        <button class="btn btn-primary btn-sm"
            onclick="exportTableToCSV('List {{$data['periode']->nama_penilaian}}.csv','tabel_user')">Export
            CSV</button>
    </div>
    <div class="block-content block-content-full">
        <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
        <table id="tabel_user" class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
            <thead>
                <tr>
                    <th class="d-sm-table-cell" style="width: 5%;">No</th>
                    <th class="d-sm-table-cell" style="width: 10%;">Foto</th>
                    <th class="d-sm-table-cell" style="width: 50%;">Nama
                        {{$data['periode']->jenis_penilaians == "asisten" ? "Asisten" : "Instruktur"}}</th>
                    <th class="d-sm-table-cell" style="width: 15%;">Predikat</th>
                    <th class="d-sm-table-cell" style="width: 10%;">Nilai</th>
                    <th class="d-sm-table-cell" style="width: 10%;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0;
                ?>
                @foreach ($data['data_user'] as $d)
                <tr>
                    <td class=" text-center font-w600 font-size-sm">{{$i+1}}</td>
                    <td class="text-center kolom-foto">
                        @if ($data['periode']->jenis_penilaians == "asisten")
                        <img class="img-avatar" style="width:50px; height:auto;"
                            src="{{$data['user_info'][$i]->user_name}}" alt="Foto User" height="50">
                        <p style="display: none;">{{$data['user_info'][$i]->full_name}}</p>
                        @else
                        <span>
                            <i class="fa fa-user fa-3x" aria-hidden="true"></i></span>
                        <p style="display: none;">{{$data['user_info'][$i]->full_name}}</p>
                        @endif

                    </td>
                    <td class="font-w600 font-size-sm">
                        {{$data['user_info'][$i]->full_name}}
                    </td>
                    <td class="font-w600 font-size-sm">
                        {{$data['predikat'][$d->detail_predikat_id]}}
                    </td>
                    <td class="font-w600 font-size-sm">
                        {{$d->total_nilai_user}}
                    </td>
                    <td>
                        <div class="row justify-content-center">
                            <a type="button"
                                href="{{route('admin_detail_feedback_asisten_instruktur',['id'=>$data['periode']->id,'usr' =>$d->id])}}"
                                class="btn btn-sm btn-rounded btn-secondary" data-toggle="tooltip" data-placement="top"
                                data-original-title="Detail {{$data['periode']->jenis_penilaians == "asisten"? "Asisten":"Instruktur"}}">
                                <i class="fa fa-arrow-right text-light"></i>
                            </a>
                        </div>
                    </td>
                </tr>

                <?php
                    $i++;
                ?>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END Dynamic Table Full Pagination -->
@endsection

@section('js_after')
<script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
<script src="{{asset('js/custom/table2csv.js')}}"></script>
<script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
@if(Session::get('berhasil'))
<script>
    Swal.fire(
        '{{Session::get('berhasil')}}',
        '',
        'success'
    )
</script>
@endif
@endsection
