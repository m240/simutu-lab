@extends('layouts.backend')
@section('title-page','Praktikum Asisten')

@section('css_before')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
    <div id="page-loader" class="show"></div>
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">{{'Periode ' . ucfirst($selectSemester . ' '. $selectTahun)}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item" aria-current="page">Praktikum
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="{{route('admin_praktikum_periode')}}">Periode</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">Asisten
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- Dynamic Table Periode Full Pagination -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Rekap Assistensi</h3>
        </div>

        <!-- Donut Chart -->
        <div class="block-content">
            <div class="row row-cols-1 row-cols-md-2">
                <div class="col mb-4">
                    <div class="card">
                        <h5 class="card-title text-center">Kehadiran Terendah</h5>
                        <canvas id="myPieChart2"></canvas>
                    </div>
                </div>
                <div class="col mb-4">
                    <div class="card">
                        <h5 class="card-title text-center">Kehadiran Tertinggi</h5>
                        <canvas id="myPieChart"></canvas>
                    </div>
                </div>
            </div>
            <hr/>
        </div>

        <div class="block-content d-flex justify-content-between mt-2" style="padding-top: 0;">
            <select class="form-control" id="exampleFormControlSelect1" style="width: 180px;">
                <option value="">Filter Angkatan</option>
                <option value="{{$selectTahun -1}}">{{$selectTahun -1}}</option>
                <option value="{{$selectTahun -2}}">{{$selectTahun -2}}</option>
                <option value="{{$selectTahun -3}}">{{$selectTahun -3}}</option>
                <option value="{{$selectTahun -4}}">{{$selectTahun -4}}</option>
            </select>

            <form action="{{route("admin_praktikum_allasisten")}}" method="post">
                @csrf
                <input type="hidden" name="title" value="{{'Periode ' . $selectSemester . ' '. $selectTahun}}">
                @foreach ($allAssistants as $assistant)
                    @if(count($assistant -> ilab_user_assistant_class) > 0)
                        <input type="hidden" name="nim[]" value="{{$assistant -> ilab_user -> user_name}}">
                        <input type="hidden" name="nama[]" value="{{$assistant -> ilab_user -> full_name}}">
                        <input type="hidden" name="jumlah[]" value="{{count($assistant -> ilab_user_assistant_class)}}">
                        <input type="hidden" name="avgModul[]" value="{{round($assistant->ilab_task_detail->where('ilab_task', '!=', null)->avg('grade'), 2)}}">
                        <input type="hidden" name="avgKehadiran[]" value="{{round(count($assistant->ilab_user->ilab_presence_assistant_instructor
                                    ->where('ilab_presence_count', '!=', 0)) / count($assistant -> ilab_user_assistant_class), 2)}}">
                    @endif
                @endforeach
                <button type="submit" class="btn btn-primary btn-sm" style="color: white">
                    Export
                    CSV
                </button>
            </form>
        </div>


        <div class="block-content block-content-full mt-2" style="padding-top: 0;">
            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter"
                   id="tableAssistants">
                <thead>
                <tr>
                    <th class="text-center" style="width: 5%;">No</th>
                    <th class="text-center">NIM</th>
                    <th class="text-center">Asisten</th>
                    <th class="text-center">Jumlah Asistensi Kelas</th>
                    <th class="text-center">Average nilai modul</th>
                    <th class="text-center">Average Kehadiran</th>
                    <th class="text-center">Persentase Kehadiran</th>
                    <th class="text-center d-sm-table-cell" style="width: 10%;">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1;?>
                @foreach ($allAssistants as $assistant)
                    @if(count($assistant -> ilab_user_assistant_class) > 0)
                        <tr>
                            <td class="text-center font-size-sm">{{$i}}</td>
                            <td class="font-w600 text-center font-size-sm">{{$assistant -> ilab_user -> user_name}}</td>
                            <td class="font-size-sm text-center">
                                {{$assistant -> ilab_user -> full_name}}
                            </td>
                            <td class="font-size-sm text-center">
                                {{count($assistant -> ilab_user_assistant_class)}}
                            </td>
                            <td class="font-size-sm text-center">
                                {{round($assistant->ilab_task_detail->where('ilab_task', '!=', null)->avg('grade'), 2)}}
                            </td>
                            <td class="font-size-sm text-center">
                                {{--                                SOON--}}
                                {{round(count($assistant->ilab_user->ilab_presence_assistant_instructor
                                    ->where('ilab_presence_count', '!=', 0)) / count($assistant -> ilab_user_assistant_class), 2)}}
                            </td>
                            <td class="font-size-sm text-center">
                                {{round(count($assistant->ilab_user->ilab_presence_assistant_instructor
                                    ->where('ilab_presence_count', '!=', 0)) / count($assistant -> ilab_user_assistant_class) / 6 * 100, 2)}}
                            </td>
                            <td>
                                <div class="row justify-content-center">
                                    <a type="button"
                                       href="{{route('admin_praktikum_periode_detail_asisten', ['id' => $index, 'idasisten'=>$assistant->id, 'iduser'=>$assistant->user_id])}}"
                                       class="btn btn-sm btn-rounded btn-secondary" data-toggle="tooltip"
                                       data-placement="top"
                                       data-original-title="Detail Penilaian">
                                        <i class="fa fa-arrow-right text-light"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php $i++;?>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- END Dynamic Table Full Pagination -->
@endsection

@section('js_after')
    <script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
    <script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    <script src="{{asset('js/plugins/chart.js/Chart.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            oTable = $('#tableAssistants').dataTable()
            {
                order: [[0, "asc"]]
                $('#exampleFormControlSelect1').on('change', function () {
                    var selectedValue = $(this).val();

                    oTable.fnFilter(selectedValue); //Exact value, column, reg
                });
            }
        })

        Chart.defaults.global.legend.display = false;
        var ctx = document.getElementById('myPieChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: {!! json_encode($nameAsc) !!},
                datasets: [{
                    label: 'Average',
                    data: {!! json_encode($resultAsc) !!},
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            stepSize: 1
                        }
                    }]
                }
            }
        });

        var ctx = document.getElementById('myPieChart2').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: {!! json_encode($nameDesc) !!},
                datasets: [{
                    label: 'Average',
                    data: {!! json_encode($resultDesc) !!},
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            stepSize: 1
                        }
                    }]
                }
            }
        });
    </script>

@endsection
