@extends('layouts.backend')
@section('title-page','Periode')

@section('css_before')
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
<div id="page-loader" class="show"></div>
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Periode</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{route('adm1n.dashboard.index')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">Periode
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<!-- Dynamic Table Periode Full Pagination -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title">List Periode</h3>
        <button type="button" class="btn btn-sm btn-success mr-1" data-toggle="modal"
            data-target="#modal-tambah-periode">
            <i class="fa fa-fw fa-plus mr-1"></i> Tambah Periode
        </button>
    </div>
    <div class="block-content block-content-full">
        <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
        <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination js-table-checkable">
            <thead>
                <tr>
                    <th class="text-center" style="width: 5%;" id="clm_centang_all">
                        <div class="custom-control custom-checkbox d-inline-block">
                            <input type="checkbox" class="custom-control-input" id="check-all" name="check-all">
                            <label class="custom-control-label" for="check-all"></label>
                        </div>
                    </th>
                    <th class="text-center" style="width: 5%;">No</th>
                    <th class="d-sm-table-cell" style="width: 23%;">Nama</th>
                    <th class="d-sm-table-cell" style="width: 10%;">Tahun</th>
                    <th class="d-sm-table-cell" style="width: 10%;">Semester</th>
                    <th class="d-sm-table-cell" style="width: 10%;">Aktif</th>
                    <th class="d-sm-table-cell" style="width: 25%;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 1;
                ?>
                @foreach ($data['periode'] as $d)
                <tr>
                    <td class="text-center">
                        <div class="custom-control custom-checkbox d-inline-block">
                            <input type="checkbox" class="custom-control-input" data-id="{{$d->id}}" id="cek_row_{{$i}}"
                                name="cek_row[{{$d->id}}]">
                            <label class="custom-control-label" for="cek_row_{{$i}}"></label>
                        </div>
                    </td>
                    <td class="text-center font-size-sm">{{$i}}</td>
                    <td class="font-w600 font-size-sm">
                        {{$d->nama_penilaian}}
                    </td>
                    <td class="d-sm-table-cell font-size-sm">
                        {{$d->tahun}}
                    </td>
                    <td class="d-sm-table-cell">
                        {{ucfirst($d->semester)}}
                    </td>
                    <td id="kolom_status{{$d->id}}">
                        <div class="row justify-content-center">
                            <div class="btn-group btn-group-sm" role="group" aria-label="Small">
                                @if($d->active)
                                <button type="button" class="btn btn-success">Yes</button>
                                <button type="button" class="btn btn-outline-danger btn-status-nonaktif"
                                    data-id="{{$d->id}}">No</button>
                                <div class="spinner-border spinner-border-sm text-primary" role="status"
                                    style="display: none;">
                                    <span class="sr-only">Loading...</span>
                                </div>
                                @else
                                <button type="button" class="btn btn-outline-success btn-status-aktif"
                                    data-id="{{$d->id}}">Yes</button>
                                <div class="spinner-border spinner-border-sm text-primary" role="status"
                                    style="display: none;">
                                    <span class="sr-only">Loading...</span>
                                </div>
                                <button type="button" class="btn btn-danger">No</button>
                                @endif
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row justify-content-center">
                            <a type="button" href="{{route('proses_periode_penilaian',['id'=>$d->id])}}"
                                class="btn btn-sm btn-rounded btn-secondary mr-1 btn-proc" data-toggle="tooltip"
                                data-placement="top" data-original-title="Proses Penilaian">
                                <i class="fa fa-arrow-right text-light"></i>
                            </a>
                            @if($d->jenis_penilaians == 'asisten' || $d->jenis_penilaians == 'instruktur')
                            {{-- TOMBOL PREDIKAT --}}
                            <a type="button" href="{{route('predikat_detail_byId',['id'=>$d->predikat_id])}}"
                                class="btn btn-sm btn-rounded btn-info mr-1" data-toggle="tooltip" data-placement="top"
                                title="" data-original-title="Detail Predikat">
                                <i class="fa fa-star text-light"></i>
                            </a>
                            @else
                            <a type="button" href="{{route('adm1n_periode_temp_comments',['id'=>$d->id])}}"
                                class="btn btn-sm btn-rounded btn-info ml-1 mr-1"
                                data-toggle="tooltip" data-placement="top" title=""
                                data-original-title="Feedback Komentar Sementara">
                                <i class="fa fa-comments text-light"></i>
                            </a>
                            @endif
                            {{-- TOMBOL KRITERIA --}}
                            <a type="button" href="{{route('admin_detail_kriteria_idx',['id'=>$d->kriteria_id])}}"
                                class="btn btn-sm btn-rounded btn-info ml-1 mr-1" data-id="{{$d->id}}"
                                data-toggle="tooltip" data-placement="top" title=""
                                data-original-title="Detail Kriteria">
                                <i class="fa fa-list text-light"></i>
                            </a>
                            <div data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                <button type="button" class="btn btn-sm btn-rounded btn-success ml-1 mr-1"
                                    data-id="{{$d->id}}" data-nama="{{$d->nama_penilaian}}" data-tahun="{{$d->tahun}}"
                                    data-semester="{{$d->semester}}" data-jenis="{{$d->jenis_penilaians}}"
                                    {{-- @if(is_null($data['kriteria']['id'])) --}} data-kritid="{{$d->kriteria_id}}"
                                    data-kritnama="{{$data['kriteria']['nama'][array_search($d->kriteria_id,$data['kriteria']['id'])]}}"
                                    {{-- @endif --}} @if(!empty($data['predikat']['id']))
                                    data-predid="{{$d->predikat_id}}"
                                    data-prednama="{{$data['predikat']['nama'][array_search($d->kriteria_id,$data['predikat']['id'])]}}"
                                    @endif data-clasid="{{$d->ilab_class_category_id}}"
                                    data-clasnama="{{$data['class_category']['category_name'][array_search($d->ilab_class_category_id,$data['class_category']['id'])]}}"
                                    data-toggle="modal" data-target="#modal-edit-periode">
                                    <i class="fa fa-pencil-alt"></i>
                                </button>
                            </div>                            
                            <button type="button" class="btn btn-sm btn-rounded btn-danger ml-1 btn-hapus-periode"
                                data-id="{{$d->id}}" data-toggle="tooltip" data-placement="top" title=""
                                data-original-title="Hapus">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <?php
                    $i++;
                ?>
                @endforeach
            </tbody>
        </table>
        <button id="aktif-sekumpulan" class="btn btn-success mt-2 mr-2">Aktifkan</button>
        <button id="non-aktif-sekumpulan" class="btn btn-danger mt-2 ">Non-Aktifkan</button>
    </div>
</div>
<!-- END Dynamic Table Full Pagination -->

<!-- Modal Tambah -->
<div class="modal fade" id="modal-tambah-periode" tabindex="-1" role="dialog" aria-labelledby="modal-block-popout"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-popout modal-lg" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Tambah Periode</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content font-size-sm">
                    {{-- form input --}}
                    <form id="form-tambah-periode" action="{{route('adm1n_dashboard_periode_tambah')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="example-text-input">Tahun <span class="text-danger">*</span></label>
                            <input type="text" class="js-maxlength form-control" id="tahun_tambah" name="tahun"
                                minlength="4" maxlength="4" placeholder="Tahun" data-always-show="true" required="">
                            <div class="alert alert-danger alert-dismissable" id="err_tahun_tambah"
                                style="display: none;" role="alert">
                                <p class="mb-0">Tahun harus angka!</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="d-block">Semester <span class="text-danger">*</span></label>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="ganjil" name="semester" value="1"
                                    checked>
                                <label class="custom-control-label" for="ganjil">Ganjil</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="genap" name="semester" value="2">
                                <label class="custom-control-label" for="genap">Genap</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Jenis Penilaian <span class="text-danger">*</span></label>
                            <select class="custom-select" id="jenis_penilaian" name="jenis_penilaian"
                                onchange="jenisChange(this)">
                                <option value="0">Asisten</option>
                                <option value="1">Instruktur</option>
                                <option value="2">Fasilitas Lab</option>
                                <option value="3">Modul</option>
                            </select>
                        </div>
                        <div class="form-group" id="blok_kriteria_tambah">
                            <label>Kriteria Penilaian <span class="text-danger">*</span></label>
                            <select class="js-select2 form-control" id="kriteria" name="kriteria" style="width: 100%;"
                                data-placeholder="Pilih Kriteria..">
                                <option></option>
                                <!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                <?php $ik=0 ?>
                                @foreach ($data['kriteria']['id'] as $d)
                                <option value="{{$d}}">{{$data['kriteria']['nama'][$ik]}}</option>
                                <?php $ik++?>
                                @endforeach
                            </select>
                            <div id="kriteria-error" class="animated fadeIn text-danger" style="display: none;">Kriteria
                                harus di isi</div>
                            <div id="kriteria-blm-ada" class="animated fadeIn text-danger" style="display: none;">
                                Tambahkan kriteria terlebih dahulu</div>
                        </div>
                        <div class="form-group" id="blok_predikat_tambah">
                            <label>Predikat Penilaian <span class="text-danger">*</span></label>
                            <select class="js-select2 form-control" id="predikat" name="predikat" style="width: 100%;"
                                data-placeholder="Pilih Predikat..">
                                <option></option>
                                <!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                <?php $ip=0 ?>
                                @foreach ($data['predikat']['id'] as $d)
                                <option value="{{$d}}">{{$data['predikat']['nama'][$ip]}}</option>
                                <?php $ip++?>
                                @endforeach
                            </select>
                            <div id="predikat-error" class="animated fadeIn text-danger" style="display: none;">Predikat
                                harus di isi</div>
                            <div id="predikat-blm-ada" class="animated fadeIn text-danger" style="display: none;">
                                Tambahkan predikat terlebih dahulu</div>
                        </div>
                        <div class="form-group" id="blok_classCategory_tambah" style="display: none;">
                            <label>Kategori Kelas <span class="text-danger">*</span></label>
                            <select class="js-select2 form-control" id="classcategory" name="classcategory"
                                style="width: 100%;" data-placeholder="Pilih Kategori Kelas..">
                                <option></option>
                                <!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                <?php $ic=0 ?>
                                @foreach ($data['class_category']['id'] as $d)
                                <option value="{{$d}}">{{$data['class_category']['category_name'][$ic]}}</option>
                                <?php $ic++ ?>
                                @endforeach
                            </select>
                            <div id="class-error" class="animated fadeIn text-danger" style="display: none;">Kategori
                                kelas harus di isi</div>
                        </div>
                </div>
                <div class="block-content block-content-full text-right border-top">
                    <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check mr-1"></i>Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Modal Tambah -->

<!-- Modal Edit -->
<div class="modal fade" id="modal-edit-periode" tabindex="-1" role="dialog" aria-labelledby="modal-block-popout"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-popout modal-lg" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Edit Periode</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content font-size-sm">
                    {{-- form input --}}
                    <form id="form-edit-periode" action="{{route('adm1n_dashboard_periode_edit')}}" method="POST">
                        @csrf
                        <input type="hidden" name="id" id="id">
                        <div class="form-group">
                            <label for="example-text-input">Nama Periode <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="nama" placeholder="Nama Periode" required=""
                                disabled>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input">Tahun <span class="text-danger">*</span></label>
                            <input type="text" class="js-maxlength form-control" id="tahun_edit" name="tahun"
                                minlength="4" maxlength="4" placeholder="Tahun" data-always-show="true" required="">
                            <div class="alert alert-danger alert-dismissable" id="err_tahun_edit" style="display: none;"
                                role="alert">
                                <p class="mb-0">Tahun harus angka!</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="d-block">Semester <span class="text-danger">*</span></label>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="ganjil_edit" name="semester"
                                    value="1">
                                <label class="custom-control-label" for="ganjil_edit">Ganjil</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="genap_edit" name="semester"
                                    value="2">
                                <label class="custom-control-label" for="genap_edit">Genap</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Jenis Penilaian <span class="text-danger">*</span></label>
                            <select onchange="jenisChangeE(this)" id="jenis_penilaianE" class="custom-select"
                                name="jenis_penilaian">
                                <option value="0">Asisten</option>
                                <option value="1">Instruktur</option>
                                <option value="2">Fasilitas Lab</option>
                                <option value="3">Modul</option>
                            </select>
                        </div>
                        <div class="form-group" id="blok_kriteria_tambahE">
                            <label>Kriteria Penilaian <span class="text-danger">*</span></label>
                            <select class="js-select2 form-control" id="kriteriaE" name="kriteria" style="width: 100%;"
                                data-placeholder="Pilih Kriteria..">
                                <option></option>
                                <!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                <?php $ik=0 ?>
                                @foreach ($data['kriteria']['id'] as $d)
                                <option value="{{$d}}">{{$data['kriteria']['nama'][$ik]}}</option>
                                <?php $ik++?>
                                @endforeach
                            </select>
                            <div id="kriteriaE-error" class="animated fadeIn text-danger" style="display: none;">
                                Kriteria
                                harus di isi</div>
                        </div>
                        <div class="form-group" id="blok_predikat_tambahE">
                            <label>Predikat Penilaian <span class="text-danger">*</span></label>
                            <select class="js-select2 form-control" id="predikatE" name="predikat" style="width: 100%;"
                                data-placeholder="Pilih Predikat..">
                                <option></option>
                                <!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                <?php $ip=0 ?>
                                @foreach ($data['predikat']['id'] as $d)
                                <option value="{{$d}}">{{$data['predikat']['nama'][$ip]}}</option>
                                <?php $ip++?>
                                @endforeach
                            </select>
                            <div id="predikatE-error" class="animated fadeIn text-danger" style="display: none;">
                                Predikat
                                harus di isi</div>
                        </div>
                        <div class="form-group" id="blok_classCategory_tambahE" style="display: none;">
                            <label>Kategori Kelas <span class="text-danger">*</span></label>
                            <select class="js-select2 form-control" id="classcategoryE" name="classcategory"
                                style="width: 100%;" data-placeholder="Pilih Kategori Kelas..">
                                <option></option>
                                <!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                <?php $ic=0 ?>
                                @foreach ($data['class_category']['id'] as $d)
                                <option value="{{$d}}">{{$data['class_category']['category_name'][$ic]}}</option>
                                <?php $ic++ ?>
                                @endforeach
                            </select>
                            <div id="classE-error" class="animated fadeIn text-danger" style="display: none;">Kategori
                                kelas harus di isi</div>
                        </div>
                </div>
                <div class="block-content block-content-full text-right border-top">
                    <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check mr-1"></i>Simpan</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Modal Edit -->
@endsection

@section('js_after')
<!-- Page JS Plugins -->
<script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/buttons.print.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/plugins/es6-promise/es6-promise.auto.min.js')}}"></script>
<script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('js/plugins/select2/js/select2.full.min.js')}}"></script>
<script src="{{asset('js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
<script src="{{asset('js/pages/periode.all.js')}}"></script>

<!-- Page JS Code -->
<script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
<script>
    jQuery(function(){ One.helpers(['table-tools-checkable']); });
</script>
<script>
    $(function(){ One.helpers(['maxlength','select2']); });

    var route = {
    active : '{{route('adm1n_dashboard_periode_nonaktifkan')}}',
    nonactive : '{{route('adm1n_dashboard_periode_aktifkan')}}',
    delete : '{{route('adm1n_dashboard_periode_hapus')}}',
    nonactive_sebagian : '{{route('adm1n_nonaktif_sebagian_periode')}}',
    active_sebagian : '{{route('adm1n_aktif_sebagian_periode')}}'
    }

    function jenisChange(opt){
        if(opt.value == "0" || opt.value == "1"){
            $('#blok_predikat_tambah').show()
            $('#blok_classCategory_tambah').hide()
        }else if(opt.value == "3"){
            $('#blok_classCategory_tambah').show()
            $('#blok_predikat_tambah').hide()
        }else{
            $('#blok_classCategory_tambah').hide()
            $('#blok_predikat_tambah').hide()
        }
    }

    $(document).on('submit','#form-tambah-periode',function(){
        @if(count($data['kriteria']['id']) == 0 || count($data['predikat']['id']) == 0)
        if($('#jenis_penilaian').val() != "2"){
            event.preventDefault()
        }
        @endif

        if($('#kriteria').val() == ""){
            $('#kriteria-error').show()
            event.preventDefault()
        }else{
            $('#kriteria-error').hide()
        }
        if($('#jenis_penilaian').val() == "0" || $('#jenis_penilaian').val() == "1"){
            if($('#predikat').val() == ""){
                $('#predikat-error').show()
                event.preventDefault()
            }else{
                $('#predikat-error').hide()
            }
        }else if($('#jenis_penilaian').val() == "3" ){
            if($('#classcategory').val() == ""){
                $('#class-error').show()
                event.preventDefault()
            }else{
                $('#class-error').hide()
            }
        }
    })

    function jenisChangeE(opt){
        if(opt.value == "0" || opt.value == "1"){
            $('#blok_predikat_tambahE').show()
            $('#blok_classCategory_tambahE').hide()
        }else if(opt.value == "3"){
            $('#blok_classCategory_tambahE').show()
            $('#blok_predikat_tambahE').hide()
        }else{
            $('#blok_classCategory_tambahE').hide()
            $('#blok_predikat_tambahE').hide()
        }
    }

    $(document).on('submit','#form-edit-periode',function(){
        if($('#kriteriaE').val() == ""){
            $('#kriteriaE-error').show()
            event.preventDefault()
        }else{
            $('#kriteriaE-error').hide()
        }
        if($('#jenis_penilaianE').val() == "0" || $('#jenis_penilaianE').val() == "1"){
            if($('#predikatE').val() == ""){
                $('#predikatE-error').show()
                event.preventDefault()
            }else{
                $('#predikatE-error').hide()
            }
        }else if($('#jenis_penilaianE').val() == "3" ){
            if($('#classcategoryE').val() == ""){
                $('#classE-error').show()
                event.preventDefault()
            }else{
                $('#classE-error').hide()
            }
        }
    })

    $(document).ready(function(){
        @if(count($data['kriteria']['id']) == 0)
        $('#kriteria-blm-ada').show()
        @endif
        @if(count($data['predikat']['id']) == 0)
        $('#predikat-blm-ada').show()
        @endif

    })

    function afterDelete() {
        location.reload();
    }


</script>

@if(Session::get('berhasil'))
<script>
    Swal.fire(
        '{{Session::get('berhasil')}}',
        '',
        'success'
    )
</script>
@endif
@if(Session::get('gagal'))
<script>
    Swal.fire(
        '{{Session::get('gagal')}}',
        '',
        'error'
    )
</script>
@endif
@endsection
