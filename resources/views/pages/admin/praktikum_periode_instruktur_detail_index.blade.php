@extends('layouts.backend')
@section('title-page','Praktikum Asisten')

@section('css_before')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
<div id="page-loader" class="show"></div>
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">{{'Periode ' . ucfirst($selectSemester . ' '. $selectTahunAkhir)}}</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">Praktikum
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{route('instruktur_index')}}">Periode</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">Instruktur
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<!-- Dynamic Table Periode Full Pagination -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title">Rekap Instruktur</h3>
    </div>

    <!-- Donut Chart -->
    <div class="block-content">
        <div class="row row-cols-1 row-cols-md-2">
            <div class="col mb-4">
                <div class="card">
                    <h5 class="card-title text-center">Kehadiran Terendah</h5>
                    <canvas id="myPieChart2"></canvas>
                </div>
            </div>
            <div class="col mb-4">
                <div class="card">
                    <h5 class="card-title text-center">Kehadiran Tertinggi</h5>
                    <canvas id="myPieChart"></canvas>
                </div>
            </div>
        </div>
        <hr/>
    </div>



    <div class="block-content d-flex justify-content-between mt-2" style="padding-top: 0;">


        <div style="width: 180px;"></div>
        <form action="{{route('exportallInstruktur')}}" method="post">
            @csrf
            <input type="hidden" name="title" value="{{'Periode ' . $selectSemester . ' '. $selectTahun}}">

             @foreach($allinstructor as $datas)
               <input type="hidden" name="" value="{{$b=0}} "> @foreach(App\Model\Ilab\ilab_class::where('user_instructor_id',$datas->id)->whereBetween('updated_at',[$startRekap, $endRekap])->get() as $key=>$a)
               @if(count($a->ilab_user_student_class) != 0)
               <input type="hidden" name="" value="{{$b++}} ">
               @endif
               @endforeach

               @foreach(App\Model\Ilab\ilab_class::where('user_instructor_id',$datas->id)->whereBetween('updated_at',[$startRekap, $endRekap])->where('deleted_at',null)->get() as $key=>$a)
               <input type="hidden" name="" value="{{$var=0}} {{$var = $key+1}}">
               @endforeach
               @if($b > 0 && $var !=0)
                    <input type="" hidden="" name="nama[]" value="{{$datas->ilab_user->full_name}}   ">                                                     
                <input type="" hidden="" name="jumlah[]" value="{{$b}}"> <input type="" hidden="" name="" value="{{$temp=$b}}">
                
                    <input type="" hidden="" name="" value="{{$var=0}}">

                    @foreach(App\Model\Ilab\ilab_class::where('user_instructor_id',$datas->id)->whereBetween('updated_at',[$startRekap, $endRekap])->get() as $a)
                    @foreach(App\Model\Ilab\ilab_presence::where('class_id',$a->id)->get() as $b)
                    @foreach(App\Model\Ilab\ilab_presence_assistant_instructor::where('presence_id',$b->id)->where('user_id',$a->ilab_user_instructor->ilab_user->id)->get() as $key=>$c)
                    <input type="" hidden="" name="" value="{{$var+=$key+1}}">
                    @endforeach
                    @endforeach
                    @endforeach     
            
                    
                    <input type="" hidden="" name="avgKehadiran[]" value="{{round($var/$temp,2)}}">

        @endif
        @endforeach

            <button type="submit" class="btn btn-primary btn-sm" style="color: white">
                Export
                CSV
            </button>
        </form>


    </div>
    <div class="block-content block-content-full mt-2" style="padding-top: 0;">
        <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
        <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
            <thead align="center">
                <tr>
                    <th class="text-center" style="width: 5%;">No</th>
                    <th class="d-sm-table-cell" style="width: 25%;">Nama Instructor</th>
                    <th class="d-sm-table-cell" style="width: 15%;">Kelas Di Ampu</th>
                    <th class="d-sm-table-cell" style="width: 15%;">Average Kehadiran</th>
                    <th class="d-sm-table-cell" style="width: 15%;">Persentase Kehadiran</th>
                    <th class="d-sm-table-cell" style="width: 15%;">Action</th>
                </tr>
            </thead>
            <tbody align="center">


               <?php
               $i = 1;
               ?>
               @foreach($allinstructor as $datas)
               <input type="hidden" name="" value="{{$b=0}} "> @foreach(App\Model\Ilab\ilab_class::where('user_instructor_id',$datas->id)->whereBetween('updated_at',[$startRekap, $endRekap])->get() as $key=>$a)
               @if(count($a->ilab_user_student_class) != 0)
               <input type="hidden" name="" value="{{$b++}} ">
               @endif
               @endforeach

               @foreach(App\Model\Ilab\ilab_class::where('user_instructor_id',$datas->id)->whereBetween('updated_at',[$startRekap, $endRekap])->where('deleted_at',null)->get() as $key=>$a)
               <input type="hidden" name="" value="{{$var=0}} {{$var = $key+1}}">
               @endforeach
               @if($b > 0 && $var !=0)
               <tr id="baris{{$datas->id}}">
                  <td>{{$i}}</td>
                  <td class="font-w600 font-size-sm">
                    {{$datas->ilab_user->full_name}}                                                        
                    <input type="" hidden="" name="" value="{{$nama[]=$datas->ilab_user->full_name}}">
                    </td>

                <td>
                {{$b}} <input type="" hidden="" name="" value="{{$temp=$b}}"></td>
                <td>
                    <input type="" hidden="" name="" value="{{$var=0}}">

                    @foreach(App\Model\Ilab\ilab_class::where('user_instructor_id',$datas->id)->whereBetween('updated_at',[$startRekap, $endRekap])->get() as $a)
                    @foreach(App\Model\Ilab\ilab_presence::where('class_id',$a->id)->get() as $b)
                    @foreach(App\Model\Ilab\ilab_presence_assistant_instructor::where('presence_id',$b->id)->where('user_id',$a->ilab_user_instructor->ilab_user->id)->get() as $key=>$c)
                    <input type="" hidden="" name="" value="{{$var+=$key+1}}">
                    @endforeach
                    @endforeach
                    @endforeach     
                {{round($var/$temp,2)}}
                     <input type="" hidden="" name="" value="{{$avgKeh[]=round($var/$temp,2)}}">



                </td>
                <td>
                    <input type="" hidden="" name="" value="{{$var=0}}">

                    @foreach(App\Model\Ilab\ilab_class::where('user_instructor_id',$datas->id)->whereBetween('updated_at',[$startRekap, $endRekap])->get() as $a)
                    @foreach(App\Model\Ilab\ilab_presence::where('class_id',$a->id)->get() as $b)
                    @foreach(App\Model\Ilab\ilab_presence_assistant_instructor::where('presence_id',$b->id)->where('user_id',$a->ilab_user_instructor->ilab_user->id)->get() as $key=>$c)
                    <input type="" hidden="" name="" value="{{$var+=$key+1}}">
                    @endforeach
                    @endforeach
                    @endforeach     
                {{round(($var/$temp)/6*100,2)}}
                    
                </td>
                <td> 
                    <div class="row justify-content-center">
                        <a type="button"
                        href="

                        {{route('instruktur_user_detail', ['idclass'=>$idclass, 'ins'=>$datas->user_id,'id' => $datas->id])}}"


                        class="btn btn-sm btn-rounded btn-secondary" data-toggle="tooltip" data-placement="top"
                        data-original-title="Detail Instruktur">

                        <i class="fa fa-arrow-right text-light"></i>
                    </a>
                </div>
            </td>

        </tr>
        <?php $i++; ?>
        @endif
        @endforeach


        <?php

        unset($avgKeh[0]);
        unset($nama[0]);
        $arrayCombine = array();
        foreach ($nama as $i => $ass) {
            $arrayCombine[] = array("avgKeh" => $avgKeh[$i], "nama" => $nama[$i]);
        }

        // Sort
        $arrayCombineAsc = bubbleSortAsc($arrayCombine);
        $arrayCombineDesc = bubbleSortDesc($arrayCombine);

        // Split Array Ascending
        $nameAsc = array();
        $resultAsc = array();
        foreach ($arrayCombineAsc as $i => $key) {
            if ($i <= 5) {
                $resultAsc[] = $arrayCombineAsc[$i]['avgKeh'];
                $nameAsc[] = $arrayCombineAsc[$i]['nama'];
            }
        }

        // Split Array Descending
        $nameDesc = array();
        $resultDesc = array();
        foreach ($arrayCombineDesc as $i => $key) {
            if ($i <= 5) {
                $resultDesc[] = $arrayCombineDesc[$i]['avgKeh'];
                $nameDesc[] = $arrayCombineDesc[$i]['nama'];
            }
        }

        function bubbleSortAsc($arr)
    {
        $n = sizeof($arr);

        for ($i = 0; $i < $n; $i++) {
            for ($j = 0; $j < $n - $i - 1; $j++) {
                if ($arr[$j] < $arr[$j + 1]) {
                    $t = $arr[$j];
                    $arr[$j] = $arr[$j + 1];
                    $arr[$j + 1] = $t;
                }
            }
        }
        return $arr;
    }

    function bubbleSortDesc($arr)
    {
        $n = sizeof($arr);

        for ($i = 0; $i < $n; $i++) {
            for ($j = 0; $j < $n - $i - 1; $j++) {
                if ($arr[$j] > $arr[$j + 1]) {
                    $t = $arr[$j];
                    $arr[$j] = $arr[$j + 1];
                    $arr[$j + 1] = $t;
                }
            }
        }
        return $arr;
    }

         ?>

    </tbody>
</table>


</div>
</div>


<!-- END Dynamic Table Full Pagination -->
@endsection

@section('js_after')
<script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
<script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('js/plugins/chart.js/Chart.min.js')}}"></script>

<script>
    $(document).ready(function () {
        oTable = $('#tableAssistants').dataTable()
        {
            order: [[0, "asc"]]
            $('#exampleFormControlSelect1').on('change', function () {
                var selectedValue = $(this).val();

                    oTable.fnFilter(selectedValue); //Exact value, column, reg
                });
        }
    })

    Chart.defaults.global.legend.display = false;
    var ctx = document.getElementById('myPieChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        
        data: {
            labels: {!! json_encode($nameAsc) !!},
            datasets: [{
                label: 'Average',
                data: {!! json_encode($resultAsc) !!},
                backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1,
                        max:6
                        
                    }
                }]
            }
        }
    });

    var ctx = document.getElementById('myPieChart2').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: {!! json_encode($nameDesc) !!},
            datasets: [{
                label: 'Average',
                data: {!! json_encode($resultDesc) !!},
                backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1,
                        max:6
                    }
                }]
            }
        }
    });
</script>

@endsection
