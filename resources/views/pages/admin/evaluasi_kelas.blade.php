@extends('layouts.backend')
@section('title-page','Evaluasi Kelas')

@section('css_before')
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
<div id="page-loader" class="show"></div>
<div class="bg-body-light">
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Evaluasi Kelas
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="{{route('adm1n.dashboard.index')}}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">Evaluasi Kelas</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->
    <div class="content">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Tabel Data <small> Evaluasi {{Session('filter')!=null? $all_semester->where('id', Session('filter'))->first()->category_name:''}}</small></h3>
            </div>
            <div class="block-content block-content-full">
                <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <div class="row mb-2">
                    <div class="col-2">
                        <form id="myForm" action="{{route('admin_evaluasi_kelas_index_filter')}}" method="POST">
                            @csrf
                            <select name="filter" class="form-control" onchange="this.form.submit()">
                                <option value="">Filter by semester ajaran</option>
                                @foreach($all_semester as $data)
                                <option value="{{$data->id}}">{{$data->category_name}}</option>
                                @endforeach
                            </select>
                        </form>
                    </div>
                </div>
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Kategori Kelas</th>
                            <th>Kategori</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; ?>
                        @foreach($class->where('parent', '>', '0')->where('is_teori_class', '<', '1' ) as $data)
                            @if($semester->where('id', $data->parent)->isNotEmpty())
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$data->category_name}}</td>
                                <td>{{$semester->where('id', $data->parent)->first()->category_name}}</td>

                                <td>
                                    <a type="button" href="{{route('admin_evaluasi_kelas_post', $data->id)}}"
                                        class="btn btn-sm btn-rounded btn-secondary mr-1 btn-proc" data-toggle="tooltip"
                                        data-placement="top" data-original-title="Lihat Detail Evaluasi">
                                        <i class="fa fa-arrow-right text-light"></i>
                                    </a>
                                </td>
                            </tr>
                            @endif
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Dynamic Table Full Pagination -->
    </div>
    @endsection

    @section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/buttons/buttons.print.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/buttons/buttons.html5.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/buttons/buttons.flash.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/buttons/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    <script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
    @endsection
