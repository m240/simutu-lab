@extends('layouts.backend')
@section('title-page','Praktikum Asisten')

@section('css_before')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
    <div id="page-loader" class="show"></div>
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Detail Asisten</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item" aria-current="page">Praktikum
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="{{route('admin_praktikum_periode')}}">Periode</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx"
                               href="{{route('admin_praktikum_periode_detail', ['id' => $idxperiode])}}">Asisten</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">Detail
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- Dynamic Table Periode Full Pagination -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">{{$detailAssistant -> ilab_user -> full_name}}</h3>
        </div>

        <!-- Donut Chart -->
        <div class="block-content">
            <div class="row row-cols-1 row-cols-md-2">
                <div class="col mb-4">
                    <div class="card">
                        <h5 class="card-title text-center">Average Penilaian Modul</h5>
                        <canvas id="myPieChart"></canvas>
                    </div>
                </div>
                <div class="col mb-4">
                    <div class="card">
                        <h5 class="card-title text-center">Average Penilaian UAP</h5>
                        <canvas id="myPieChart2"></canvas>
                    </div>
                </div>
            </div>
            <hr/>
        </div>

        <div class="block-content d-flex justify-content-end mt-2" style="padding-top: 0;">
            <form action="{{route("admin_praktikum_detailasisten")}}" method="post">
                @csrf
                <input type="hidden" name="title" value="{{"Rekap ".$detailAssistant -> ilab_user -> full_name}}">
                @foreach ($detailAssistant -> ilab_user_assistant_class as $key => $assistant)
                        <input type="hidden" name="kelas[]" value="{{$assistant -> ilab_class -> full_name}}">
                        <input type="hidden" name="jumlahasistensi[]" value="{{$assistant -> ilab_class -> ilab_task -> sum('ilab_task_detail_count')}}">
                        <input type="hidden" name="jumlahkehadiran[]" value="{{$assistant -> ilab_class -> ilab_presence -> sum('ilab_presence_assistant_instructor_count')}}">
                        <input type="hidden" name="avgModul[]"
                               value="{{round($assistant -> ilab_class -> ilab_task -> where('final_exam', 0) -> avg('avg_modul'), 2)}}">
                        <input type="hidden" name="avgFinalGrade[]" value="{{round($assistant -> ilab_class -> ilab_task -> where('final_exam', 1) -> avg('avg_modul'), 2)}}">
                @endforeach
                <button type="submit" class="btn btn-primary btn-sm" style="color: white">
                    Export
                    CSV
                </button>
            </form>
        </div>


        <div class="block-content block-content-full mt-2" style="padding-top: 0;">
            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter"
                   id="tableAssistants">
                <thead>
                <tr>
                    <th class="text-center" style="width: 5%;">No</th>
                    <th class="text-center">Kelas Asisten</th>
                    <th class="text-center">Jumlah Praktikkan Demo</th>
                    <th class="text-center">Jumlah Kehadiran</th>
                    <th class="text-center">Average nilai modul</th>
                    <th class="text-center">Average Final Grade</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1;?>
                @foreach ($detailAssistant -> ilab_user_assistant_class as $key => $assistant)
                    <tr>
                        <td class="text-center font-size-sm">{{$i}}</td>
                        <td class="font-w600 text-center font-size-sm">{{$assistant -> ilab_class -> full_name}}</td>
                        <td class="font-size-sm text-center">
                            {{$assistant -> ilab_class -> ilab_task -> sum('ilab_task_detail_count')}}
                        </td>
                        <td class="font-size-sm text-center">
                            {{$assistant -> ilab_class -> ilab_presence -> sum('ilab_presence_assistant_instructor_count')}}
                        </td>
                        <td class="font-size-sm text-center">
                            {{round($assistant -> ilab_class -> ilab_task -> where('final_exam', 0) -> avg('avg_modul'), 2)}}
                        </td>
                        <td class="font-size-sm text-center">
                            {{round($assistant -> ilab_class -> ilab_task -> where('final_exam', 1) -> avg('avg_modul'), 2)}}
                        </td>
                    </tr>
                    <?php $i++;?>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full Pagination -->
@endsection

@section('js_after')
    <script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
    <script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    <script src="{{asset('js/plugins/chart.js/Chart.min.js')}}"></script>

    <script>
        $('#tableAssistants').DataTable({
            pageLength: 10,
            filter: true,
            deferRender: true,
            scrollCollapse: true,
            scroller: true
        });


        // Set new default font family and font color to mimic Bootstrap's default styling
        Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
        Chart.defaults.global.defaultFontColor = '#858796';

        // Pie Chart Example
        var ctx = document.getElementById("myPieChart");
        var myPieChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels:{!! json_encode($assistantClass) !!} ,
                datasets: [{
                    data: {!! json_encode($avgModul) !!},
                    backgroundColor: ['#1abc9c', '#2ecc71', '#3498db', '#9b59b6', '#34495e', '#f1c40f', '#e67e22',
                        '#e74c3c', '#ecf0f1', '#95a5a6'],
                    hoverBackgroundColor: ['#16a085', '#27ae60', '#2980b9', '#8e44ad', '#2c3e50', '#f39c12', '#d35400',
                        '#c0392b', '#bdc3c7', '#7f8c8d'],
                    hoverBorderColor: "rgba(234, 236, 244, 1)",
                }],
            },
        });

        var ctx2 = document.getElementById("myPieChart2");
        var myPieChart = new Chart(ctx2, {
            type: 'doughnut',
            data: {
                labels:{!! json_encode($assistantClass) !!} ,
                datasets: [{
                    data: {!! json_encode($avgUAP) !!},
                    backgroundColor: ['#1abc9c', '#2ecc71', '#3498db', '#9b59b6', '#34495e', '#f1c40f', '#e67e22',
                        '#e74c3c', '#ecf0f1', '#95a5a6'],
                    hoverBackgroundColor: ['#16a085', '#27ae60', '#2980b9', '#8e44ad', '#2c3e50', '#f39c12', '#d35400',
                        '#c0392b', '#bdc3c7', '#7f8c8d'],
                    hoverBorderColor: "rgba(234, 236, 244, 1)",
                }],
            },
        });

    </script>

@endsection
