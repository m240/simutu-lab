@extends('layouts.backend')
@section('title-page','Praktikum Online')

@section('css_before')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
    <div id="page-loader" class="show"></div>
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Task</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item" aria-current="page">Praktikum
                        </li>
                        <li class="breadcrumb-item" aria-current="page" >
                            <a class="link-fx" href="{{route('admin_praktikum_online')}}">Online</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page" >
                            <a class="link-fx" href="{{route('admin_praktikum_online_class',['id'=>$class->ilab_semester->id])}}">{{$class->ilab_semester->category_name}}</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page" >
                            <a class="link-fx" href="{{route('admin_praktikum_online_class_list',['id'=>$class->id])}}">{{$class->category_name}}</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">{{$class->ilab_class[0]->full_name}}
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- Dynamic Table Periode Full Pagination -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Daftar Modul Kelas {{$class->ilab_class[0]->full_name}}</h3>
        </div>
        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
                <thead>
                <tr>
                    <th class="text-center" style="width: 5%;">No</th>
                    <th class="d-sm-table-cell" style="width: 30%;">Title</th>
                    <th class="d-sm-table-cell" style="width: 50%;">Description</th>
                    <th class="d-sm-table-cell" style="width: 50%;">Status</th>
                    <th class="d-sm-table-cell" style="width: 20%;">Bukti</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($class->ilab_class[0]->ilab_task as $task)
                    <tr>
                        <td class="text-center font-size-sm">{{$loop -> iteration}}</td>
                        <td class="font-size-sm">
                            {{$task -> title}}
                        </td>
                        <td class="font-size-sm">
                            {{$task -> description}}
                        </td>
                        <td class="font-size-sm">
                            <small class="badge {{$task->statusBuktiBool ? 'badge-success' : 'badge-danger'}} text" 
                            style="font-size: 12px;"
                            >{{$task->statusBuktiStr}}</small>
                        </td>                        
                        <td>
                            @if($task->statusBuktiBool)
                                <div class="row justify-content-center">
                                    <a type="button" href="{{$task->buktiPraktikum}}" target="_blank"
                                       class="btn btn-sm btn-rounded btn-secondary" data-toggle="tooltip" download
                                       data-placement="top"
                                       data-original-title="Bukti Praktikum">
                                        <i class="fa fa-download text-light"></i>
                                    </a>
                                </div>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full Pagination -->
@endsection

@section('js_after')
    <script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
    <script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
@endsection
