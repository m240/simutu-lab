@extends('layouts.backend')
@section('title-page','Detail Evaluasi Kelas')

@section('css_before')
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
<div id="page-loader" class="show"></div>
<div class="bg-body-light">
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Evaluasi Kelas
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item">
                            Dashboard
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="{{route('admin_evaluasi_kelas_index')}}">Evaluasi Kelas</a>
                        </li>
                        <li class="breadcrumb-item">Detail Evaluasi Kelas</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->
    <div class="content">
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Visualisasi Data <small> Evaluasi Kelas</small></h3>
                <div class="block-options">
                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                        <i class="si si-refresh"></i>
                    </button>
                </div>
            </div>
            <div class="block-content block-content-full text-center">
                <div class="row">
                    <div class="col-6">
                        <div class="py-1">
                            <canvas id="myChart" class="js-chartjs-bars"></canvas>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="py-1">
                            <!-- Bars Chart Container -->
                            <canvas id="myChart2" class="js-chartjs-bars"></canvas>
                            <?php $idx = 0; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Tabel Data <small> Evaluasi Kelas</small></h3>
            </div>
            <div class="block-content block-content-full">
                <div class="row mb-2">
                    <div class="col-12">
                        <form action="{{route('admin_evaluasi_kelas_export')}}" method="POST">
                            @csrf
                            <button class="btn btn-success float-right"><i class="fas fa-file-csv"></i>&nbsp;Export
                                CSV</button>
                        </form>
                    </div>
                </div>
                <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Kelas Praktikum</th>
                            <th>Pengampu</th>
                            <th>Total Praktikan</th>
                            <th>Rata-Rata Nilai Semua Modul</th>
                            <th>Rata-Rata Nilai Ujian Praktikum</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        $idx = 0; ?>
                        @foreach($kelas_praktikum as $class)
                        <tr>
                            <td>{{$no++}}</td>
                            <td>{{$class->full_name}}</td>
                            <td>{{$pengampu[$idx++]}}</td>
                            <td>{{$total_praktikan[$idx-1]}}</td>
                            <td>{{$avgModul[$idx-1]}}</td>
                            <td>{{$avgUap[$idx-1] == 0 ? 'Nilai UAP tidak ditemukan' : $avgUap[$idx-1]}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Dynamic Table Full Pagination -->
    </div>

    @endsection

    @section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/buttons/buttons.print.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/buttons/buttons.html5.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/buttons/buttons.flash.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/buttons/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    <script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
    <script src="{{asset('js/plugins/chart.js/Chart.bundle.min.js')}}"></script>
    <script>
        var ctx = document.getElementById('myChart2');
        var myChart2 = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: [
                    @foreach($kelas_praktikum as $data)
                    'Rata-Rata UAP {{$data->short_name}}',
                    @endforeach
                ],
                datasets: [{
                    data: @json($pieChart),
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.7)',
                        'rgba(54, 162, 235, 0.7)',
                        'rgba(255, 206, 86, 0.7)',
                        'rgba(75, 192, 192, 0.7)',
                        'rgba(153, 102, 255, 0.7)',
                        'rgba(255, 159, 64, 0.7)',
                        'rgba(66, 173, 255, 0.7)',
                        'rgba(189, 255, 66,  0.7)',
                        'rgba(164, 66, 255, 0.7)',
                        'rgba(75, 192, 192, 0.7)',
                        'rgba(153, 102, 255, 0.7)',
                    ],

                    borderWidth: 4
                }]
            },
            options: {
                events: ['mousemove'], // this is needed, otherwise onHover is not fired
                onHover: (event, chartElement) => {
                    event.target.style.cursor = chartElement[0] ? 'pointer' : 'default';
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
    <script>
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [@foreach($kelas_praktikum as $kelas)
                            "{{$kelas->short_name}}",
                        @endforeach],
                datasets: [{
                    label: 'Rata-Rata Nilai Modul Dari Seluruh Kelas',
                    backgroundColor: 'rgba(62, 224, 145, 0.50)',
                    borderWidth: 2,
                    data: @json($avgModul),
                }]
            },
            options: {
                events: ['mousemove'], // this is needed, otherwise onHover is not fired
                onHover: (event, chartElement) => {
                    event.target.style.cursor = chartElement[0] ? 'pointer' : 'default';
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            max: 100
                        }
                    }]
                }
            }
        });
    </script>
    <!-- Page JS Plugins -->


    @endsection