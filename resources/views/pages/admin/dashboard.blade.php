@extends('layouts.backend')

@section('title-page', 'Dashboard')

@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Dashboard</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="">Dashboard</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="bg-image overflow-hidden" style="background-image: url('{{asset('media/quality.png')}}');">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div
                class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h1 class="font-w600 text-white mb-0 invisible" data-toggle="appear">Dashboard</h1>
                    <h2 class="h4 font-w400 text-white-75 mb-0 invisible" data-toggle="appear" data-timeout="250">
                        Welcome Administrator</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content content-narrow">
    <div class="row">
        <div class="col-6 col-md-3 col-lg-6 col-xl-3">
            <a class="block block-rounded block-link-pop border-left border-primary border-4x"
                href="javascript:void(0)">
                <div class="block-content block-content-full">
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Jumlah Penilaian Asisten</div>
                    <div class="font-size-h2 font-w400 text-dark">{{ $stats['asisten'] }}</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-3 col-lg-6 col-xl-3">
            <a class="block block-rounded block-link-pop border-left border-primary border-4x"
                href="javascript:void(0)">
                <div class="block-content block-content-full">
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Jumlah Penilaian Modul</div>
                    <div class="font-size-h2 font-w400 text-dark">{{ $stats['modul'] }}</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-3 col-lg-6 col-xl-3">
            <a class="block block-rounded block-link-pop border-left border-primary border-4x"
                href="javascript:void(0)">
                <div class="block-content block-content-full">
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Jumlah Penilaian Instruktur</div>
                    <div class="font-size-h2 font-w400 text-dark">{{ $stats['instruktur'] }}</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-3 col-lg-6 col-xl-3">
            <a class="block block-rounded block-link-pop border-left border-primary border-4x"
                href="javascript:void(0)">
                <div class="block-content block-content-full">
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Jumlah Fasilitas Laboratorium</div>
                    <div class="font-size-h2 font-w400 text-dark">{{ $stats['lab'] }}</div>
                </div>
            </a>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-6">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Kontribusi Terbaru 5 Modul Tertinggi</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-toggle="block-option"
                            data-action="state_toggle" data-action-mode="demo">
                            <i class="si si-refresh"></i>
                        </button>
                        <button type="button" class="btn-block-option">
                            <i class="si si-settings"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content p-0 text-center">
                    <div class="pt-3" >
                        <canvas id="js-modul-pie-high" class="js-chartjs-pie chartjs-render-monitor"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Capaian Terbaru 5 Modul Terendah</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-toggle="block-option"
                            data-action="state_toggle" data-action-mode="demo">
                            <i class="si si-refresh"></i>
                        </button>
                        <button type="button" class="btn-block-option">
                            <i class="si si-settings"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content p-0 text-center">
                    <div class="pt-3" >
                        <canvas id="js-modul-pie-low" class="js-chartjs-pie chartjs-render-monitor"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Presensi 3 tahun terakhir</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-toggle="block-option"
                            data-action="state_toggle" data-action-mode="demo">
                            <i class="si si-refresh"></i>
                        </button>
                        <button type="button" class="btn-block-option">
                            <i class="si si-settings"></i>
                        </button>
                        
                    </div>
                </div>
                <div class="block-content p-0 text-center">
                    <div class="pt-3" >
                        <canvas id="js-present-line" height="100px" class="js-chartjs-lines chartjs-render-monitor"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Perolehan nilai 3 tahun terakhir</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-toggle="block-option"
                            data-action="state_toggle" data-action-mode="demo">
                            <i class="si si-refresh"></i>
                        </button>
                        <button type="button" class="btn-block-option">
                            <i class="si si-settings"></i>
                        </button>
                        
                    </div>
                </div>
                <div class="block-content p-0 text-center">
                    <div class="pt-3" >
                        <canvas id="js-modul-line" height="100px" class="js-chartjs-lines chartjs-render-monitor"></canvas>
                    </div>
                </div>
                <div class="block-content p-0 text-center">
                    <div class="pt-3" >
                        <canvas id="js-lab-line" height="100px" class="js-chartjs-lines chartjs-render-monitor"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <div class="row">
        <div class="col-lg-12">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Hasil Penilaian Laboratorium Terbaru</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-toggle="block-option"
                            data-action="state_toggle" data-action-mode="demo">
                            <i class="si si-refresh"></i>
                        </button>
                        <button type="button" class="btn-block-option">
                            <i class="si si-settings"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content p-0 text-center">
                    <div class="pt-3" >
                        <canvas id="js-lab" class="js-chartjs-bars chartjs-render-monitor"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
<!-- END Page Content -->
@endsection

@section('js_after')
<script src="{{asset('js/plugins/chart.js/Chart.bundle.min.js')}}"></script>
<script src="{{asset('js/pages/admin_dashboard.js') }}"></script>
<script>
    var ctx = document.getElementById('js-lab').getContext('2d');
    var myBarChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
                labels : @json($vis_lab['kriteria']),
                datasets: [{
                    label: 'Perolehan Nilai',
                    data: @json($vis_lab['nilai']),
                    backgroundColor:'rgba(255, 102, 86, 0.2)',
                    borderColor:'rgba(255, 99, 132, 1)',
                    borderWidth: 1
                }]
            },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        suggestedMin: 0,
                        suggestedMax: 100
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Nilai'
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Point Penilaian'
                        }
                    }]
            }
        }
    });
    var ctx = document.getElementById('js-modul-line').getContext('2d');
    var myBarChart = new Chart(ctx, {
        type: 'line',
        data: {
                labels :  [@foreach($modul_timeline as $key => $value)
                            '{{$key}}',
                         @endforeach],
                datasets: [{
                    label: 'Nilai Modul',
                    data: [@foreach($modul_timeline as $key => $value)
                            {{$value}},
                         @endforeach],
                    borderColor: '#00e676'
                }]
            },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        suggestedMin: 0,
                        suggestedMax: 100
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Akumulasi Penilaian'
                        }
                    }]
            }
        }
    });

    
    var ctx = document.getElementById('js-present-line').getContext('2d');
    var myBarChart = new Chart(ctx, {
        type: 'line',
        data: {
                labels :  [@foreach($asistant_timeline as $key => $value)
                            '{{$key}}',
                         @endforeach],
                datasets: [{
                    label: 'Asisten',
                    data: [@foreach($asistant_timeline as $key => $value)
                            {{round($value, 2)}},
                         @endforeach],
                    borderColor:'#ff6e40',
                    borderWidth: 1
                },
                {
                    label: 'Instruktur',
                    data: [@foreach($instructor_timeline as $key => $value)
                             {{round($value, 2)}},
                         @endforeach],
                    borderColor:'#1de9b6',
                    borderWidth: 1
                }
                ]
            },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        max: 6,
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Rate Kehadiran'
                        }
                }],
                xAxes : [{
                    ticks: {
                        autoSkip: false,
                        maxRotation: 80,
                        minRotation: 80,
                    }
                }]
            }
        }
    });

    var ctx = document.getElementById('js-lab-line').getContext('2d');
    var myBarChart = new Chart(ctx, {
        type: 'line',
        data: {
                labels : @json($timeline['period']),
                datasets: [{
                    label: 'Nilai Lab',
                    data: @json($timeline['grade']),
                    borderColor:'#039be5',
                    borderWidth: 1
                }]
            },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        suggestedMin: 0,
                        suggestedMax: 100
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Akumulasi Penilaian'
                        }
                }],
                xAxes : [{
                    ticks: {
                        autoSkip: false,
                        maxRotation: 70,
                        minRotation: 70,
                    }
                }]
            }
        }
    });
    

    // js lab timeline
    var ctx = document.getElementById('js-modul-pie-high').getContext('2d');
    var myBarChart = new Chart(ctx, {
        type: 'pie',
        data: {
                labels : [
                    @foreach($modul['high'] as $mod)
                        '{{$mod->periode()->nama_penilaian}}',
                    @endforeach
                ],
                datasets: [{
                    label: 'Akumulasi Nilai',
                    data:   [@foreach($modul['high'] as $mod)
                                {{$mod->total_nilai_modul}},
                            @endforeach],
                    backgroundColor: 
                    [
                        'rgba(255, 99, 132, 0.7)',
                        'rgba(54, 162, 235, 0.7)',
                        'rgba(255, 206, 86, 0.7)',
                        'rgba(75, 192, 192, 0.7)',
                        'rgba(153, 102, 255, 0.7)',
                    ],
                    borderWidth: 1
                }]
            },
        options: Chart.defaults.pie
    });

    var ctx = document.getElementById('js-modul-pie-low').getContext('2d');
    var myBarChart = new Chart(ctx, {
        type: 'pie',
        data: {
                labels : [
                    @foreach($modul['low'] as $mod)
                        '{{$mod->periode()->nama_penilaian}}',
                    @endforeach
                ],
                datasets: [{
                    label: 'Akumulasi Nilai',
                    data:   [@foreach($modul['low'] as $mod)
                                {{$mod->total_nilai_modul}},
                            @endforeach],
                    backgroundColor: 
                    [
                        'rgba(255, 99, 132, 0.7)',
                        'rgba(54, 162, 235, 0.7)',
                        'rgba(255, 206, 86, 0.7)',
                        'rgba(75, 192, 192, 0.7)',
                        'rgba(153, 102, 255, 0.7)',
                    ],
                    borderWidth: 1
                }]
            },
        options: Chart.defaults.pie
    });
</script>
@endsection