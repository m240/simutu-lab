@extends('layouts.backend')
@section('title-page','Detail Predikat')

@section('css_before')
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
<div id="page-loader" class="show"></div>
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Detail Predikat</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{route('adm1n.dashboard.index')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{route('adm1n_dashboard_predikat')}}">Predikat</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        Detail Predikat
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="block">
    <div class="block-header block-header-default bg-primary-dark-op d-flex">
        {{-- <div>
            <a type="button" href="/adm1n/dashboard/predikat" class="btn btn-dark mr-2 mb-2 mt-2"><i
                    class="fa fa-arrow-left"></i>Kembali</a>
        </div> flex-grow-1 --}}
        <div class="text-center">
            <h3 class="block-title text-light">Detail Predikat {{$data['predikat']->nama_predikat}}</h3>
        </div>
    </div>
    {{-- <div class="block-content">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="block block-bordered">
                    <div class="block-header border-bottom">
                        <h3 class="block-title">{{$data['periode']->nama_penilaian}}</h3>
</div>
<div class="row block-content">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 my-auto">
        <div class="font-size-h4 mb-1">Tahun {{$data['periode']->tahun}} /
            Semester {{ucfirst($data['periode']->semester)}}
        </div>
        <div class="font-size-h3">
            <br>
            @if($data['periode']->jenis_penilaians)
            Jenis Penilaian : Fasilitas Lab<br><br>
            @else
            Jenis Penilaian : {{ucfirst($data['periode']->jenis_penilaians)}}<br><br>
            @endif
        </div>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 shadow mb-3 rounded">
        <div class="row items-push text-center py-3 mt-1">
            <div class="col-6 col-xl-6">
                <i class="fa fa-star fa-2x text-muted"></i>
                <div class="text-muted mt-1">Predikat</div>
                <div class="text-muted mt-3">{{$data['jml_predikat']}}</div>
            </div>
            <div class="col-6 col-xl-6">
                <i class="fa fa-users fa-2x text-muted"></i>
                <div class="text-muted mt-1">Penilai</div>
                <div class="text-muted mt-3">{{$data['jml_penilai']}}</div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div> --}}
</div>

<!-- Dynamic Table Predikat Full Pagination -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title">List Detail Predikat</h3>
        <a type="button" class="btn btn-sm btn-success mr-1"
            href="{{route('adm1n_predikat_tambah',['id'=>$data['id']])}}">
            <i class="fa fa-fw fa-plus mr-1"></i> Tambah Detail Predikat
        </a>
    </div>
    <div class="block-content block-content-full">
        <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
        <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
            <thead>
                <tr>
                    <th class="text-center" style="width: 80px;">No</th>
                    <th class="d-sm-table-cell" style="width: 10%;">Logo</th>
                    <th class="d-sm-table-cell" style="width: 20%;">Nama</th>
                    <th class="d-sm-table-cell" style="width: 30%;">Deskripsi</th>
                    <th class="d-sm-table-cell" style="width: 10%;">Max Nilai</th>
                    <th class="d-sm-table-cell" style="width: 10%;">Min Nilai</th>
                    <th class="d-sm-table-cell">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 1;
                ?>
                @foreach ($data['detail'] as $d)
                <tr id="baris{{$d->id}}">
                    <td class="text-center font-size-sm">{{$i}}</td>
                    <td>
                        <img class="img-avatar img-avatar-thumb" id="view" src="{{asset($d->logo)}}">
                    </td>
                    <td class="font-w600 font-size-sm">
                        {{$d->nama_predikat}}
                    </td>
                    <td>
                        {{Str::limit($d->desc,100)}}
                    </td>
                    <td>
                        {{$d->max_nilai}}
                    </td>
                    <td>
                        {{$d->min_nilai}}
                    </td>
                    <td>
                        <div class="row justify-content-center">
                            <div data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                <a type="button" class="btn btn-sm btn-rounded btn-success mr-1"
                                    href="{{route('adm1n_predikat_detail_edit_idx',['id_P'=>$data['id'],'id_C'=>$d->id])}}">
                                    <i class="fa fa-fw fa-pencil-alt"></i>
                                </a>
                            </div>
                            <button type="button" class="btn btn-sm btn-rounded btn-danger ml-1 btn-hapus"
                                data-id="{{$d->id}}" data-toggle="tooltip" data-placement="top" title=""
                                data-original-title="Hapus">
                                <i class="fa fa-fw fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <?php
                    $i++;
                ?>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END Dynamic Table Full Pagination -->

@endsection

@section('js_after')
<!-- Page JS Plugins -->
<script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/buttons.print.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<!-- Page JS Code -->
<script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>

<script>
    var route = '{{route('adm1n_dashboard_detail_predikat_hapus')}}'
    $(document).on('click','.btn-hapus',function(){
        data = $(this).data('id')
        Swal.fire({
        title: 'Yakin ingin menghapus predikat?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Ya, hapus!',
        cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                One.loader('show')
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'post',
                    url: route,
                    data: {
                        'id':  $(this).data('id')
                    },
                    success: function(){
                        afterHapus(data)
                    }
                });
            }
        })
    })

    function afterHapus(i){
        $('#baris'+i).remove()
        One.loader('hide')
        Swal.fire(
            'Terhapus!',
            'Predikat telah terhapus.',
            'success'
        )
    }
</script>

@if(Session::get('berhasil'))
<script>
    Swal.fire(
        '{{Session::get('berhasil')}}',
        '',
        'success'
    )
</script>
@endif
@endsection
