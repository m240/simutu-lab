@extends('layouts.backend')
@section('title-page',request()->is('*adm1n/dashboard/predikat/*/tambah') ? 'Tambah Perdikat' : 'Edit Predikat')

@section('css_before')
<link rel="stylesheet" href="{{asset('js/plugins/ion-rangeslider/css/ion.rangeSlider.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
<div id="page-loader" class="show"></div>
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                {{request()->is('*adm1n/dashboard/predikat/*/tambah') ? 'Tambah Detail Perdikat' : 'Edit Detail Predikat'}}
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{route('adm1n.dashboard.index')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{route('adm1n_dashboard_predikat')}}">Predikat</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{route('predikat_detail_byId',['id'=>$data['id']])}}">Detail
                            Predikat</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        Tambah Predikat
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="content">
    <div class="block">
        <div class="block-content">
            @if(request()->is('*adm1n/dashboard/predikat/*/tambah'))
            <form action="{{route('adm1n_tambah_detail_predikat_ok',['id'=>$data['id']])}}" method="post"
                enctype="multipart/form-data">
                @else
                <form
                    action="{{route('adm1n_simpan_edit_detail_predikat',['id'=>$data['id'],'id_C'=>$data['detail']->id])}}"
                    method="post" enctype="multipart/form-data">
                    @endif

                    @csrf
                    <div class="form-group">
                        <label class="" for="nama">Nama Predikat <span class="text-danger">*</span></label>
                        @if(request()->is('*adm1n/dashboard/predikat/*/tambah'))
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Predikat"
                            value="{{Session::get('nama')}}" required>
                        @else
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Predikat"
                            value="{{$data['detail']->nama_predikat}}" required>
                        @endif
                    </div>
                    <div class="form-group mb-5">
                        <label class="mb-4">Batas Nilai (Minimal dan Maksimal) <span
                                class="text-danger">*</span></label>
                        @if(request()->is('*adm1n/dashboard/predikat/*/tambah'))
                        <input type="text" class="js-rangeslider" name="range" data-type="double" data-grid="true"
                            data-min="0" data-max="100"
                            data-from="{{Session::get('min')!=Null ? Session::get('min') : '20'}}"
                            data-to="{{Session::get('max')!=Null ? Session::get('max') : '80'}}">
                        @else
                        <input type="text" class="js-rangeslider" name="range" data-type="double" data-grid="true"
                            data-min="0" data-max="100" data-from="{{$data['detail']->min_nilai}}"
                            data-to="{{$data['detail']->max_nilai}}">
                        @endif
                    </div>
                    <div class="form-group">
                        @if(request()->is('*adm1n/dashboard/predikat/*/tambah'))
                        <label class="" for="desc">Deskripsi <span class="text-danger">*</span></label>
                        <textarea class="form-control" name="desc" id="desc" rows="10" placeholder="Deskripsi"
                            required>{{Session::get('desc')}}</textarea>
                        @else
                        <textarea class="form-control" name="desc" id="desc" rows="10" placeholder="Deskripsi"
                            required>{{$data['detail']->desc}}</textarea>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="" for="desc">Logo <span class="text-danger">*</span></label>
                        <div class="row">
                            <div class="col-1">
                                @if(request()->is('*adm1n/dashboard/predikat/*/tambah'))
                                <img class="img-avatar img-avatar-thumb" id="view"
                                    src="{{asset('media/photos/logo/default.jpg')}}">
                                @else
                                <img class="img-avatar img-avatar-thumb" id="view"
                                    src="{{asset($data['detail']->logo)}}">
                                @endif
                            </div>
                            <div class="col-11">
                                <div class="custom-file mt-3">
                                    @if(request()->is('*adm1n/dashboard/predikat/*/tambah'))
                                    <input type="file" class="custom-file-input" id="file" name="logo" required>
                                    <label class="custom-file-label" for="file">Pilih Foto Logo</label>
                                    @else
                                    <input type="file" class="custom-file-input" id="file" name="logo">
                                    <label class="custom-file-label" for="file">Logo.png</label>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <br>
                        Syarat foto:
                        <ul>
                            <li>Ukuran masimal 2MB</li>
                            <li>Extensi : jpg, jpeg, dan png</li>
                        </ul>
                    </div>
                    <a type="button" href="{{route('predikat_detail_byId',['id'=>$data['id']])}}"
                        class="btn btn-dark mr-2 mb-2 mt-2"><i class="fa fa-arrow-left mr-1"></i>Kembali</a>
                    @if(request()->is('*adm1n/dashboard/predikat/*/tambah'))
                    <button type="submit" class="btn btn-primary mb-2 mt-2"><i
                            class="fa fa-plus mr-1 mt-1"></i>Tambah</button>
                    @else
                    <button type="submit" class="btn btn-primary mb-2 mt-2"><i
                            class="fa fa-check mr-1 mt-1"></i>Simpan</button>
                    @endif
                </form>
        </div>
    </div>
</div>

@endsection

@section('js_after')
<script src="{{asset('js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js')}}"></script>
<script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script>
    jQuery(function(){ One.helpers(['rangeslider']); });

    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
            $('#view').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#file").change(function() {
        readURL(this);
    });
</script>

@if(Session::get('gagal'))
<script>
    Swal.fire(
        '{{Session::get('gagal')}}',
        '',
        'error'
    )
</script>
@endif
@if($errors->any())
<script>
    <?php
    $tmp = '';
        foreach($errors->all() as $e){
            $tmp = $tmp.' - '.$e;
        }
        if($errors->all()[0] == 'The logo failed to upload.'){
            $tmp = '- File size too large.';
        }
    ?>
    Swal.fire(
    '{{$tmp}}',
    '',
    'error'
    )
</script>
@endif
@endsection
