@extends('layouts.backend')
@section('title-page','Praktikum Asisten')

@section('css_before')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
    <div id="page-loader" class="show"></div>
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Periode Praktikum</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item" aria-current="page">Evaluasi
                        </li>
                        <li class="breadcrumb-item" aria-current="page">Modul
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- Dynamic Table Periode Full Pagination -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Pilih Periode Praktikum</h3>
        </div>

        <div class="block-content">
            <select class="form-control" id="exampleFormControlSelect1" style="width: 200px;">
                <option value="">Filter Tahun Ajaran</option>
                <option value="{{now()->year}}">{{now()->year}}</option>
                <option value="{{now()->year-1}}">{{now()->year-1}}</option>
                <option value="{{now()->year-2}}">{{now()->year-2}}</option>
                <option value="{{now()->year-3}}">{{now()->year-3}}</option>
                <option value="{{now()->year-4}}">{{now()->year-4}}</option>
            </select>
        </div>

        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination"
                   id="tablePraktikum">
                <thead>
                <tr>
                    <th class="text-center" style="width: 5%;">No</th>
                    <th class="text-center" style="width: 20%;">Tahun</th>
                    <th class="text-center" style="width: 10%;">Semester</th>
                    <th class="d-sm-table-cell" style="width: 40%;">Name</th>
                    <th class="d-sm-table-cell text-center" style="width: 10%;">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1;?>
                @foreach ($classCategories as $k => $category)
                    <tr>
                        <td class="text-center font-size-sm">{{$i}}</td>
                        <td class="font-w600 font-size-sm text-center">
                            {{explode(' ', $category -> category_name)[2]}}
                        </td>
                        <td class="font-size-sm text-center">
                            {{explode(' ', $category -> category_name)[1]}}
                        </td>
                        <td class="font-size-sm">
                            {{$category -> category_name}}
                        </td>
                        <td>
                            <div class="row justify-content-center">
                                <a type="button" href="{{route('admin_evaluasi_modul_semester',['id'=>$category->id])}}"
                                   class="btn btn-sm btn-rounded btn-secondary" data-toggle="tooltip"
                                   data-placement="top"
                                   data-original-title="Detail Penilaian">
                                    <i class="fa fa-arrow-right text-light"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    <?php $i++;?>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full Pagination -->
@endsection

@section('js_after')
    <script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
    <script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            oTable = $('#tablePraktikum').dataTable()
            {
                order: [[0, "asc"]]
                $('#exampleFormControlSelect1').on('change', function () {
                    var selectedValue = $(this).val();

                    oTable.fnFilter(selectedValue); //Exact value, column, reg
                });
            }
        })
    </script>

@endsection
