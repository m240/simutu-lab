@extends('layouts.backend')
@section('title-page','Praktikum Asisten')

@section('css_before')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
    <div id="page-loader" class="show"></div>
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Pilih Kelas {{$classCategories->category_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item" aria-current="page">Evaluasi
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx"
                               href="{{route('admin_evaluasi_praktikum')}}">Praktikum</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx"
                               href="{{route('admin_evaluasi_praktikum_semester', ['id'=>$idx])}}">Category</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">Detail
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- Dynamic Table Periode Full Pagination -->
    <div class="block">
        <div class="block-header">
            {{--            <h3 class="block-title">{{$classCategories -> category_name}}</h3>--}}
        </div>

        <div class="block-content d-flex justify-content-between mt-2" style="padding-top: 0;">
            <select class="form-control" id="exampleFormControlSelect1" style="width: 100%;"
                    onchange="top.location.href = this.options[this.selectedIndex].value">
                <option value="">Filter Kelas Praktikum</option>
                @foreach($classCategories -> ilab_class as $class)
                    <option
                        value="{{route('admin_evaluasi_praktikum_semester_category_class', ['id'=>$idx, 'idcategory'=>$idcategory, 'idclass' => $class->id])}}">
                        {{$class->full_name}}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="row row-cols-1 row-cols-md-2 d-flex justify-content-center mt-5">
            <div class="col mb-4">
                <div class="card">
                    <canvas id="myChart"></canvas>
                </div>
            </div>
        </div>

        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination"
                   id="tablePraktikum">
                <thead>
                <tr>
                    <th class="text-center" style="width: 5%;">No</th>
                    <th class="text-center" style="width: 20%;">Task</th>
                    <th class="text-center text-center">Nilai Maksimal Selain 100</th>
                    <th class="d-sm-table-cell text-center">Rata rata Nilai Sesuai Task</th>
                    <th class="d-sm-table-cell text-center">Total Praktikkan Mendapat 100</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($classCategories -> ilab_class_category as $category)
                    <tr>
                        <td class="text-center font-size-sm">{{$loop -> iteration}}</td>
                        <td class="font-w600 font-size-sm text-center">
                            {{$category -> category_name}}
                        </td>
                        <td class="font-size-sm text-center">
                            {{$category -> ilab_class_count}}
                        </td>
                        <td class="font-size-sm">
                            {{$category -> ilab_class -> ilab_task -> avg('ilab_task_detail_count')}}
                        </td>
                        <td class="font-size-sm text-center">
                            {{$category -> ilab_class_category_count}}
                        </td>
                        <td>
                            <div class="row justify-content-center">
                                <a type="button"
                                   href="{{route('admin_praktikum_periode_detail',['id'=>$category->id])}}"
                                   class="btn btn-sm btn-rounded btn-secondary" data-toggle="tooltip"
                                   data-placement="top"
                                   data-original-title="Detail Penilaian">
                                    <i class="fa fa-arrow-right text-light"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full Pagination -->
@endsection

@section('js_after')
    <script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
    <script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    <script src="{{asset('js/plugins/chart.js/Chart.min.js')}}"></script>

    <script>
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Modul 1', 'Modul 2', 'Modul 3', 'Modul 4', 'Modul 5', 'Modul 6', 'UAP'],
                datasets: [{
                    label: 'Rata rata nilai Task',
                    data: [0, 0, 0, 0, 0, 0],
                    backgroundColor:
                        'rgba(41, 128, 185,1.0)'
                    ,
                    borderColor:
                        'rgba(41, 128, 185,1.0)'
                    ,
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
@endsection
