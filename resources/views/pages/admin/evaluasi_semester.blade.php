@extends('layouts.backend')
@section('title-page','Evaluasi Semester')

@section('css_before')
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
<div id="page-loader" class="show"></div>
<div class="bg-body-light">
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Evaluasi Semester
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="{{route('adm1n.dashboard.index')}}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">Evaluasi Semester</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->
    <div class="content">
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Visualisasi Data <small> Evaluasi Semester</small></h3>
                <div class="block-options">
                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle"
                        data-action-mode="demo">
                        <i class="si si-refresh"></i>
                    </button>
                </div>
            </div>
            <div class="block-content block-content-full text-center">
                <div class="row justify-content-xl-center">
                    <div class="col-10">
                        <div class="py-1">
                            <!-- Bars Chart Container -->
                            <canvas id="myChart" class="js-chartjs-bars" width="1000px"></canvas>
                            <script src="{{asset('js/plugins/chart.js/Chart.bundle.min.js')}}"></script>
                            <script>
                                var ctx = document.getElementById('myChart').getContext('2d');
                                var myChart = new Chart(ctx, {
                                    type: 'bar',
                                    data: {
                                        labels: [
                                            @foreach($semester as $category)
                                            '{{$category->category_name}}',
                                            @endforeach
                                        ],
                                        datasets: [{
                                            label: 'Rata-Rata Nilai Modul Untuk Seluruh Kelas',
                                            backgroundColor: 'rgba(62, 224, 145, 0.50)',
                                            borderWidth: 2,
                                            data: {!!json_encode($avgModul)!!},
                                        },
                                        {
                                            label: 'Rata-Rata Nilai UAP Untuk Seluruh Kelas',
                                            backgroundColor: 'rgba(224, 97, 62, 0.5)',
                                            borderWidth: 2,
                                            data: {!!json_encode($avgUap)!!},
                                        }
                                        ]
                                    },
                                    options: {
                                        maintainAspectRatio: false,
                                        responsive: true,
                                        events: ['mousemove'], // this is needed, otherwise onHover is not fired
                                        onHover: (event, chartElement) => {
                                            event.target.style.cursor = chartElement[0] ? 'pointer' : 'default';
                                        },
                                        scales: {
                                            yAxes: [{
                                                ticks: {
                                                    beginAtZero: true,
                                                    max: 100
                                                }
                                            }]
                                        }
                                    }
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Tabel Data <small> Evaluasi Semester</small></h3>
            </div>
            <div class="block-content block-content-full">
                <div class="row">
                    <div class="col-5">
                        <form action="{{route('admin_evaluasi_semester_filter')}}" method="POST">
                        <div class="input-group">
                                @csrf
                                <select name="start" id="" class="form-control mb-2" required>
                                    <option value="">Filter by Mulai tahun</option>
                                    @foreach($year as $years)
                                    <option value="{{$years}}" {{Session('filterStart')==$years ? 'selected' : ''}}>{{$years}}</option>
                                    @endforeach
                                </select>
                                <select name="end" id="" class="form-control mb-2" required>
                                    <option value="">Filter by Sampai tahun</option>
                                    @foreach($year as $years)
                                    <option value="{{$years}}" {{Session('filterEnd')==$years ? 'selected' : ''}}>{{$years}}</option>
                                    @endforeach
                                </select>
                                <button type="submit" class="btn btn-sm btn-primary ml-2" style="height: 38px;">Filter now!</button>
                        </div>
                        </form>

                    </div>
                    <div class="col-5"></div>
                    <div class="col-2">
                        <?php $no=0; ?>
                        <form action="{{route('admin_evaluasi_semester_export')}}" method="post">
                            @csrf
                            @foreach($semester as $data)
                            <input type="hidden" name="semester[]" value="{{$data->category_name}}">
                            <input type="hidden" name="total_praktikum[]" value="{{$total_praktikum[$no++]}}">
                            <input type="hidden" name="avgModul[]" value="{{$avgModul[$no-1]}}">
                            <input type="hidden" name="avgUap[]" value="{{$avgUap[$no-1]}}">
                            @endforeach
                            <button type="submit" class="btn btn-success float-right"><i class="fas fa-file-csv"></i>&nbsp; Export CSV</button>
                        </form>
                    </div>
                </div>
                <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tahun Ajaran</th>
                            <th>Total Praktikum Berdasarkan Kategori</th>
                            <th>Rata-Rata Nilai Praktikum Dari Seluruh Kelas</th>
                            <th>Rata-Rata Nilai Modul UAP Dari Seluruh Kelas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; ?>
                        @foreach($semester as $category)
                        <tr>
                            <td>{{$no++}}</td>
                            <td>{{$category->category_name}}</td>
                            <td>{{$total_praktikum[$no-2]}}</td>
                            <td>{{$avgModul[$no-2]}}</td>
                            <td>{{$avgUap[$no-2]}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Dynamic Table Full Pagination -->
    </div>

    @endsection

    @section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/buttons/buttons.print.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/buttons/buttons.html5.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/buttons/buttons.flash.min.js')}}"></script>
    <script src="{{asset('js/plugins/datatables/buttons/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    <script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>
    <!-- Page JS Plugins -->


    @endsection
