@extends('layouts.backend')

@section('title-page', 'Pengaturan Dashboard')

@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Pengaturan Dashboard</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">App</li>
                    <li class="breadcrumb-item">
                        <a class="link-fx" href="{{route('adm1n.dashboard.index')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="">Setting</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

@if (session('status'))
<div class="alert alert-warning alert-dismissable" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">×</span>
</button>
<div class="font-size-sm font-w600 text-uppercase text-muted">{{ session('status') }}</div>
</div>
@endif

<!-- Page Content -->
<div class="content">
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Pengaturan Akun Sistem Informasi Manajemen Mutu</h3>
        </div>
        <div class="block-content block-content-full">
            <form action="{{route('adm1n.setting.update_info')}}" method="POST" enctype="multipart/form-data">
            @csrf
                <div class="row push">                   
                    <div class="col-lg-12 col-xl-12">
                        <div class="form-group">
                            <label for="example-textarea-input">Deskripsi Halaman</label>
                            <textarea class="form-control" id="example-textarea-input" name="desc" rows="4" required >{{$admins->desc}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="example-email-input">Email Feedback</label>
                            <input type="email" class="form-control" id="example-email-input" name="email" value="{{$admins->email}}" required>
                        </div>
                        <div class="form-group">
                            <label for="example-email-input">WhatsApp Feedback</label>
                            <input type="text" class="form-control" id="example-email-input" name="nomor_wa" value="{{$admins->nomor_wa}}" required>
                        </div>
                        <div class="form-group">
                            <label for="example-email-input">Nomor Telepon Feedback</label>
                            <input type="number" class="form-control" id="example-email-input" name="nomor_telp" value="{{$admins->nomor_telp}}" required>
                        </div>
                        
                        <!-- <div class="form-group">
                            <label for="example-password-input">Konfirmasi Password</label>
                            <input type="password" class="form-control" id="example-password-input" name="password" placeholder="Password Input" required>
                        </div>   -->
                        <div class="form-group float-right">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>                                          
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- 
<div class="content">
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Ganti Password</h3>
        </div>
        <div class="block-content block-content-full">
            <form action="{{route('adm1n.update.password')}}" class="validatedForm" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row push">                   
                    <div class="col-lg-12 col-xl-12">
                    <div class="form-group">
                            <label for="old_pass">Password Lama</label>
                            <input type="password" id="old_pass" class="form-control" name="password" placeholder="Old Password" required>
                        </div>
                        <div class="form-group">
                            <label for="password_new">Passowrd Baru</label>
                            <input type="password" class="form-control" id="password_new" name="password_new" placeholder="New Password">
                        </div>
                        <div class="form-group">
                            <label for="password_new_conf">Konfirmasi Password Baru</label>
                            <input type="password" class="form-control" id="password_new_conf" placeholder="New Password Confirmation" name="password_new_conf">
                        </div>       
                        <div class="form-group float-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>                       
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
END Page Content -->
@endsection

@section('js_after')
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="{{asset('js/plugins/chart.js/Chart.bundle.min.js')}}"></script>
<script src="{{asset('js/pages/admin_dashboard.js') }}"></script>
<script>
    jQuery('.validatedForm').validate({
        rules : {
            password_new : {
                minlength : 7
            },
            password_new_conf : {
                minlength : 7,
                equalTo : "#password_new"
            }
        }
    });
</script>
@endsection