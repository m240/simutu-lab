<li class="nav-main-item">
    <a class="nav-main-link{{ request()->is('adm1n/dashboard') ? ' active' : '' }}"
       href="{{route('adm1n.dashboard.index')}}">
        <i class="nav-main-link-icon si si-cursor"></i>
        <span class="nav-main-link-name">Dashboard</span>
    </a>
</li>
<li class="nav-main-heading">Proyek Penilaian</li>
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('adm1n/dashboard/periode*') ? 'active' : ''}}"
       href="{{route('adm1n_dashboard_periode_idx')}}">
        <i class="nav-main-link-icon si si-globe"></i>
        <span class="nav-main-link-name">Periode</span>
    </a>
    <a class="nav-main-link {{request()->is('adm1n/dashboard/kriteria*') ? 'active' : ''}}"
       href="{{route('adm1n_dashboard_kriteria')}}">
        <i class="nav-main-link-icon si si-list"></i>
        <span class="nav-main-link-name">Kriteria</span>
    </a>
    <a class="nav-main-link {{request()->is('adm1n/dashboard/predikat*') ? 'active' : ''}}"
       href="{{route('adm1n_dashboard_predikat')}}">
        <i class="nav-main-link-icon si si-star"></i>
        <span class="nav-main-link-name">Predikat</span>
    </a>
</li>
<li class="nav-main-heading">Feedback</li>
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('adm1n/feedback/modul*') ? 'active' : ''}}"
       href="{{route('admin_feedback_modul_idx')}}">
        <i class="nav-main-link-icon fa fa-tasks"></i>
        <span class="nav-main-link-name">Modul</span>
    </a>
</li>
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('adm1n/feedback/asisten-instruktur*') ? 'active' : ''}}"
       href="{{route('admin_feedback_asisten_instruktur_idx')}}">
        <i class="nav-main-link-icon si si-users"></i>
        <span class="nav-main-link-name">Asisten / Instruktur</span>
    </a>
</li>
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('adm1n/feedback/fasilitas-lab*') ? 'active' : ''}}"
       href="{{route('admin_feedback_fasilab_idx')}}">
        <i class="nav-main-link-icon fa fa-hotel"></i>
        <span class="nav-main-link-name">Laboratorium</span>
    </a>
</li>
<li class="nav-main-heading">Evaluasi Nilai</li>
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('adm1n/evaluasi/modul*') ? 'active' : ''}}" href="{{route('admin_evaluasi_modul')}}">
        <i class="nav-main-link-icon si si-book-open"></i>
        <span class="nav-main-link-name">Modul</span>
    </a>
</li>
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('adm1n/evaluasi/kelas*') ? 'active' : ''}}" href="{{route('admin_evaluasi_kelas_index')}}">
        <i class="nav-main-link-icon si si-graph"></i>
        <span class="nav-main-link-name">Kelas</span>
    </a>
</li>
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('adm1n/evaluasi/praktikum*') ? 'active' : ''}}"
       href="{{route('admin_evaluasi_praktikum')}}">
        <i class="nav-main-link-icon fa fa-laptop-code"></i>
        <span class="nav-main-link-name">Praktikum</span>
    </a>
</li>
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('adm1n/evaluasi/semester*') ? 'active' : ''}}" href="{{route('admin_evaluasi_semester_index')}}">
        <i class="nav-main-link-icon si si-clock"></i>
        <span class="nav-main-link-name">Semester</span>
    </a>
</li>
<li class="nav-main-heading">Praktikum</li>
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('adm1n/praktikum/instruktur*') ? 'active' : ''}}" href="{{route('instruktur_index')}}">
        <i class="nav-main-link-icon fa fa-chalkboard-teacher"></i>
        <span class="nav-main-link-name">Instruktur</span>
    </a>
</li>
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('adm1n/praktikum/periode*') ? 'active' : ''}}"
       href="{{route('admin_praktikum_periode')}}">
        <i class="nav-main-link-icon fa fa-users"></i>
        <span class="nav-main-link-name">Asisten</span>
    </a>
</li>
<li class="nav-main-item open">
    <a class="nav-main-link {{request()->is('adm1n/praktikum/online*') ? 'active' : ''}}"
        href="{{route('admin_praktikum_online')}}">
        <i class="nav-main-link-icon fa fa-globe-asia"></i>
        <span class="nav-main-link-name">Praktikum Online</span>
    </a>
</li>
