<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Simutu Lab - Bukti Praktikum Online</title>
    <style>
        hr {
            border-top: 2px solid black;
        }
        .center{
            text-align: center;
        }
    </style>
    <style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
</head>

<body>
    <img src="https://infotech.umm.ac.id/assets/img/logo_umm_putih.jpg" alt="UMM" width="100px" style="position: absolute;">
    <div style="text-align: center;">
        <h5 style="margin-bottom: 0px; margin-top: 0px;">UNIVERSITAS MUHAMMADIYAH MALANG</h5>
        <h5 style="margin-bottom: 0px; margin-top: 0px;">LABORATORIUM INFORMATIKA</h5>
        <h6 style="margin-bottom: 0px; margin-top: 0px;">Rekayasa Perangkat Lunak(RPL) - Jaringan Internet dan Intranet (JII)</h6>
        <h6 style="margin-bottom: 0px; margin-top: 0px;">Sistem Informasi (SI) - Sistem dan Arsitektur Komputer (SAK)</h6>
        <h6 style="margin-bottom: 0px; margin-top: 0px;">Desain Aplikasi Multimedia dan Grafis (DAMG)</h6>
    </div>
    <hr>
    <div >        
        <center><h6 style="margin-bottom: 0px; margin-top: 0px;">SISTEM INFORMASI MANAJEMEN MUTU</h6></center>
        <center><h6 >Kategori Kelas</h6></center>
    </div>
    <div>
        <table style="width: 100%;">
            <tbody>
                <tr>
                    <td style="width: 30%;">
                        <span>Nama Kategori Kelas</span>
                    </td >
                    <td style="width: 70%;">
                        <span>: {{$class->category_name}}</span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <span>Periode</span>
                    </td >
                    <td style="width: 70%;">
                        <span>: {{$class->ilab_semester->category_name}}</span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <span>Tanggal File Dibuat</span>
                    </td>
                    <td>
                        <span>: {{$tanggal}}</span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <hr>
    <div>
        <center><h6>{{$class->category_name}}</h6></center>
    </div>
    <div >
        <table style="width: 100%;">
            <thead>
                <tr class="text-uppercase">
                    <th class="font-w700">No.</th>
                    <th class="d-none d-sm-table-cell font-w700">Nama Kelas</th>
                    <th class="d-none d-sm-table-cell font-w700">Nama Task</th>
                    <th class="d-none d-sm-table-cell font-w700">Status</th>
                    <th class="d-none d-sm-table-cell font-w700">URL Bukti Praktikum</th>
                </tr>
            </thead>
            <?php $no = 1 ?>
            @foreach($class->ilab_class as $cli)
                @foreach($cli->ilab_task as $task)
                <tbody>
                    <tr>
                        <td style="width: 5%">
                            <span>{{$no++}}</span>
                        </td>
                        <td>
                            <span> {{$cli->full_name}} </span>
                        </td>
                        <td>
                            <span> {{$task->title}} </span>
                        </td>
                        <td>
                            <span style="{{$task->statusBuktiBool? 'color:green;': 'color:red;'}}" >  {{$task->statusBuktiStr}} </span>
                        </td>
                        <td>
                            @if($task->statusBuktiBool)
                                <span> <a href="{{$task->buktiPraktikum}}" target="_blank"></a>
                                {{$task->buktiPraktikum}} 
                                </span>
                            @endif
                        </td>
                    </tr>
                </tbody>
                @endforeach
            @endforeach
        </table>
    </div>
    <hr>
</body>