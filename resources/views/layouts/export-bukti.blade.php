<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Simutu Lab - Bukti Pengisian</title>
    <style>
        hr {
            border-top: 2px solid black;
        }
        .center{
            text-align: center;
        }
    </style>
    <style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
</head>

<body>
    <img src="https://infotech.umm.ac.id/assets/img/logo_umm_putih.jpg" alt="UMM" width="100px" style="position: absolute;">
    <div style="text-align: center;">
        <h5 style="margin-bottom: 0px; margin-top: 0px;">UNIVERSITAS MUHAMMADIYAH MALANG</h5>
        <h5 style="margin-bottom: 0px; margin-top: 0px;">LABORATORIUM INFORMATIKA</h5>
        <h6 style="margin-bottom: 0px; margin-top: 0px;">Rekayasa Perangkat Lunak(RPL) - Jaringan Internet dan Intranet (JII)</h6>
        <h6 style="margin-bottom: 0px; margin-top: 0px;">Sistem Informasi (SI) - Sistem dan Arsitektur Komputer (SAK)</h6>
        <h6 style="margin-bottom: 0px; margin-top: 0px;">Desain Aplikasi Multimedia dan Grafis (DAMG)</h6>
    </div>
    <hr>
    <div >        
        <center><h6 style="margin-bottom: 0px; margin-top: 0px;">SISTEM INFORMASI MANAJEMEN MUTU</h6></center>
        <center><h6 >Pengisian Atas Nama</h6></center>
    </div>
    <div>
        <table style="width: 100%;">
            <tbody>
                <tr>
                    <td style="width: 10%;">
                        <span>Nama: </span>
                    </td >
                    <td style="width: 90%;">
                        <span>{{$user->full_name}}</span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%;">
                        <span>NIM: </span>
                    </td>
                    <td>
                        <span>{{$user->user_name}}</span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%;">
                        <span>Tanggal: </span>
                    </td>
                    <td>
                        <span>{{$tanggal}}</span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <hr>
    <div>
        <center><h6 >Modul Praktikum</h6></center>
    </div>
    <div >
        <table style="width: 100%;">
            <thead>
                <tr class="text-uppercase">
                    <th class="font-w700">No.</th>
                    <th class="d-none d-sm-table-cell font-w700">Nama</th>
                    <th class="font-w700">Status</th>
                </tr>
            </thead>
            <?php $no = 1 ?>
            @foreach($moduls['periode_penilaian'] as $d)
            <tbody>
                <tr>
                    <td style="width: 5%">
                        <span>{{$no++}}</span>
                    </td>
                    <td>
                        <span> {{$moduls['nama_kelas'][$d->ilab_class_category_id]}}</span>
                    </td>
                    <td>
                        @if(in_array($d->id,$moduls['udh_isi']))
                        <span style="color: green;">sudah mengisi</span>
                        @else
                        <span style="color: red;">belum mengisi</span>
                        @endif
                    </td>
                </tr>
            </tbody>
            @endforeach
        </table>
    </div>
    <hr>
    <div >
        <center><h6 >Asisten</h6></center>
    </div>
    <div >
        <table style="width: 100%;">
            <thead>
                <tr class="text-uppercase">
                    <th class="font-w700">No.</th>
                    <th class="font-w700">NIM</th>
                    <th class="d-none d-sm-table-cell font-w700">Nama</th>
                    <th class="font-w700">Kelas</th>
                    <th class="font-w700">Status</th>
                </tr>
            </thead>
            <?php $no = 1 ?>
            @foreach($asistens as $kelas)
            @foreach($kelas['data_asisten'] as $asisten)
            <tbody>
                <tr>
                    <td style="width: 5%">
                        <span>{{$no++}}</span>
                    </td>
                    <td>
                        <span>{{$asisten->user_name}}</span>
                    </td>
                    <td>
                        <span>{{$asisten->full_name}}</span>
                    </td>
                    <td>
                        <span>{{$kelas->full_name}}</span>
                    </td>
                    <td>
                        @if($asisten->sudah_isi == 0)
                        <span style="color: red;">belum mengisi</span>
                        @elseif($asisten->sudah_isi == 1)
                        <span style="color: #FF4500;">belum aktif</span>
                        @elseif($asisten->sudah_isi == 2)
                        <span style="color: green;">sudah mengisi</span>
                        @endif
                    </td>
                    </td>
                </tr>
            </tbody>
            @endforeach
            @endforeach
        </table>
    </div>
    <hr>
    <div >
        <center><h6 >Instruktur</h6></center>
    </div>
    <div >
        <table style="width: 100%;">
            <thead>
                <tr class="text-uppercase">
                    <th class="font-w700">No.</th>
                    <th class="font-w700">NIP / NIDN</th>
                    <th class="d-none d-sm-table-cell font-w700">Nama</th>
                    <th class="font-w700">Status</th>
                </tr>
            </thead>
            <?php $no = 1 ?>
            @foreach($instrukturs as $instruktur)
            <tbody>
                <tr>
                    <td style="width: 5%">
                        <span>{{$no++}}</span>
                    </td>
                    <td>
                        <span>{{$instruktur->ilab_user_instructor()->first()->nip}}</span>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <span>{{$instruktur->full_name}}</span>
                    </td>
                    <td>
                        @if($instruktur->sudah_isi == 0)
                        <span style="color: red;">belum mengisi</span>
                        @elseif($instruktur->sudah_isi == 1)
                        <span style="color: #FF4500;">belum aktif</span>
                        @elseif($instruktur->sudah_isi == 2)
                        <span style="color: green;">sudah mengisi</span>
                        @endif
                    </td>
                </tr>
            </tbody>
            @endforeach
        </table>
    </div>
    <hr>
    <div >
        <center><h6>Laboratorium</h6></center>
    </div>
    <div >
        <table style="width: 100%;">
            <thead>
                <tr class="text-uppercase">
                    <th class="font-w700">No.</th>
                    <th class="font-w700">Nama</th>
                    <th class="font-w700">Status</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="width: 5%">
                        <span>1</span>
                    </td>
                    <td>
                        <span>
                            @if($lab['sudah_isi'] == 1)
                            Fasilitas Laboratorium Informatika
                            @else
                            {{$lab['nama']}}
                            @endif
                        </span>
                    </td>
                    <td>
                        @if($lab['sudah_isi'] == 0)
                        <span style="color: red;">belum mengisi</span>
                        @elseif($lab['sudah_isi'] == 1)
                        <span style="color: #FF4500;">belum aktif</span>
                        @elseif($lab['sudah_isi'] == 2)
                        <span style="color: green;">sudah mengisi</span>
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>