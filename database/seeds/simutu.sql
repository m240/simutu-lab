-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Jul 2020 pada 09.55
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simutu`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'labit', 'labit@gmail.com', '$2y$10$cCs7ppp7mrlEZC.55PcY7.QOCXq/uqPUbT9CAKKZDfPlTIfuBxErm', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_fasilab_processed`
--

CREATE TABLE `data_fasilab_processed` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `total_nilai_fasilab` int(11) NOT NULL,
  `periode_penilaian_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_fasilab_processed`
--

INSERT INTO `data_fasilab_processed` (`id`, `total_nilai_fasilab`, `periode_penilaian_id`, `created_at`, `updated_at`) VALUES
(1, 80, 3, '2020-07-13 12:49:16', '2020-07-13 12:49:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_kriteria_processed`
--

CREATE TABLE `data_kriteria_processed` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `total_nilai_kriteria` int(11) NOT NULL,
  `detail_kriteria_id` bigint(20) UNSIGNED DEFAULT NULL,
  `data_user_processed_id` bigint(20) UNSIGNED DEFAULT NULL,
  `data_fasilab_processed_id` bigint(20) UNSIGNED DEFAULT NULL,
  `data_modul_processed_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_kriteria_processed`
--

INSERT INTO `data_kriteria_processed` (`id`, `total_nilai_kriteria`, `detail_kriteria_id`, `data_user_processed_id`, `data_fasilab_processed_id`, `data_modul_processed_id`, `created_at`, `updated_at`) VALUES
(1, 24, 16, NULL, 1, NULL, '2020-07-13 12:49:16', '2020-07-13 12:49:16'),
(2, 30, 17, NULL, 1, NULL, '2020-07-13 12:49:17', '2020-07-13 12:49:17'),
(3, 14, 18, NULL, 1, NULL, '2020-07-13 12:49:17', '2020-07-13 12:49:17'),
(4, 8, 19, NULL, 1, NULL, '2020-07-13 12:49:17', '2020-07-13 12:49:17'),
(5, 4, 20, NULL, 1, NULL, '2020-07-13 12:49:17', '2020-07-13 12:49:17'),
(6, 25, 6, NULL, NULL, 1, '2020-07-13 12:51:09', '2020-07-13 12:51:09'),
(7, 24, 7, NULL, NULL, 1, '2020-07-13 12:51:09', '2020-07-13 12:51:09'),
(8, 20, 8, NULL, NULL, 1, '2020-07-13 12:51:09', '2020-07-13 12:51:09'),
(9, 19, 9, NULL, NULL, 1, '2020-07-13 12:51:09', '2020-07-13 12:51:09'),
(10, 30, 13, 1, NULL, NULL, '2020-07-13 12:56:46', '2020-07-13 12:56:46'),
(11, 30, 14, 1, NULL, NULL, '2020-07-13 12:56:46', '2020-07-13 12:56:46'),
(12, 24, 15, 1, NULL, NULL, '2020-07-13 12:56:46', '2020-07-13 12:56:46'),
(13, 24, 13, 2, NULL, NULL, '2020-07-13 12:56:46', '2020-07-13 12:56:46'),
(14, 18, 14, 2, NULL, NULL, '2020-07-13 12:56:46', '2020-07-13 12:56:46'),
(15, 40, 15, 2, NULL, NULL, '2020-07-13 12:56:46', '2020-07-13 12:56:46'),
(16, 24, 13, 3, NULL, NULL, '2020-07-13 12:56:46', '2020-07-13 12:56:46'),
(17, 18, 14, 3, NULL, NULL, '2020-07-13 12:56:46', '2020-07-13 12:56:46'),
(18, 40, 15, 3, NULL, NULL, '2020-07-13 12:56:46', '2020-07-13 12:56:46'),
(19, 16, 1, NULL, NULL, 2, '2020-07-13 12:56:58', '2020-07-13 12:56:58'),
(20, 21, 3, NULL, NULL, 2, '2020-07-13 12:56:58', '2020-07-13 12:56:58'),
(21, 22, 4, NULL, NULL, 2, '2020-07-13 12:56:58', '2020-07-13 12:56:58'),
(22, 18, 5, NULL, NULL, 2, '2020-07-13 12:56:58', '2020-07-13 12:56:58'),
(23, 23, 10, 4, NULL, NULL, '2020-07-13 12:57:07', '2020-07-13 12:57:07'),
(24, 18, 11, 4, NULL, NULL, '2020-07-13 12:57:07', '2020-07-13 12:57:07'),
(25, 33, 12, 4, NULL, NULL, '2020-07-13 12:57:07', '2020-07-13 12:57:07'),
(26, 29, 10, 5, NULL, NULL, '2020-07-13 12:57:08', '2020-07-13 12:57:08'),
(27, 24, 11, 5, NULL, NULL, '2020-07-13 12:57:08', '2020-07-13 12:57:08'),
(28, 33, 12, 5, NULL, NULL, '2020-07-13 12:57:08', '2020-07-13 12:57:08'),
(29, 23, 10, 6, NULL, NULL, '2020-07-13 12:57:08', '2020-07-13 12:57:08'),
(30, 18, 11, 6, NULL, NULL, '2020-07-13 12:57:08', '2020-07-13 12:57:08'),
(31, 41, 12, 6, NULL, NULL, '2020-07-13 12:57:08', '2020-07-13 12:57:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_modul_processed`
--

CREATE TABLE `data_modul_processed` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `total_nilai_modul` int(11) NOT NULL,
  `periode_penilaian_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_modul_processed`
--

INSERT INTO `data_modul_processed` (`id`, `total_nilai_modul`, `periode_penilaian_id`, `created_at`, `updated_at`) VALUES
(1, 88, 4, '2020-07-13 12:51:09', '2020-07-13 12:51:09'),
(2, 77, 5, '2020-07-13 12:56:58', '2020-07-13 12:56:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_msg_penilaians`
--

CREATE TABLE `data_msg_penilaians` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ilab_praktikkan_id` bigint(20) NOT NULL,
  `ilab_user_id` bigint(20) DEFAULT NULL,
  `ilab_class_id` bigint(20) DEFAULT NULL,
  `periode_penilaian_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_msg_penilaians`
--

INSERT INTO `data_msg_penilaians` (`id`, `message`, `ilab_praktikkan_id`, `ilab_user_id`, `ilab_class_id`, `periode_penilaian_id`, `created_at`, `updated_at`) VALUES
(1, 'Saran untuk dibuat coworking space', 2333, NULL, NULL, 3, NULL, NULL),
(2, 'AC pada lab CD kurang dingin', 2127, NULL, NULL, 3, NULL, NULL),
(3, 'materi ditambah dengan database nosql', 2333, NULL, 847, 5, NULL, NULL),
(4, 'materi menggunakan flutter', 2333, NULL, 893, 4, NULL, NULL),
(5, 'kdang suka telat', 2333, 1985, 872, 1, NULL, NULL),
(6, 'terlalu serius', 2333, 1655, 847, 1, NULL, NULL),
(7, 'penjelasan terlalu cepat', 2333, 2663, 847, 2, NULL, NULL),
(8, 'Sering hadir', 2127, 26, 901, 2, NULL, NULL),
(9, 'penjelasan terkadang kurang nyambung', 2127, 1726, 901, 1, NULL, NULL),
(10, 'Mohon ditambah penjelasan mengenai SQL untuk Postgre', 2333, NULL, 847, 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_penilaians`
--

CREATE TABLE `data_penilaians` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nilai` int(11) NOT NULL,
  `ilab_praktikkan_id` bigint(20) NOT NULL,
  `ilab_user_id` bigint(20) DEFAULT NULL,
  `ilab_class_id` bigint(20) DEFAULT NULL,
  `periode_penilaian_id` bigint(20) UNSIGNED NOT NULL,
  `detail_kriteria_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_penilaians`
--

INSERT INTO `data_penilaians` (`id`, `nilai`, `ilab_praktikkan_id`, `ilab_user_id`, `ilab_class_id`, `periode_penilaian_id`, `detail_kriteria_id`, `created_at`, `updated_at`) VALUES
(1, 60, 2333, NULL, NULL, 3, 16, NULL, NULL),
(2, 100, 2333, NULL, NULL, 3, 17, NULL, NULL),
(3, 40, 2333, NULL, NULL, 3, 18, NULL, NULL),
(4, 80, 2333, NULL, NULL, 3, 19, NULL, NULL),
(5, 40, 2333, NULL, NULL, 3, 20, NULL, NULL),
(6, 100, 2127, NULL, NULL, 3, 16, NULL, NULL),
(7, 100, 2127, NULL, NULL, 3, 17, NULL, NULL),
(8, 100, 2127, NULL, NULL, 3, 18, NULL, NULL),
(9, 80, 2127, NULL, NULL, 3, 19, NULL, NULL),
(10, 40, 2127, NULL, NULL, 3, 20, NULL, NULL),
(11, 80, 2333, NULL, 847, 5, 1, NULL, NULL),
(12, 60, 2333, NULL, 847, 5, 3, NULL, NULL),
(13, 100, 2333, NULL, 847, 5, 4, NULL, NULL),
(14, 80, 2333, NULL, 847, 5, 5, NULL, NULL),
(15, 80, 2333, NULL, 893, 4, 6, NULL, NULL),
(16, 80, 2333, NULL, 893, 4, 7, NULL, NULL),
(17, 100, 2333, NULL, 893, 4, 8, NULL, NULL),
(18, 100, 2333, NULL, 893, 4, 9, NULL, NULL),
(19, 80, 2333, 1985, 872, 1, 10, NULL, NULL),
(20, 60, 2333, 1985, 872, 1, 11, NULL, NULL),
(21, 80, 2333, 1985, 872, 1, 12, NULL, NULL),
(22, 100, 2333, 1655, 847, 1, 10, NULL, NULL),
(23, 80, 2333, 1655, 847, 1, 11, NULL, NULL),
(24, 80, 2333, 1655, 847, 1, 12, NULL, NULL),
(25, 100, 2333, 2663, 847, 2, 13, NULL, NULL),
(26, 100, 2333, 2663, 847, 2, 14, NULL, NULL),
(27, 60, 2333, 2663, 847, 2, 15, NULL, NULL),
(28, 80, 2333, 2660, 872, 2, 13, NULL, NULL),
(29, 60, 2333, 2660, 872, 2, 14, NULL, NULL),
(30, 100, 2333, 2660, 872, 2, 15, NULL, NULL),
(31, 80, 2127, 26, 901, 2, 13, NULL, NULL),
(32, 60, 2127, 26, 901, 2, 14, NULL, NULL),
(33, 100, 2127, 26, 901, 2, 15, NULL, NULL),
(34, 80, 2127, 1726, 901, 1, 10, NULL, NULL),
(35, 60, 2127, 1726, 901, 1, 11, NULL, NULL),
(36, 100, 2127, 1726, 901, 1, 12, NULL, NULL),
(37, 80, 2333, NULL, 847, 7, 1, NULL, NULL),
(38, 60, 2333, NULL, 847, 7, 3, NULL, NULL),
(39, 100, 2333, NULL, 847, 7, 4, NULL, NULL),
(40, 60, 2333, NULL, 847, 7, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_sudah_mengisis`
--

CREATE TABLE `data_sudah_mengisis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ilab_praktikkan_id` bigint(20) NOT NULL,
  `ilab_user_id` bigint(20) DEFAULT NULL,
  `ilab_class_id` bigint(20) DEFAULT NULL,
  `periode_penilaian_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_sudah_mengisis`
--

INSERT INTO `data_sudah_mengisis` (`id`, `ilab_praktikkan_id`, `ilab_user_id`, `ilab_class_id`, `periode_penilaian_id`, `created_at`, `updated_at`) VALUES
(1, 2333, NULL, NULL, 3, NULL, NULL),
(2, 2127, NULL, NULL, 3, NULL, NULL),
(3, 1447, NULL, 847, 5, NULL, NULL),
(4, 1447, NULL, 893, 4, NULL, NULL),
(5, 2333, 1985, 872, 1, NULL, NULL),
(6, 2333, 1655, 847, 1, NULL, NULL),
(7, 2333, 2663, 847, 2, NULL, NULL),
(8, 2333, 2660, 872, 2, NULL, NULL),
(9, 2127, 26, 901, 2, NULL, NULL),
(10, 2127, 1726, 901, 1, NULL, NULL),
(11, 1447, NULL, 847, 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_user_processed`
--

CREATE TABLE `data_user_processed` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ilab_user_id` bigint(20) NOT NULL,
  `total_nilai_user` int(11) NOT NULL,
  `periode_penilaian_id` bigint(20) UNSIGNED NOT NULL,
  `detail_predikat_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_user_processed`
--

INSERT INTO `data_user_processed` (`id`, `ilab_user_id`, `total_nilai_user`, `periode_penilaian_id`, `detail_predikat_id`, `created_at`, `updated_at`) VALUES
(1, 2663, 84, 2, 3, '2020-07-13 12:56:46', '2020-07-13 12:56:46'),
(2, 2660, 82, 2, 3, '2020-07-13 12:56:46', '2020-07-13 12:56:46'),
(3, 26, 82, 2, 3, '2020-07-13 12:56:46', '2020-07-13 12:56:46'),
(4, 1985, 74, 1, 2, '2020-07-13 12:57:07', '2020-07-13 12:57:07'),
(5, 1655, 86, 1, 3, '2020-07-13 12:57:08', '2020-07-13 12:57:08'),
(6, 1726, 82, 1, 3, '2020-07-13 12:57:08', '2020-07-13 12:57:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_verifikasi_mengisis`
--

CREATE TABLE `data_verifikasi_mengisis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ilab_praktikkan_id` bigint(20) NOT NULL,
  `periode_penilaian_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_kriterias`
--

CREATE TABLE `detail_kriterias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `persentase` int(11) NOT NULL,
  `kriteria_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `detail_kriterias`
--

INSERT INTO `detail_kriterias` (`id`, `desc`, `persentase`, `kriteria_id`, `created_at`, `updated_at`) VALUES
(1, 'Memberi penjelasan tentang DDL dan DML', 20, 5, NULL, '2020-07-16 07:58:43'),
(3, 'Kesesuaian soal dengan materi', 35, 5, NULL, NULL),
(4, 'Referensi tambahan', 22, 5, NULL, NULL),
(5, 'Relasi Tabel', 23, 5, NULL, NULL),
(6, 'Penjelasan arsitektur dan lifecycle mobile', 31, 4, NULL, NULL),
(7, 'Kesesuaian materi dengan soal', 30, 4, NULL, NULL),
(8, 'Sistem basis data pada mobile', 20, 4, NULL, NULL),
(9, 'Penjelasan koneksi dengan internet dengan mobile', 19, 4, NULL, NULL),
(10, 'Hadir tepat waktu', 29, 3, NULL, NULL),
(11, 'Memberi tambahan penjelasan', 30, 3, NULL, NULL),
(12, 'Menanggapi pertanyaan yang kurang jelas', 41, 3, NULL, NULL),
(13, 'Memberi pengantar praktikum', 30, 2, NULL, NULL),
(14, 'Kehadiran mendamping praktikum', 30, 2, NULL, NULL),
(15, 'Memberikan jawaban atas praktikum', 40, 2, NULL, NULL),
(16, 'Kualitas Komputer dan Kursi', 30, 1, NULL, NULL),
(17, 'Layanan administrasi dan digital', 30, 1, NULL, NULL),
(18, 'Laboratorium Riset', 20, 1, NULL, NULL),
(19, 'Lab AB', 10, 1, NULL, NULL),
(20, 'Lab CD', 10, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_predikats`
--

CREATE TABLE `detail_predikats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_predikat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `max_nilai` int(11) NOT NULL,
  `min_nilai` int(11) NOT NULL,
  `predikat_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `detail_predikats`
--

INSERT INTO `detail_predikats` (`id`, `desc`, `logo`, `nama_predikat`, `max_nilai`, `min_nilai`, `predikat_id`, `created_at`, `updated_at`) VALUES
(1, 'kurang berperan mendukung aktifitas laboratorium', 'media/photos/logo/1_logo_1.jpg', 'Kurang Aktif', 25, 0, 1, NULL, '2020-07-16 08:26:03'),
(2, 'Cukup berperan mendukung aktifitas laboratorium', 'media/photos/logo/1_logo_2.jpg', 'Cukup', 74, 26, 1, NULL, '2020-07-13 12:45:30'),
(3, 'Sangat berperan mendukung aktifitas laboratorium', 'media/photos/logo/1_logo_3.jpg', 'Bagus Berdampak', 100, 75, 1, NULL, '2020-07-16 08:26:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kriterias`
--

CREATE TABLE `kriterias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kriteria` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kriterias`
--

INSERT INTO `kriterias` (`id`, `nama_kriteria`, `created_at`, `updated_at`) VALUES
(1, 'Fasilitas Lab', NULL, NULL),
(2, 'Instruktur', NULL, NULL),
(3, 'Kriteria Asisten', NULL, '2020-07-16 07:41:55'),
(4, 'Modul Mobile', NULL, NULL),
(5, 'Modul Basisdata', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_03_05_00003_create_kriterias_table', 1),
(4, '2020_03_05_00004_create_detail_kriterias_table', 1),
(5, '2020_03_06_232122_create_predikats_table', 1),
(6, '2020_03_06_253322_create_detail_predikats_table', 1),
(7, '2020_03_29_094834_create_admins_table', 1),
(8, '2020_04_05_00001_create_periode_penilaians_table', 1),
(9, '2020_04_05_00004_create_data_penilaians_table', 1),
(10, '2020_04_15_180957_create_data_msg_penilaians_table', 1),
(11, '2020_04_16_101018_create_data_verifikasi_mengisis_table', 1),
(12, '2020_04_16_104524_create_data_sudah_mengisis_table', 1),
(13, '2020_05_17_073831_create_data_user_processed_table', 1),
(14, '2020_05_17_074547_create_data_modul_processed_table', 1),
(15, '2020_05_17_074714_create_data_fasilab_processed_table', 1),
(16, '2020_05_17_074813_create_data_kriteria_processed_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `periode_penilaians`
--

CREATE TABLE `periode_penilaians` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tahun` int(11) NOT NULL,
  `nama_penilaian` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `semester` enum('ganjil','genap') COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `processed` tinyint(1) NOT NULL,
  `jenis_penilaians` enum('asisten','instruktur','fasil_lab','modul') COLLATE utf8mb4_unicode_ci NOT NULL,
  `ilab_class_category_id` bigint(20) DEFAULT NULL,
  `kriteria_id` bigint(20) UNSIGNED NOT NULL,
  `predikat_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `periode_penilaians`
--

INSERT INTO `periode_penilaians` (`id`, `tahun`, `nama_penilaian`, `semester`, `active`, `processed`, `jenis_penilaians`, `ilab_class_category_id`, `kriteria_id`, `predikat_id`, `created_at`, `updated_at`) VALUES
(1, 2020, 'Asisten Ganjil 2020', 'ganjil', 0, 1, 'asisten', NULL, 3, 1, NULL, '2020-07-16 08:09:33'),
(2, 2020, 'Instruktur Ganjil 2020', 'ganjil', 0, 1, 'instruktur', NULL, 2, 1, NULL, '2020-07-16 08:09:36'),
(3, 2020, 'Fasilitas Lab. Ganjil 2020', 'ganjil', 0, 1, 'fasil_lab', NULL, 1, NULL, NULL, '2020-07-16 08:10:01'),
(4, 2020, 'Modul Praktikum Pemrograman Mobile Ganjil 2020', 'ganjil', 0, 1, 'modul', 156, 4, NULL, NULL, '2020-07-16 08:08:16'),
(5, 2020, 'Modul Praktikum Basisdata Ganjil 2020', 'ganjil', 0, 1, 'modul', 159, 5, NULL, NULL, '2020-07-16 08:08:12'),
(6, 2019, 'Modul Praktikum Pemrograman Mobile Ganjil 2019', 'ganjil', 1, 0, 'modul', 156, 4, NULL, NULL, '2020-07-16 08:08:16'),
(7, 2019, 'Modul Praktikum Basisdata Ganjil 2019', 'ganjil', 1, 0, 'modul', 159, 5, NULL, NULL, '2020-07-16 08:08:12'),
(8, 2019, 'Asisten Ganjil 2019', 'ganjil', 1, 0, 'asisten', NULL, 3, 1, NULL, '2020-07-16 08:09:33'),
(9, 2019, 'Instruktur Ganjil 2019', 'ganjil', 1, 0, 'instruktur', NULL, 2, 1, NULL, '2020-07-16 08:09:36'),
(10, 2019, 'Fasilitas Lab. Ganjil 2019', 'ganjil', 1, 0, 'fasil_lab', NULL, 1, NULL, NULL, '2020-07-16 08:10:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `predikats`
--

CREATE TABLE `predikats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_predikat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `predikats`
--

INSERT INTO `predikats` (`id`, `nama_predikat`, `created_at`, `updated_at`) VALUES
(1, 'Predikat Instruktur / Asisten', NULL, '2020-07-16 08:03:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indeks untuk tabel `data_fasilab_processed`
--
ALTER TABLE `data_fasilab_processed`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_fasilab_processed_periode_penilaian_id_foreign` (`periode_penilaian_id`);

--
-- Indeks untuk tabel `data_kriteria_processed`
--
ALTER TABLE `data_kriteria_processed`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_kriteria_processed_detail_kriteria_id_foreign` (`detail_kriteria_id`),
  ADD KEY `data_kriteria_processed_data_user_processed_id_foreign` (`data_user_processed_id`),
  ADD KEY `data_kriteria_processed_data_fasilab_processed_id_foreign` (`data_fasilab_processed_id`),
  ADD KEY `data_kriteria_processed_data_modul_processed_id_foreign` (`data_modul_processed_id`);

--
-- Indeks untuk tabel `data_modul_processed`
--
ALTER TABLE `data_modul_processed`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_modul_processed_periode_penilaian_id_foreign` (`periode_penilaian_id`);

--
-- Indeks untuk tabel `data_msg_penilaians`
--
ALTER TABLE `data_msg_penilaians`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_msg_penilaians_periode_penilaian_id_foreign` (`periode_penilaian_id`);

--
-- Indeks untuk tabel `data_penilaians`
--
ALTER TABLE `data_penilaians`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_penilaians_periode_penilaian_id_foreign` (`periode_penilaian_id`),
  ADD KEY `data_penilaians_detail_kriteria_id_foreign` (`detail_kriteria_id`);

--
-- Indeks untuk tabel `data_sudah_mengisis`
--
ALTER TABLE `data_sudah_mengisis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_sudah_mengisis_periode_penilaian_id_foreign` (`periode_penilaian_id`);

--
-- Indeks untuk tabel `data_user_processed`
--
ALTER TABLE `data_user_processed`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_user_processed_periode_penilaian_id_foreign` (`periode_penilaian_id`),
  ADD KEY `data_user_processed_detail_predikat_id_foreign` (`detail_predikat_id`);

--
-- Indeks untuk tabel `data_verifikasi_mengisis`
--
ALTER TABLE `data_verifikasi_mengisis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_verifikasi_mengisis_periode_penilaian_id_foreign` (`periode_penilaian_id`);

--
-- Indeks untuk tabel `detail_kriterias`
--
ALTER TABLE `detail_kriterias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_kriterias_kriteria_id_foreign` (`kriteria_id`);

--
-- Indeks untuk tabel `detail_predikats`
--
ALTER TABLE `detail_predikats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_predikats_predikat_id_foreign` (`predikat_id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kriterias`
--
ALTER TABLE `kriterias`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `periode_penilaians`
--
ALTER TABLE `periode_penilaians`
  ADD PRIMARY KEY (`id`),
  ADD KEY `periode_penilaians_kriteria_id_foreign` (`kriteria_id`),
  ADD KEY `periode_penilaians_predikat_id_foreign` (`predikat_id`);

--
-- Indeks untuk tabel `predikats`
--
ALTER TABLE `predikats`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `data_fasilab_processed`
--
ALTER TABLE `data_fasilab_processed`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `data_kriteria_processed`
--
ALTER TABLE `data_kriteria_processed`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT untuk tabel `data_modul_processed`
--
ALTER TABLE `data_modul_processed`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `data_msg_penilaians`
--
ALTER TABLE `data_msg_penilaians`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `data_penilaians`
--
ALTER TABLE `data_penilaians`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT untuk tabel `data_sudah_mengisis`
--
ALTER TABLE `data_sudah_mengisis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `data_user_processed`
--
ALTER TABLE `data_user_processed`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `data_verifikasi_mengisis`
--
ALTER TABLE `data_verifikasi_mengisis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `detail_kriterias`
--
ALTER TABLE `detail_kriterias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `detail_predikats`
--
ALTER TABLE `detail_predikats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kriterias`
--
ALTER TABLE `kriterias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `periode_penilaians`
--
ALTER TABLE `periode_penilaians`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `predikats`
--
ALTER TABLE `predikats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `data_fasilab_processed`
--
ALTER TABLE `data_fasilab_processed`
  ADD CONSTRAINT `data_fasilab_processed_periode_penilaian_id_foreign` FOREIGN KEY (`periode_penilaian_id`) REFERENCES `periode_penilaians` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `data_kriteria_processed`
--
ALTER TABLE `data_kriteria_processed`
  ADD CONSTRAINT `data_kriteria_processed_data_fasilab_processed_id_foreign` FOREIGN KEY (`data_fasilab_processed_id`) REFERENCES `data_fasilab_processed` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `data_kriteria_processed_data_modul_processed_id_foreign` FOREIGN KEY (`data_modul_processed_id`) REFERENCES `data_modul_processed` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `data_kriteria_processed_data_user_processed_id_foreign` FOREIGN KEY (`data_user_processed_id`) REFERENCES `data_user_processed` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `data_kriteria_processed_detail_kriteria_id_foreign` FOREIGN KEY (`detail_kriteria_id`) REFERENCES `detail_kriterias` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `data_modul_processed`
--
ALTER TABLE `data_modul_processed`
  ADD CONSTRAINT `data_modul_processed_periode_penilaian_id_foreign` FOREIGN KEY (`periode_penilaian_id`) REFERENCES `periode_penilaians` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `data_msg_penilaians`
--
ALTER TABLE `data_msg_penilaians`
  ADD CONSTRAINT `data_msg_penilaians_periode_penilaian_id_foreign` FOREIGN KEY (`periode_penilaian_id`) REFERENCES `periode_penilaians` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `data_penilaians`
--
ALTER TABLE `data_penilaians`
  ADD CONSTRAINT `data_penilaians_detail_kriteria_id_foreign` FOREIGN KEY (`detail_kriteria_id`) REFERENCES `detail_kriterias` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `data_penilaians_periode_penilaian_id_foreign` FOREIGN KEY (`periode_penilaian_id`) REFERENCES `periode_penilaians` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `data_sudah_mengisis`
--
ALTER TABLE `data_sudah_mengisis`
  ADD CONSTRAINT `data_sudah_mengisis_periode_penilaian_id_foreign` FOREIGN KEY (`periode_penilaian_id`) REFERENCES `periode_penilaians` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `data_user_processed`
--
ALTER TABLE `data_user_processed`
  ADD CONSTRAINT `data_user_processed_detail_predikat_id_foreign` FOREIGN KEY (`detail_predikat_id`) REFERENCES `detail_predikats` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `data_user_processed_periode_penilaian_id_foreign` FOREIGN KEY (`periode_penilaian_id`) REFERENCES `periode_penilaians` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `data_verifikasi_mengisis`
--
ALTER TABLE `data_verifikasi_mengisis`
  ADD CONSTRAINT `data_verifikasi_mengisis_periode_penilaian_id_foreign` FOREIGN KEY (`periode_penilaian_id`) REFERENCES `periode_penilaians` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `detail_kriterias`
--
ALTER TABLE `detail_kriterias`
  ADD CONSTRAINT `detail_kriterias_kriteria_id_foreign` FOREIGN KEY (`kriteria_id`) REFERENCES `kriterias` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `detail_predikats`
--
ALTER TABLE `detail_predikats`
  ADD CONSTRAINT `detail_predikats_predikat_id_foreign` FOREIGN KEY (`predikat_id`) REFERENCES `predikats` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `periode_penilaians`
--
ALTER TABLE `periode_penilaians`
  ADD CONSTRAINT `periode_penilaians_kriteria_id_foreign` FOREIGN KEY (`kriteria_id`) REFERENCES `kriterias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `periode_penilaians_predikat_id_foreign` FOREIGN KEY (`predikat_id`) REFERENCES `predikats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
