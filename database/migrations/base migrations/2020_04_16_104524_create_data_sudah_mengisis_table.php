<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataSudahMengisisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_sudah_mengisis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ilab_praktikkan_id');
            $table->bigInteger('ilab_user_id')->nullable();            
            $table->bigInteger('ilab_class_id')->nullable();
            $table->unsignedBigInteger('periode_penilaian_id');
            $table->foreign('periode_penilaian_id')->references('id')->on('periode_penilaians')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_sudah_mengisis');
    }
}
