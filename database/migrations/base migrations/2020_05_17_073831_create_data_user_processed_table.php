<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataUserProcessedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_user_processed', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ilab_user_id');
            $table->integer('total_nilai_user');
            $table->unsignedBigInteger('periode_penilaian_id');
            $table->unsignedBigInteger('detail_predikat_id')->nullable();
            $table->foreign('periode_penilaian_id')->references('id')->on('periode_penilaians')->onDelete('cascade');
            $table->foreign('detail_predikat_id')->references('id')->on('detail_predikats')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_user_processed');
    }
}
