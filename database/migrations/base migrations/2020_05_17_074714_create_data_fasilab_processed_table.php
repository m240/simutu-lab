<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataFasilabProcessedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_fasilab_processed', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('total_nilai_fasilab');
            $table->unsignedBigInteger('periode_penilaian_id');
            $table->foreign('periode_penilaian_id')->references('id')->on('periode_penilaians')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_fasilab_processed');
    }
}
