<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataKriteriaProcessedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_kriteria_processed', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('total_nilai_kriteria');
            $table->unsignedBigInteger('detail_kriteria_id')->nullable();
            $table->unsignedBigInteger('data_user_processed_id')->nullable();
            $table->unsignedBigInteger('data_fasilab_processed_id')->nullable();
            $table->unsignedBigInteger('data_modul_processed_id')->nullable();
            $table->foreign('detail_kriteria_id')->references('id')->on('detail_kriterias')->onDelete('cascade');
            $table->foreign('data_user_processed_id')->references('id')->on('data_user_processed')->onDelete('cascade');
            $table->foreign('data_fasilab_processed_id')->references('id')->on('data_fasilab_processed')->onDelete('cascade');
            $table->foreign('data_modul_processed_id')->references('id')->on('data_modul_processed')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_kriteria_processed');
    }
}
