<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailPredikatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_predikats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('desc');
            $table->text('logo');
            $table->text('nama_predikat');
            $table->integer('max_nilai');
            $table->integer('min_nilai');
            $table->unsignedBigInteger('predikat_id');
            $table->timestamps();
            $table->foreign('predikat_id')->references('id')->on('predikats')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_predikats');
    }
}
