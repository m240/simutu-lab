<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeriodePenilaiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periode_penilaians', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('tahun');
            $table->text('nama_penilaian');
            $table->enum('semester', ['ganjil', 'genap']);
            $table->boolean('active');
            $table->boolean('processed');
            $table->enum('jenis_penilaians', ['asisten', 'instruktur', 'fasil_lab', 'modul']);
            $table->bigInteger('ilab_class_category_id')->nullable();
            $table->unsignedBigInteger('kriteria_id');
            $table->unsignedBigInteger('predikat_id')->nullable();
            $table->foreign('kriteria_id')->references('id')->on('kriterias')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('predikat_id')->references('id')->on('predikats')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('periode_penilaians');
    }
}
