<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class data_kriteria_processed extends Model
{
    protected $table = "data_kriteria_processed";
    protected $fillable = ["total_nilai_kriteria", "detail_kriteria_id", "data_user_processed_id", "data_fasilab_processed_id", "data_modul_processed_id"];
    
}
