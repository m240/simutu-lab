<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class data_fasilab_processed extends Model
{
    protected $table = "data_fasilab_processed";
    protected $fillable = ['total_nilai_fasilab', 'periode_penilaian_id'];

    public function periode_penilaian(){
        return $this->belongsTo('App\Model\Admin\periode_penilaian', 'periode_penilaian_id')->first();
    }
}
