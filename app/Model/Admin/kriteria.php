<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class kriteria extends Model
{
    use SoftDeletes;
    protected $fillable = ['nama_kriteria'];

    public function periode_penilaians()
    {
        return $this->hasMany('App\Model\Admin\periode_penilaian','kriteria_id');
    }

    public function data_penilaians()
    {
        return $this->hasMany('App\Model\Admin\data_penilaian', 'kriteria_penilaians');
    }

    public function detail_kriterias()
    {
        return $this->hasMany('App\Model\Admin\detail_kriteria', 'kriteria_id');
    }
}
