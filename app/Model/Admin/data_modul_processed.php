<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class data_modul_processed extends Model
{
    protected $table = "data_modul_processed";
    protected $fillable = ["total_nilai_modul", 'periode_penilaian_id'];

    public function periode(){
        return $this->belongsTo('App\Model\Admin\periode_penilaian', 'periode_penilaian_id')->first();
    }

    public function periode_penilaian(){
        return $this->belongsTo('App\Model\Admin\periode_penilaian', 'periode_penilaian_id');
    }
    
}
