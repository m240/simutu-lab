<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class data_penilaian extends Model
{
    protected $fillable = ['nilai', 'ilab_praktikkan_id', 'ilab_user_id', 'ilab_class_id', 'periode_penilaian_id', 'detail_kriteria_id'];

    public function periode_penilaians()
    {
        return $this->belongsTo('App\Model\Admin\periode_penilaian', 'periode_penilaian_id');
    }

    public function detail_kriterias()
    {
        return $this->belongsTo('App\Model\Admin\detail_kriteria', 'detail_kriteria_id');
    }
}
