<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class periode_penilaian extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'tahun',
        'nama_penilaian',
        'semester',
        'active',
        'jenis_penilaians',
        'ilab_class_category_id',
        'processed',
        'kriteria_id',
        'predikat_id',
        'semester_id'
    ];

    public function ilab_semester(){
        return $this->belongsTo('App\Model\Ilab\ilab_class_category', 'semester_id');
    }

    public function kriteris()
    {
        return $this->belongsTo('App\Model\Admin\kriteria', 'kriteria_id');
    }

    public function predikats()
    {
        return $this->belongsTo('App\Model\Admin\predikat', 'predikat_id');
    }

    public function data_penilaians()
    {
        return $this->hasMany('App\Model\Admin\data_penilaian', 'periode_penilaian_id');
    }

    public function data_msg_penilaians()
    {
        return $this->hasMany('App\Model\Admin\data_msg_penilaian', 'periode_penilaian_id');
    }
    public function data_sudah_mengisis()
    {
        return $this->belongsTo('App\Model\Admin\data_sudah_mengisi', 'periode_penilaian_id');
    }
    public function data_modul_proc()
    {
        return $this->hasOne('App\Model\Admin\data_modul_processed');
    }
    public function data_fasilab_proc()
    {
        return $this->hasOne('App\Model\Admin\data_fasilab_processed');
    }
    public function data_user_proc()
    {
        return $this->hasMany('App\Model\Admin\data_user_processed');
    }
}
