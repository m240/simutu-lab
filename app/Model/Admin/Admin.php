<?php

namespace App\Model\Admin;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable;
    protected $guard = 'admin';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = ['desc','email','password','nomor_wa','nomor_telp'];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
