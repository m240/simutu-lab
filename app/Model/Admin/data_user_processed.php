<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class data_user_processed extends Model
{
    protected $table = "data_user_processed";
    protected $fillable = ['ilab_user_id', 'total_nilai_user', 'periode_penilaian_id', 'detail_predikat_id'];
}
