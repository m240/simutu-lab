<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class detail_predikat extends Model
{
    use SoftDeletes;
    protected $fillable = ['desc', 'logo', 'nama_predikat', 'max_nilai', 'min_nilai', 'predikat_id'];
    public function detail_predikats()
    {
        return $this->belongsTo('App\Model\Admin\predikat', 'predikat_id');
    }
}
