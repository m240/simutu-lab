<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class data_msg_penilaian extends Model
{
    protected $fillable = ['message', 'ilab_praktikkan_id', 'ilab_user_id', 'ilab_class_id', 'periode_penilaian_id'];

    public function periode_penilaians()
    {
        return $this->belongsTo('App\Model\Admin\periode_penilaian', 'periode_penilaian_id');
    }
    public function ilab_praktikkan(){
        return $this->belongsTo('App\Model\Ilab\ilab_user', 'ilab_praktikkan_id');
    }
}
