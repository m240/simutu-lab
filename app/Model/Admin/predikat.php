<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class predikat extends Model
{
    use SoftDeletes;
    protected $fillable = ['nama_predikat'];

    public function periode_penilaians()
    {
        return $this->hasMany('App\Model\Admin\periode_penilaian', 'predikat_id');
    }

    public function data_predikat_penilaians()
    {
        return $this->hasMany('App\Model\Admin\data_predikat_penilaian', 'predikat_penilaians');
    }

    public function detail_predikats()
    {
        return $this->hasMany('App\Model\Admin\detail_predikat', 'predikat_id');
    }
}
