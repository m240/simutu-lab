<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class detail_kriteria extends Model
{
    use SoftDeletes;
    protected $fillable = ['desc', 'persentase', 'kriteria_id'];
    public function kriterias()
    {
        return $this->belongsTo('App\Model\Admin\kriteria', 'kriteria_id');
    }
    public function data_penilaians()
    {
        return $this->belongsTo('App\Model\Admin\data_penilaian', 'detail_kriteria_id');
    }
    public function data_kriteria_proc()
    {
        return $this->hasMany('App\Model\Admin\data_kriteria_processed');
    }
}
