<?php

namespace App\Model\Ilab;

use Illuminate\Database\Eloquent\Model;

class ilab_class_category extends Model
{
    protected $connection = 'ilab';
    protected $table = 'class_category';

    public function ilab_class(){
        return $this->hasMany('App\Model\Ilab\ilab_class','class_category_id');
    }

    public function ilab_class_category(){
        return $this->hasMany('App\Model\Ilab\ilab_class_category', 'parent', 'id');
    }

    public function ilab_semester(){
        return $this->belongsTo('App\Model\Ilab\ilab_class_category', 'parent', 'id');
    }

    public function penilaians_simutu(){
        $this->hasMany('App\Model\Admin\periode_penilaian','semester_id');
    }
}
