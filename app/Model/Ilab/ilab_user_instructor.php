<?php

namespace App\Model\Ilab;

use Illuminate\Database\Eloquent\Model;

class ilab_user_instructor extends Model
{
    protected $connection = 'ilab';
    protected $table = 'user_instructor';

    public function ilab_user()
    {
        return $this->belongsTo('App\Model\Ilab\ilab_user', 'user_id');
    }

    public function ilab_class()
    {
    	return $this->hasMany('App\Model\Ilab\ilab_class','user_instructor_id');
    }
}
