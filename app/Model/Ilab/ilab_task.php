<?php

namespace App\Model\Ilab;

use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;

class ilab_task extends Model
{
    protected $connection = 'ilab';
    protected $table = 'task';
    protected $appends = ['buktiPraktikum','statusBuktiStr', 'statusBuktiBool'];
    

    // TAMBAHKAN LIBRARY INI KARENA ADA MASALAH KALAU 2 FK
    use Compoships;

    public function ilab_task_detail()
    {
        return $this->hasMany('App\Model\Ilab\ilab_task_detail', 'task_id');
    }

    public function task_detail()
    {
        return $this->belongsTo('\App\Model\Ilab\ilab_task_detail', 'id_task_detail');
    }

    public function class()
    {
        return $this->belongsTo('\App\Model\Ilab\ilab_class', 'id_ilab_class');
    }

    public function getBuktiPraktikumAttribute(){
        $folder_name = md5($this->id."abc").$this->id;
        $path = "http://infotech.umm.ac.id/uploads/screenshot/". $folder_name ."/";
        $file_name = $this->file_screenshot;
        return $path.$file_name;
    }

    public function getStatusBuktiStrAttribute(){
        if($this->file_screenshot == null){
            return 'belum upload';
        }else{
            return 'sudah';
        }
    }

    public function getStatusBuktiBoolAttribute(){
        if($this->file_screenshot == null){
            return false;
        }else{
            return true;
        }
    }
}
