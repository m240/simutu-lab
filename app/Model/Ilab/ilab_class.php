<?php

namespace App\Model\Ilab;

use Illuminate\Database\Eloquent\Model;

class ilab_class extends Model
{
    protected $connection = 'ilab';
    protected $table = 'class';


    public function ilab_class_category()
    {
        return $this->belongsTo('App\Model\Ilab\ilab_class_category', 'class_category_id');
    }

    public function ilab_user_student_class()
    {
        return $this->hasMany('App\Model\Ilab\ilab_user_student_class','class_id');
    }

    public function ilab_user_instructor()
    {
        return $this->belongsTo('App\Model\Ilab\ilab_user_instructor', 'user_instructor_id');
    }

    public function ilab_task()
    {
        return $this->hasMany('App\Model\Ilab\ilab_task', 'class_id');
    }

    public function ilab_presence()
    {
        return $this->hasMany('App\Model\Ilab\ilab_presence', 'class_id');
    }
}
