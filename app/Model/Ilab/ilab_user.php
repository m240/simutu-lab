<?php

namespace App\Model\Ilab;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ilab_user extends Authenticatable
{
    use Notifiable, SoftDeletes;
    protected $connection = 'ilab';
    protected $table = 'user';

    protected $fillable = [
        'user_name', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    public function code_user(){
        $asisten = $this->ilab_user_assistant()->first();
        $instructor = $this->ilab_user_instructor()->first();
        if($asisten == NULL && $instructor == NULL){
            return 0;
        }
        else if($asisten != NULL){
            return 1;
        }
        else if($instructor != NULL){
            return 2;
        }
    }

    public function ilab_user_student(){
        return $this->hasOne('App\Model\Ilab\ilab_user_student', 'user_id');
    }

    public function ilab_user_assistant(){
        return $this->hasOne('App\Model\Ilab\ilab_user_assistant', 'user_id');
    }

    public function ilab_user_instructor(){
        return $this->hasOne('App\Model\Ilab\ilab_user_instructor', 'user_id');
    }

    public function ilab_presence_assistant_instructor(){
        return $this->hasMany('App\Model\Ilab\ilab_presence_assistant_instructor', 'user_id');
    }

    public function ilab_user_admin(){
        return $this->hasOne('App\Model\Ilab\ilab_user_admin', 'user_id');
    }
}
