<?php

namespace App\Model\Ilab;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ilab_user_assistant extends Model
{
    use SoftDeletes;
    protected $connection = 'ilab';
    protected $table = 'user_assistant';

    public function ilab_user()
    {
        return $this->belongsTo('App\Model\Ilab\ilab_user', 'user_id');
    }

    public function ilab_user_assistant_class()
    {
        return $this->hasMany('App\Model\Ilab\ilab_user_assistant_class', 'user_assistant_id');
    }

    public function ilab_class()
    {
        return $this->hasMany('App\Model\Ilab\ilab_class', 'class_id');
    }

    public function ilab_task()
    {
        return $this->hasMany('App\Model\Ilab\ilab_task', 'user_assistant_id');
    }

    public function ilab_task_detail()
    {
        return $this->hasMany('App\Model\Ilab\ilab_task_detail', 'user_assistant_id');
    }
}
