<?php

namespace App\Model\Ilab;

use Illuminate\Database\Eloquent\Model;

class ilab_presence_assistant_instructor extends Model
{
    protected $connection = 'ilab';
    protected $table = 'presence_assistant_instructor';

    public function ilab_presence()
    {
        return $this->belongsTo('App\Model\Ilab\ilab_presence', 'presence_id');
    }
    public function ilab_user()
    {
    	return $this->belongsTo('App\Model\Ilab\ilab_user','user_id');
    }
}
