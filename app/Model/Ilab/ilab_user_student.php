<?php

namespace App\Model\Ilab;

use Illuminate\Database\Eloquent\Model;

class ilab_user_student extends Model
{
    protected $connection = 'ilab';
    protected $table = 'user_student';

    public function ilab_user_student_class()
    {
        return $this->hasMany('App\Model\Ilab\ilab_user_student_class', 'user_student_id');
    }

    public function ilab_user()
    {
        return $this->hasOne('App\Model\Ilab\ilab_user', 'user_id');
    }

    public function task_detail(){
        return $this->hasMany('\App\Model\Ilab\ilab_task_detail', 'id_task_detail');
    }
}
