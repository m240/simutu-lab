<?php

namespace App\Model\Ilab;

use Illuminate\Foundation\Auth\User as Authenticatable;

class ilab_user_admin extends Authenticatable
{
    protected $connection = 'ilab';
    protected $table = 'user_admin';
    protected $appends = ['fullName','userName'];

    public function ilab_user(){
        return $this->belongsTo('App\Model\Ilab\ilab_user', 'user_id');
    }

    public function getFullNameAttribute(){
        return $this->ilab_user->full_name;
    }

    public function getUserNameAttribute(){
        return $this->ilab_user->user_name;
    }

}
