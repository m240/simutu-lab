<?php

namespace App\Model\Ilab;

use Illuminate\Database\Eloquent\Model;

class ilab_presence extends Model
{
    protected $connection = 'ilab';
    protected $table = 'presence';

    public function ilab_presence_assistant_instructor()
    {
        return $this->hasMany('App\Model\Ilab\ilab_presence_assistant_instructor', 'presence_id');
    }
}
