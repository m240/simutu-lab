<?php

namespace App\Model\Ilab;

use Illuminate\Database\Eloquent\Model;

class ilab_user_student_class extends Model
{
    protected $connection = 'ilab';
    protected $table = 'user_student_class';

   public function ilab_user_student()
    {
        return $this->belongsTo('App\Model\Ilab\ilab_user_student', 'user_student_id');
    }
    public function ilab_class()
    {
        return $this->belongsTo('App\Model\Ilab\ilab_class', 'class_id');
    }
}
