<?php

namespace App\Model\Ilab;

use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;

class ilab_task_detail extends Model
{
    protected $connection = 'ilab';
    protected $table = 'task_detail';

    // TAMBAHKAN LIBRARY INI KARENA ADA MASALAH KALAU 2 FK
    use Compoships;

    public function ilab_task()
    {
        return $this->belongsTo('App\Model\Ilab\ilab_task', 'task_id');
    }

    public function task()
    {
        return $this->hasMany('\App\Model\Ilab\ilab_task', 'id_task');
    }

    public function user_student()
    {
        return $this->belongsTo('\App\Model\Ilab\ilab_user_student', 'id_user_student');
    }
}
