<?php

namespace App\Model\Ilab;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ilab_user_assistant_class extends Model
{
    use SoftDeletes;
    protected $connection = 'ilab';
    protected $table = 'user_assistant_class';

    public function ilab_user_assistant()
    {
        return $this->belongsTo('App\Model\Ilab\ilab_user_assistant', 'user_assistant_id');
    }

    public function ilab_class()
    {
        return $this->belongsTo('App\Model\Ilab\ilab_class', 'class_id');
    }

    public function ilab_task(){

    }
}
