<?php

namespace App\Http\Controllers\Praktikkan;

use App\Http\Controllers\Controller;
use App\Model\Admin\Admin;
use App\Model\Admin\data_msg_penilaian;
use App\Model\Admin\data_penilaian;
use App\Model\Admin\data_sudah_mengisi;
use App\Model\Admin\periode_penilaian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FeedbackLabController extends Controller
{
    public function form_kriteria_penilaian()
    {
        $user_id =  Auth::user()->id;
        $periode = periode_penilaian::where([
            ['active','=',1],
            ['processed','=', 0],
            ['jenis_penilaians','=', 'fasil_lab']
        ])->first();
        $status = 0;        
        $data = array();
        $admin = Admin::where('id',1)->first();
        if($periode == null){
            // periode belum aktif
            $status = 1;
            $data['status_isi'] = $status;
            return view('pages.praktikkan.form-feedback-lab',compact('data','admin'));
        }
        $status_isi = data_sudah_mengisi::where([
            ['ilab_praktikkan_id','=',$user_id],
            ['periode_penilaian_id','=',$periode->id]
        ])->first();
        if($status_isi != null){
            // telah mengisi
            $status = 2;
        }
        $kriterias = $periode->kriteris()->first()->detail_kriterias()->get();
        $data['kriterias'] = $kriterias;
        $data['jml_kriteria'] = count($kriterias);
        $data['status_isi'] = $status;
        return view('pages.praktikkan.form-feedback-lab',compact('data','admin'));
    }


    public function submit_form_penilaian(Request $request){
        $user_id =  Auth::user()->id;
        $periode = periode_penilaian::where([
            ['active','=',1],
            ['processed','=', 0],
            ['jenis_penilaians','=', 'fasil_lab']
        ])->first();
        $status_isi = data_sudah_mengisi::where([
            ['ilab_praktikkan_id','=',$user_id],
            ['periode_penilaian_id','=',$periode->id]
        ])->first();
        if($status_isi == null){
            $nilai = array(0, 20, 40, 60, 80, 100);
            $kriterias = $periode->kriteris()->first()->detail_kriterias()->get();
            $jml_detail = $kriterias->toArray();            
            for ($i = 1; $i <= count($jml_detail); $i++) {
                data_penilaian::insert(
                    [
                        "nilai" => $nilai[$request['point' . $jml_detail[$i - 1]['id']]],
                        "ilab_praktikkan_id" => $user_id,
                        "periode_penilaian_id" => $periode->id,
                        "detail_kriteria_id" => $jml_detail[$i - 1]['id']
                    ]
                );
            }
            if (!empty($request->kritnsar)) {
                data_msg_penilaian::insert([
                    "message" => $request->kritnsar,
                    "ilab_praktikkan_id" => $user_id,
                    "periode_penilaian_id" => $periode->id
                ]);
            }

            data_sudah_mengisi::insert([
                "ilab_praktikkan_id" => $user_id,
                "periode_penilaian_id" => $periode->id
            ]);
            return redirect(route('praktikkan.dashboard.index'))->with('status', 'Pengisian penilaian berhasil dilakukan !');
        }else{
            return redirect(route('praktikkan.dashboard.index'))->with('status', 'Anda telah mengisi penilaian laboratorium sebelumnya !');
        }
    }
}
