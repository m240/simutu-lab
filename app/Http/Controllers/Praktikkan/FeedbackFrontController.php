<?php

namespace App\Http\Controllers\Praktikkan;

use App\Http\Controllers\Controller;
use App\Model\Admin\data_kriteria_processed;
use App\Model\Admin\data_user_processed;
use App\Model\Admin\detail_kriteria;
use App\Model\Admin\detail_predikat;
use App\Model\Admin\periode_penilaian;
use App\Model\Ilab\ilab_user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FeedbackFrontController extends Controller
{

    public function index()
    {
        $data = array();
        $user_code = Auth::user()->code_user();
        if($user_code == 2){
            $data = periode_penilaian::where('processed', "1")->where("jenis_penilaians", "instruktur")->get();
        }else if($user_code == 1){
            $data = periode_penilaian::where('processed', "1")->where("jenis_penilaians", "asisten")->get();
        }
        return view('pages.perform.feedback', compact("data"));
    }

    public function detail_feedback($id_periode)
    {
        $data = array();
        $data['periode'] = periode_penilaian::find($id_periode);
        $data['detail_kriteria'] = detail_kriteria::where('kriteria_id', $data['periode']->kriteria_id)->get();
        $data['nilai'] = array();
        $usr_pcoc = data_user_processed::where('periode_penilaian_id',$data['periode']->id)->where('ilab_user_id',Auth::user()->id)->first();
        if($usr_pcoc == null){
            return redirect(route('frontend_feedback_index'))->with('status', 'Mohon maaf masukan feedback yang ditujukan kepada anda belum tersedia');
        }
        $usr = $usr_pcoc->id;
        foreach ($data['detail_kriteria'] as $d) {
            array_push($data['nilai'], data_kriteria_processed::where('detail_kriteria_id', $d->id)->where('data_user_processed_id', $usr)->pluck('total_nilai_kriteria')[0]);
        }
        $data['data_user_proc'] = data_user_processed::find($usr);
        $data['msg'] = $data['periode']->data_msg_penilaians()->where('ilab_user_id', $data['data_user_proc']->ilab_user_id)->get();
        $data['user'] = array();
        $i = 0;
        $data['nim'] = array();
        foreach ($data['msg'] as $d) {
            array_push($data['user'], ilab_user::whereId($d->ilab_praktikkan_id)->pluck('user_name')[0]);
            array_push($data['nim'], $data['user'][$i]);
            $data['user'][$i] = "https://krs.umm.ac.id/Poto/" . substr($data['user'][$i], 0, 4)  . "/" . $data['user'][$i] . ".JPG";

            $i++;
        }
        $data['usernya'] = ilab_user::whereId($data['data_user_proc']->ilab_user_id)->pluck('full_name')[0];
        $data['predikat'] = detail_predikat::where('id',  $usr_pcoc->detail_predikat_id)->first();
        $nilai_vis = array();
        $desc = array();
        $i = 0;
        foreach($data['detail_kriteria'] as $d){
            array_push($desc, $d->desc);
            array_push($nilai_vis, 100*$data['nilai'][$i]/$d->persentase);
            $i++;
        }
        return view('pages.perform.detail_feedback', [
            'data' => $data,
            'nilai_vis' => $nilai_vis,
            'desc_vis' => $desc,
        ]);
    }
}
