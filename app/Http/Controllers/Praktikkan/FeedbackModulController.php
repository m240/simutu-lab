<?php

namespace App\Http\Controllers\Praktikkan;

use App\Http\Controllers\Controller;
use App\Model\Admin\data_msg_penilaian;
use App\Model\Admin\data_penilaian;
use App\Model\Admin\data_sudah_mengisi;
use App\Model\Admin\detail_kriteria;
use App\Model\Admin\kriteria;
use App\Model\Admin\periode_penilaian;
use App\Model\Admin\predikat;
use App\Model\Ilab\ilab_class;
use App\Model\Ilab\ilab_class_category;
use App\Model\Ilab\ilab_user;
use App\Model\Ilab\ilab_user_student;
use App\Model\Ilab\ilab_user_student_class;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FeedbackModulController extends Controller
{
    public function index()
    {
        $tmp = array();
        $user_id =  ilab_user::find(Auth::user()->id);
        $user_student_id = $user_id->ilab_user_student()->get()[0]->id;
        $class_id = ilab_user_student::find($user_student_id)->ilab_user_student_class()->pluck('class_id');
        $class_category_id = ilab_class::find($class_id)->pluck('class_category_id');
        $class_category = ilab_class_category::whereIn('id', $class_category_id)->where('active', '1')->get(['id', 'category_name']);
        $tmp = array();
        foreach ($class_category as $c) {
            $tmp[$c['id']] = $c['category_name'];
        }
        $data['nama_kelas'] = $tmp;
        $data['periode_penilaian'] = periode_penilaian::whereIn('ilab_class_category_id', array_keys($tmp))->where('processed', '0')->where('jenis_penilaians', 'modul')->get();
        $data['jml_kriteria'] = array();
        $id_periode = array();
        foreach ($data['periode_penilaian'] as $i) {
            array_push($data['jml_kriteria'], kriteria::find($i->kriteria_id)->detail_kriterias()->count());
            array_push($id_periode, $i->id);
        }

        $data['udh_isi'] = data_sudah_mengisi::whereIn('periode_penilaian_id', $id_periode)->where('ilab_praktikkan_id', $user_student_id)->orderBy('periode_penilaian_id')->pluck('periode_penilaian_id')->toArray();

        return view('pages.praktikkan.feedback-modul', compact('data'));
    }

    public function detail_feedback_modul_idx($id_predikat)
    {
        $user_id =  ilab_user::find(Auth::user()->id);
        $user_student_id = $user_id->ilab_user_student()->get()[0]->id;
        $temp = data_sudah_mengisi::where('periode_penilaian_id', $id_predikat)->where('ilab_praktikkan_id', $user_student_id)->count();
        if ($temp != 0) {
            return redirect(route('praktikan_feedback_modul_idx'));
        }
        $data['periode_penilaian'] = periode_penilaian::find($id_predikat);
        $data['kelas'] = ilab_class_category::find($data['periode_penilaian']->ilab_class_category_id);
        $data['kriteria'] = kriteria::find($data['periode_penilaian']->kriteria_id)->detail_kriterias()->get();
        $data['jml_kriteria'] = count($data['kriteria']);

        return view('pages.praktikkan.detail-feedback-modul', compact('data'));
    }

    public function tambah_detail_feedback_modul($id_predikat, Request $request)
    {
        $user_id =  ilab_user::find(Auth::user()->id);
        $user_student_id = $user_id->ilab_user_student()->get()[0]->id;
        $temp = data_sudah_mengisi::where('periode_penilaian_id', $id_predikat)->where('ilab_praktikkan_id', $user_student_id)->count();
        if ($temp != 0) {
            return redirect(route('praktikan_feedback_modul_idx'));
        }
        $nilai = array(0, 20, 40, 60, 80, 100);
        $predikat = periode_penilaian::find($id_predikat);
        $jml_detail = detail_kriteria::whereKriteria_id($predikat->kriteria_id)->get();
        $id_praktikan = ilab_user_student::whereUser_id(Auth::user()->id)->take(1)->pluck('id')[0];
        $class_all_id = ilab_class::where('class_category_id', $predikat->ilab_class_category_id)->pluck('id');
        $class_id = ilab_user_student_class::whereIn('class_id', $class_all_id)->where('user_student_id', $id_praktikan)->pluck('class_id')[0];
        for ($i = 1; $i <= count($jml_detail); $i++) {
            data_penilaian::insert(
                [
                    "nilai" => $nilai[$request['point' . $jml_detail[$i - 1]->id]],
                    "ilab_praktikkan_id" => Auth::user()->id,
                    "ilab_class_id" => $class_id,
                    "periode_penilaian_id" => $id_predikat,
                    "detail_kriteria_id" => $jml_detail[$i - 1]->id
                ]
            );
        }
        if (!empty($request->kritnsar)) {
            data_msg_penilaian::insert([
                "message" => $request->kritnsar,
                "ilab_praktikkan_id" => Auth::user()->id,
                "ilab_class_id" => $class_id,
                "periode_penilaian_id" => $id_predikat
            ]);
        }

        data_sudah_mengisi::insert([
            "ilab_praktikkan_id" => $id_praktikan,
            "ilab_class_id" => $class_id,
            "periode_penilaian_id" => $id_predikat
        ]);
        return redirect(route('praktikan_feedback_modul_idx'));
    }
}
