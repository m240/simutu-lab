<?php

namespace App\Http\Controllers\Praktikkan;
use App\Http\Controllers\Controller;
use App\Model\Admin\data_sudah_mengisi;
use App\Model\Admin\kriteria;
use App\Model\Admin\periode_penilaian;
use App\Model\Ilab\ilab_class;
use App\Model\Ilab\ilab_class_category;
use App\Model\Ilab\ilab_user_assistant;
use App\Model\Ilab\ilab_user_assistant_class;
use App\Model\Ilab\ilab_user_student;
use Illuminate\Support\Facades\Auth;
use PDF;

class DashboardController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */

    public function index(){
        $code = Auth::user()->code_user();
        if($code!=2){
            $todo_modul = count($this->get_modul_to_fill()['periode_penilaian']);
            $todo_asisten = $this->get_jumlah_asisten();
            $todo_instruktur = count($this->get_instruktur_to_fill());
            return view('pages.praktikkan.dashboard',[
                'jml_modul' => $todo_modul,
                'jml_asisten' => $todo_asisten,
                'jml_instruktur' => $todo_instruktur,
                'jml_lab' => $this->get_lab_to_fill(),
                'moduls' => $this->get_modul_to_fill(),
                'instrukturs' => $this->get_instruktur_to_fill(),
                'asistens' => $this->get_asisten_to_fill(),
                'lab' => $this->get_status_lab()
            ]);
        }else{ 
            return view('pages.perform.dashboard');
        }
    }

    public function print_bukti(){
        $praktikkan =  Auth::user();
        $pdf = PDF::loadview('layouts.export-bukti',[
            'user' => $praktikkan,
            'tanggal' => date('d-m-Y'),
            'moduls' => $this->get_modul_to_fill(),
            'instrukturs' => $this->get_instruktur_to_fill(),
            'asistens' => $this->get_asisten_to_fill(),
            'lab' => $this->get_status_lab()
            ]);
    	return $pdf->stream();
    }

    public function get_lab_to_fill(){
        $periode = periode_penilaian::where([
            ['active','=',1],
            ['processed','=', 0],
            ['jenis_penilaians','=', 'fasil_lab']
        ])->first();
        if($periode == null){
            return 0;
        }
        $kriterias = $periode->kriteris()->first()->detail_kriterias()->get();
        return count($kriterias);
    }

    public function get_instruktur_to_fill(){
        $praktikkan =  Auth::user();
        $user_student_id = $praktikkan->ilab_user_student()->get()[0]->id;
        $student_class = ilab_user_student::find($user_student_id)->ilab_user_student_class()->get();
        $class_pluck = $student_class->pluck('class_id');
        $data = array();
        $periode = periode_penilaian::where([
            ['active','=',1],
            ['processed','=', 0],
            ['jenis_penilaians','=', 'instruktur']
        ])->first();
        $status = 0;
        if($periode == null){
            // pending
            $status = 1;
        }
        foreach($class_pluck as $id){
            $ilab_class = ilab_class::where([
                ['id','=',$id],
            ])->first();
            $class_category = $ilab_class->ilab_class_category()->first();
            if($class_category->active == 1){
		$usr_instructor = $ilab_class->ilab_user_instructor()->first();      
		if($usr_instructor == NULL) {
		   continue;
		}
                $instructor = $usr_instructor ->ilab_user()->first();
                if($status != 1){
                    $status_isi = data_sudah_mengisi::where([
                        ['ilab_praktikkan_id','=',$praktikkan->id],
                        ['ilab_user_id','=',$instructor->id],
                        ['ilab_class_id','=',$id],
                        ['periode_penilaian_id','=',$periode->id]
                    ])->first();
                    if($status_isi == null){
                        // belum isi
                        $status = 0;
                    }else{
                        //  sudah isi
                        $status = 2;
                    }
                }
                $instructor['sudah_isi'] = $status;
                array_push($data, $instructor);
            }
        }
        return $data;
    }

    public function get_asisten_to_fill(){
        $periode = periode_penilaian::where([
            ['active','=',1],
            ['processed','=', 0],
            ['jenis_penilaians','=', 'asisten']
        ])->first();
        $status = 0;
        if($periode == null){
            $status = 1;
        }
        $data = array();
        $praktikkan =  Auth::user();
        $user_student_id = $praktikkan->ilab_user_student()->get()[0]->id;
        $student_class = ilab_user_student::find($user_student_id)->ilab_user_student_class()->get();
        $class_pluck = $student_class->pluck('class_id');
        $class_active = array();
        foreach($class_pluck as $id){
            $ilab_class = ilab_class::where([
                ['id','=',$id],
            ])->first();
            $class_category = $ilab_class->ilab_class_category()->first();
            if($class_category->active == 1){
                $data_asisten = array();
                $unique_asistant_id = array_unique(ilab_user_assistant_class::where('class_id',$ilab_class->id)->pluck('user_assistant_id')->toArray());
                foreach($unique_asistant_id as $asistant_id){
                    $user = ilab_user_assistant::where('id',$asistant_id)->where('deleted_at', null)->first()->ilab_user()->first();
                    if($status != 1){
                        $status_isi = data_sudah_mengisi::where([
                            ['ilab_praktikkan_id','=',$praktikkan->id],
                            ['ilab_user_id','=',$user->id],
                            ['ilab_class_id','=',$ilab_class->id],
                            ['periode_penilaian_id','=',$periode->id]
                        ])->first();
                        if($status_isi == null){
                            $status = 0;
                        }else{
                            $status = 2;
                        }
                    }
                    $user['sudah_isi'] = $status;
                    array_push($data_asisten,$user);
                }
                $ilab_class['data_asisten'] = $data_asisten;
                array_push($class_active,$ilab_class);
            }
        }
        $data = $class_active;
        return $data;
    }

    public function get_jumlah_asisten(){
        $data = array();
        $praktikkan =  Auth::user();
        $user_student_id = $praktikkan->ilab_user_student()->get()[0]->id;
        $student_class = ilab_user_student::find($user_student_id)->ilab_user_student_class()->get();
        $class_pluck = $student_class->pluck('class_id');
        foreach($class_pluck as $id){
            $ilab_class = ilab_class::where([
                ['id','=',$id],
            ])->first();
            if($ilab_class->ilab_class_category()->first()->active == 1){
                $asistants = ilab_user_assistant_class::where('class_id',$ilab_class->id)->where('deleted_at', '=' ,null)->pluck('user_assistant_id')->toArray();
                foreach($asistants as $id_asisten){
                    array_push($data, $id_asisten);
                }
            }
        }
        return count(array_unique($data));
    }

    public function get_modul_to_fill(){
        $tmp = array();
        $praktikkan =  Auth::user();
        $user_student_id = $praktikkan->ilab_user_student()->get()[0]->id;
        $class_id = ilab_user_student::find($user_student_id)->ilab_user_student_class()->pluck('class_id');
        $class_category_id = ilab_class::find($class_id)->pluck('class_category_id');
        $class_category = ilab_class_category::whereIn('id', $class_category_id)->where('active', '1')->get(['id', 'category_name']);
        $tmp = array();
        foreach ($class_category as $c) {
            $tmp[$c['id']] = $c['category_name'];
        }
        $data['nama_kelas'] = $tmp;
        $data['periode_penilaian'] = periode_penilaian::whereIn('ilab_class_category_id', array_keys($tmp))->where('processed', '0')->where('jenis_penilaians', 'modul')->get();
        $data['jml_kriteria'] = array();
        $id_periode = array();
        foreach ($data['periode_penilaian'] as $i) {
            array_push($data['jml_kriteria'], kriteria::find($i->kriteria_id)->detail_kriterias()->count());
            array_push($id_periode, $i->id);
        }
        $data['udh_isi'] = data_sudah_mengisi::whereIn('periode_penilaian_id', $id_periode)->where('ilab_praktikkan_id', $user_student_id)->orderBy('periode_penilaian_id')->pluck('periode_penilaian_id')->toArray();
        return $data;
    }

    public function get_status_lab(){
        $praktikkan =  Auth::user();
        $periode = periode_penilaian::where([
            ['active','=',1],
            ['processed','=', 0],
            ['jenis_penilaians','=', 'fasil_lab']
        ])->first();
        $status = 0;
        $data = array();
        if($periode == null){
            $status = 1;
        }else{
            $data['nama'] = $periode->nama_penilaian;
            $status_isi = data_sudah_mengisi::where([
                ['ilab_praktikkan_id','=',$praktikkan->id],
                ['periode_penilaian_id','=',$periode->id]
            ])->first();
            if($status_isi == null){
                $status = 0;
            }else{
                $status = 2;
            }
        }
        $data['sudah_isi'] = $status;
        return $data;
    }
}