<?php

namespace App\Http\Controllers\Praktikkan;

use App\Http\Controllers\Controller;
use App\Model\Admin\data_msg_penilaian;
use App\Model\Admin\data_penilaian;
use App\Model\Admin\data_sudah_mengisi;
use App\Model\Admin\periode_penilaian;
use App\Model\Ilab\ilab_class;
use App\Model\Ilab\ilab_user;
use App\Model\Ilab\ilab_user_instructor;
use App\Model\Ilab\ilab_user_student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FeedbackInstrukturController extends Controller
{
    public function index()
    {
        $periode = periode_penilaian::where([
            ['active','=',1],
            ['processed','=', 0],
            ['jenis_penilaians','=', 'instruktur']
        ])->first();
        if($periode == null){
            return view('pages.praktikkan.feedback-instruktur', [
                'status' => 0
            ]);
        }else{
            $data = array();
            $user_id =  ilab_user::find(Auth::user()->id);
            $user_student_id = $user_id->ilab_user_student()->get()[0]->id;
            $student_class = ilab_user_student::find($user_student_id)->ilab_user_student_class()->get();
            $class_pluck = $student_class->pluck('class_id');
            $class_active = array();
            foreach($class_pluck as $id){
                $ilab_class = ilab_class::where([
                    ['id','=',$id],
                ])->first();
                $class_category = $ilab_class->ilab_class_category()->first();
                if($class_category->active == 1){
                    $ilab_class['instructor'] = $ilab_class->ilab_user_instructor()->first()->ilab_user()->first();
                    $status_isi = data_sudah_mengisi::where([
                        ['ilab_praktikkan_id','=',$user_id->id],
                        ['ilab_user_id','=',$ilab_class->instructor->id],
                        ['ilab_class_id','=',$id],
                        ['periode_penilaian_id','=',$periode->id]
                    ])->first();
                    if($status_isi == null){
                        $ilab_class['sudah_isi'] = 0;
                    }else{
                        $ilab_class['sudah_isi'] = 1;
                    }
                    array_push($class_active,$ilab_class);
                }
            }
            $data['class_active'] = $class_active;
            return view('pages.praktikkan.feedback-instruktur', [
                'data' => $data,
                'status' => 1
            ]);
        }
    }

    public function form_kriteria_penilaian($class_id, $instruktur_id)
    {
        $user_id =  Auth::user()->id;
        if(!$this->range_penilaian($class_id,$instruktur_id)){
            return redirect(route('praktikkan.dashboard.index'))->with('status', 'penilaian diluar range anda!');
        }
        $periode = periode_penilaian::where([
            ['active','=',1],
            ['processed','=', 0],
            ['jenis_penilaians','=', 'instruktur']
        ])->first();
        $status_isi = data_sudah_mengisi::where([
            ['ilab_praktikkan_id','=',$user_id],
            ['ilab_user_id','=',$instruktur_id],
            ['ilab_class_id','=',$class_id],
            ['periode_penilaian_id','=',$periode->id]
        ])->first();
        if($status_isi == null){
            $data = array();
            $kriterias = $periode->kriteris()->first()->detail_kriterias()->get();
            $instruktur = ilab_user::where('id',$instruktur_id)->first();
            $data['kriterias'] = $kriterias;
            $data['instruktur'] = $instruktur;
            $data['jml_kriteria'] = count($kriterias);
            $data['class_id'] = $class_id;
            return view('pages.praktikkan.form-feedback-instruktur',compact('data'));
        }else{
            return redirect(route('praktikkan.dashboard.index'))->with('status', 'Anda telah mengisi penilaian asisten sebelumnya!');
        }
    }

    public function range_penilaian($id_class, $instruktur_id){   
        $user_id =  ilab_user::find(Auth::user()->id);     
        $user_student_id = $user_id->ilab_user_student()->get()[0]->id;
        $student_class = ilab_user_student::find($user_student_id)->ilab_user_student_class()->get();
        $class_pluck = $student_class->pluck('class_id')->toArray();
        $classes = ilab_class::whereIn('id', $class_pluck)->get();
        $list_class = array();
        foreach($classes as $class){
            if($class->ilab_class_category()->first()->active==1){
                array_push($list_class, $class->id);
            }
        }
        if(!in_array($id_class, $list_class)){
            return false;
        }
        $instruktur_pluck = ilab_class::whereIn('id',$list_class)->get()->pluck('user_instructor_id')->toArray();
        $list_instruktur = ilab_user_instructor::whereIn('id', $instruktur_pluck)->get()->pluck('user_id')->toArray();   
        if(!in_array($instruktur_id, $list_instruktur)){
            return false;
        }
        return true;
    }

    public function submit_form_penilaian($id_class,$id_instruktur,Request $request){
        $user_id =  Auth::user()->id;
        $periode = periode_penilaian::where([
            ['active','=',1],
            ['processed','=', 0],
            ['jenis_penilaians','=', 'instruktur']
        ])->first();
        $status_isi = data_sudah_mengisi::where([
            ['ilab_praktikkan_id','=',$user_id],
            ['ilab_user_id','=',$id_instruktur],
            ['ilab_class_id','=',$id_class],
            ['periode_penilaian_id','=',$periode->id]
        ])->first();
        if($status_isi == null){
            $nilai = array(0, 20, 40, 60, 80, 100);
            $kriterias = $periode->kriteris()->first()->detail_kriterias()->get();
            $jml_detail = $kriterias->toArray();            
            for ($i = 1; $i <= count($jml_detail); $i++) {
                data_penilaian::insert(
                    [
                        "nilai" => $nilai[$request['point' . $jml_detail[$i - 1]['id']]],
                        "ilab_praktikkan_id" => $user_id,
                        "ilab_user_id" => $id_instruktur,
                        "ilab_class_id" => $id_class,
                        "periode_penilaian_id" => $periode->id,
                        "detail_kriteria_id" => $jml_detail[$i - 1]['id']
                    ]
                );
            }
            if (!empty($request->kritnsar)) {
                data_msg_penilaian::insert([
                    "message" => $request->kritnsar,
                    "ilab_praktikkan_id" => $user_id,
                    "ilab_user_id" => $id_instruktur,
                    "ilab_class_id" => $id_class,
                    "periode_penilaian_id" => $periode->id
                ]);
            }

            data_sudah_mengisi::insert([
                "ilab_praktikkan_id" => $user_id,
                "ilab_user_id" => $id_instruktur,
                "ilab_class_id" => $id_class,
                "periode_penilaian_id" => $periode->id
            ]);
            return redirect(route('praktikan_feedback_instruktur_idx'))->with('status', 'Pengisian penilaian berhasil dilakukan !');
        }else{
            return redirect(route('praktikkan.dashboard.index'))->with('status', 'Anda telah mengisi penilaian instruktur sebelumnya !');
        }
    }
}
