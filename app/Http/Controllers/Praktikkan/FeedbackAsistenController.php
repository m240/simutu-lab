<?php

namespace App\Http\Controllers\Praktikkan;

use App\Http\Controllers\Controller;
use App\Model\Admin\data_msg_penilaian;
use App\Model\Admin\data_penilaian;
use App\Model\Admin\data_sudah_mengisi;
use App\Model\Admin\periode_penilaian;
use App\Model\Ilab\ilab_class;
use App\Model\Ilab\ilab_user;
use App\Model\Ilab\ilab_user_assistant;
use App\Model\Ilab\ilab_user_assistant_class;
use App\Model\Ilab\ilab_user_student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FeedbackAsistenController extends Controller
{
    public function index()
    {
        $periode = periode_penilaian::where([
            ['active','=',1],
            ['processed','=', 0],
            ['jenis_penilaians','=', 'asisten']
        ])->first();
        if($periode == null){
            return view('pages.praktikkan.feedback-asisten', [
                'status' => 0
            ]);
        }else{
            $data = array();
            $user_id =  ilab_user::find(Auth::user()->id);
            $user_student_id = $user_id->ilab_user_student()->get()[0]->id;
            $student_class = ilab_user_student::find($user_student_id)->ilab_user_student_class()->get();
            $class_pluck = $student_class->pluck('class_id');
            $class_active = array();
            foreach($class_pluck as $id){
                $ilab_class = ilab_class::where([
                    ['id','=',$id],
                ])->first();
                $class_category = $ilab_class->ilab_class_category()->first();
                $unique_asistant = ilab_user_assistant_class::where('class_id',$ilab_class->id)->where('deleted_at', null)->pluck('user_assistant_id')->toArray();
                $ilab_class['total_asisten'] = count(array_unique($unique_asistant));
                if($class_category->active == 1){
                    array_push($class_active,$ilab_class);
                }
            }
            $data['class_active'] = $class_active;
            return view('pages.praktikkan.feedback-asisten', [
                'data' => $data,
                'status' => 1
            ]);
        }
    }

    public function list_asisten_by_class($id_class)
    {
        $data_asisten = array();
        $user_id =  Auth::user()->id;
        $periode = periode_penilaian::where([
            ['active','=',1],
            ['processed','=', 0],
            ['jenis_penilaians','=', 'asisten']
        ])->first();
        $unique_asistant_id = array_unique(ilab_user_assistant_class::where('class_id',$id_class)->where('deleted_at', null)->pluck('user_assistant_id')->toArray());
        foreach($unique_asistant_id as $asistant_id){
            $user = ilab_user_assistant::where('id',$asistant_id)->first()->ilab_user()->first();
            $status_isi = data_sudah_mengisi::where([
                ['ilab_praktikkan_id','=',$user_id],
                ['ilab_user_id','=',$user->id],
                ['ilab_class_id','=',$id_class],
                ['periode_penilaian_id','=',$periode->id]
            ])->first();
            if($status_isi == null){
                $user['sudah_isi'] = 0;
            }else{
                $user['sudah_isi'] = 1;
            }
            array_push($data_asisten,$user);
        }
        return $data_asisten;
    }

    public function form_kriteria_penilaian($class_id)
    {
        if(!in_array($class_id, $this->range_pengisian_kelas())){
            return redirect(route('praktikkan.dashboard.index'))->with('status', 'Kelas diluar range anda!');
        }
        $user_id =  Auth::user()->id;
        $periode = periode_penilaian::where([
            ['active','=',1],
            ['processed','=', 0],
            ['jenis_penilaians','=', 'asisten']
        ])->first();
        $data = array();
        $data['asistens'] = $this->list_asisten_by_class($class_id);
        $kriterias = $periode->kriteris()->first()->detail_kriterias()->get();
        $data['kriterias'] = $kriterias;
        $data['jml_kriteria'] = count($kriterias);
        $data['class_id'] = $class_id;
        $data['class'] = ilab_class::where('id',$class_id)->first();
        return view('pages.praktikkan.form-feedback-asisten',compact('data'));
           
    }

    public function range_pengisian_kelas(){
        $user_id =  ilab_user::find(Auth::user()->id);
        $user_student_id = $user_id->ilab_user_student()->get()[0]->id;
        $student_class = ilab_user_student::find($user_student_id)->ilab_user_student_class()->get();
        $class_pluck = $student_class->pluck('class_id');
        $class_active = array();
        foreach($class_pluck as $id){
            $ilab_class = ilab_class::where([
                ['id','=',$id],
            ])->first();
            $class_category = $ilab_class->ilab_class_category()->first();
            if($class_category->active == 1){
                array_push($class_active,$ilab_class->id);
            }
        }
        return $class_active;
    }

    public function submit_form_penilaian($id_class,Request $request){
        $user_id =  Auth::user()->id;
        $periode = periode_penilaian::where([
            ['active','=',1],
            ['processed','=', 0],
            ['jenis_penilaians','=', 'asisten']
        ])->first();
        $asistens =  $request->input('asisten');
        foreach($asistens as $key => $value){
            $status_isi = data_sudah_mengisi::where([
                ['ilab_praktikkan_id','=',$user_id],
                ['ilab_user_id','=',$key],
                ['ilab_class_id','=',$id_class],
                ['periode_penilaian_id','=',$periode->id]
            ])->first();
            if($status_isi == null){
                $nilai = array(0, 20, 40, 60, 80, 100);
                $kriterias = $periode->kriteris()->first()->detail_kriterias()->get();
                $jml_detail = $kriterias->toArray();            
                for ($i = 1; $i <= count($jml_detail); $i++) {
                    $idx_nilai = 5;
                    if($value['point'.$jml_detail[$i - 1]['id']] != null){
                        $idx_nilai = $value['point'.$jml_detail[$i - 1]['id']];
                    }
                    data_penilaian::insert(
                        [
                            "nilai" => $nilai[$idx_nilai],
                            "ilab_praktikkan_id" => $user_id,
                            "ilab_user_id" => $key,
                            "ilab_class_id" => $id_class,
                            "periode_penilaian_id" => $periode->id,
                            "detail_kriteria_id" => $jml_detail[$i - 1]['id']
                        ]
                    );
                }
                if (!empty($value['kritnsar'])) {
                    data_msg_penilaian::insert([
                        "message" => $value['kritnsar'],
                        "ilab_praktikkan_id" => $user_id,
                        "ilab_user_id" => $key,
                        "ilab_class_id" => $id_class,
                        "periode_penilaian_id" => $periode->id
                    ]);
                }
                data_sudah_mengisi::insert([
                    "ilab_praktikkan_id" => $user_id,
                    "ilab_user_id" => $key,
                    "ilab_class_id" => $id_class,
                    "periode_penilaian_id" => $periode->id
                ]);                
            }
        }
        return redirect(route('praktikan_feedback_asisten_idx'))->with('status', 'Pengisian penilaian berhasil dilakukan !');        
    }
}
