<?php

namespace App\Http\Controllers\Praktikkan\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Model\Admin\Desc;
use App\Model\Ilab\ilab_user;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */

    public function index(){
        $desc = Desc::find(1)->desc;
        if (Auth::check()) {
            return redirect()->route('praktikkan.dashboard.index');
        }
        return view('layouts.landing',compact('desc'));
    }

    public function login(Request $request)
    {
        $username = $request->input('username');
        $password = sha1(md5($request->input('password')."asjkdfh")."shsadfgkj");
        $user = ilab_user::where([
            ['user_name', $username],
            ['password', $password],
        ])->first();
        if($user) {
            Auth::login($user);
            return redirect()->route('praktikkan.dashboard.index');
        }else{
            return redirect()->back()->with('status','username or password is wrong!');
        }
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('landing');
    }
}