<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Ilab\ilab_class_category;
use App\Model\Ilab\ilab_user;

class TestController extends Controller
{

    public function testIlabUser()
    {
        $ilab_user = ilab_user::all();
        return json_encode($ilab_user);
    }

    public function classCategory(){
        $class = ilab_class_category::where('id',137)->first()->ilab_class;
        return $class;
    }

    public function activeClass(){
        $class = ilab_class_category::where('active',1)->get();
        return $class;
    }

}
