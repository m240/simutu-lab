<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Ilab\ilab_class_category;
use PDF;

class PraktikumOnlineController extends Controller
{
    public function index()
    {
        $classCategories = ilab_class_category::where('parent', '0')->orderBy('id', 'desc')->get();
        return view('pages.admin.praktikum_online_index', compact('classCategories'));
    }

    public function category_list($id)
    {
        $classCat = ilab_class_category::where('id', $id)->first();
        $class_list = ilab_class_category::where([
            ['parent', $id],
            ['is_teori_class', '0']
        ])->get();
        return view('pages.admin.praktikum_online_class', compact('classCat','class_list'));
    }

    public function class_list($id){
        $class_list = ilab_class_category::where('id',$id)->with(['ilab_semester','ilab_class'])->get();
        return view('pages.admin.praktikum_online_class_list', compact('class_list'));
    }

    public function class_list_task($id_sem, $id_class){
        $class = ilab_class_category::where('id',$id_sem)->with(['ilab_semester','ilab_class'=>function($query) use ($id_class){
            $query->where('id', $id_class)->with(['ilab_task'=>function($query){}])->get()[0];
        }])->get()[0];
        return view('pages.admin.praktikum_online_class_task', compact('class'));
    }

    public function generate_recap($id){
        set_time_limit(0);
        $class_list = ilab_class_category::where('id',$id)->with(['ilab_semester','ilab_class','ilab_class'=>function($query){
            return $query->with('ilab_task')->get()[0];
        }])->get()[0];
        $pdf = PDF::loadview('layouts.export-praktikum-online',[
            'class' => $class_list,
            'tanggal' => date('d-m-Y'),
            ])->setPaper('legal', 'landscape');
    	return $pdf->stream();
    }
}
