<?php

namespace App\Http\Controllers\Admin;

use App\Exports\EvaluasiPraktikumSemesterExport;
use App\Http\Controllers\Controller;
use App\Model\Ilab\ilab_class;
use App\Model\Ilab\ilab_class_category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class EvaluasiPraktikumController extends Controller
{
    public function index()
    {
        $classCategories = ilab_class_category::withCount(['ilab_class_category' => function ($query) {
            $query->where('is_teori_class', '!=', 1);
        }])
            ->where('parent', '0')->orderBy('id', 'desc')->get();

        return view('pages.admin.evaluasi_praktikum', compact('classCategories'));
    }

    public function semester($id)
    {
        $idx = $id;

        $classCategories = ilab_class_category::with(['ilab_class_category' => function ($query) {
            $query->where('is_teori_class', '!=', 1);
            $query->withCount('ilab_class');
            $query->with(['ilab_class' => function ($query) {
                $query->with(['ilab_task' => function ($query) {
                    $query->withCount(['ilab_task_detail as avg_task_detail' => function ($query) {
                        $query->select(DB::raw('avg(grade)'))->where('grade', '!=', 0);
                    }])->where('extra', 0)->where('deleted_at', null);
                }])->where('deleted_at', null);
            }])->where('deleted_at', null);
        }])->where('parent', '0')->orderBy('id', 'desc')->find($id);


        $arrGradeLabel = array();
        $arrGradeAvg = array();
        $arrExamAvg = array();
        $arrFinalAvg = array();

        foreach ($classCategories->ilab_class_category as $i => $category) {
            // Search Grade non Final Exam
            $arrGrade = array();
            foreach ($category->ilab_class as $class) {
                $arrGrade[] = $class->ilab_task->where('final_exam', 0)->avg('avg_task_detail');
            }
            if (count($arrGrade) != 0) {
                $arrGradeAvg[] = round(array_sum($arrGrade) / count($arrGrade), 2);
            } else {
                $arrGradeAvg[] = 0;
            }

            // Search Grade Final Exam
            $arrExam = array();
            foreach ($category->ilab_class as $class) {
                $arrExam[] = $class->ilab_task->where('final_exam', 1)->avg('avg_task_detail');
            }
            if (count($arrGrade) != 0) {
                $arrExamAvg[] = round(array_sum($arrExam) / count($arrExam), 2);
            } else {
                $arrExamAvg[] = 0;
            }
            $arrGradeLabel[] = $category->category_name;
        }

        // Merge 2 Array and get average
        for ($i = 0; $i < count($arrGradeAvg); $i++) {
            for ($j = 0; $j < count($arrExamAvg); $j++) {
                if ($i == $j) {
                    $arrFinalAvg[] = round(($arrGradeAvg[$i] + $arrExamAvg[$j]) / 2, 2);
                }
            }
        }


        return view('pages.admin.evaluasi_praktikum_semester', compact('idx', 'classCategories',
            'arrGradeLabel', 'arrGradeAvg', 'arrExamAvg', 'arrFinalAvg'));
    }

    public function category($id, $idcategory)
    {
        $idx = $id;

        $classCategories = ilab_class_category::with(['ilab_class' => function ($query) {
            $query->where('deleted_at', null);
        }])->find($idcategory);

        return view('pages.admin.evaluasi_praktikum_semester_category', compact('idx', 'idcategory', 'classCategories'));
    }

    public function classes($id, $idcategory, $idclasses)
    {
        $idx = $id;

        $classCategories = ilab_class_category::with(['ilab_class' => function ($query) {
            $query->where('deleted_at', null);
        }])->find($idcategory);

        $classes = ilab_class::with(['ilab_task' => function ($query) {
            $query->where('deleted_at', null);
            $query->orderBy('title', 'asc');
            $query->withCount(['ilab_task_detail as get_max' => function ($query) {
                $query->select(DB::raw('max(grade)'))->where('grade', '!=', 100);
            }]);
            $query->withCount(['ilab_task_detail as get_avg' => function ($query) {
                $query->select(DB::raw('avg(grade)'))->where('grade', '!=', 0);
            }]);
            $query->withCount(['ilab_task_detail as get_100' => function ($query) {
                $query->select(DB::raw('count(grade)'))->where('grade', 100);
            }]);
        }])->find($idclasses);


        $getLabelChart = array();
        $getAvgChart = array();

        foreach ($classes->ilab_task as $task) {
            $getLabelChart[] = $task->title;
            $getAvgChart[] = $task->get_avg;
        }


        return view('pages.admin.evaluasi_praktikum_semester_category_class', compact('idx', 'idcategory',
            'idclasses', 'classCategories', 'classes', 'getLabelChart', 'getAvgChart'));

    }

    public function semesterExport(Request $request)
    {

        $arr = $request->all();
        unset($arr['_token']);
        $attributes = collect();
        for ($i = 0; $i < sizeof($arr['category_name']); $i++) {
            $attributes->push([
                '#' => $i + 1,
                'kategori' => $arr['category_name'][$i],
                'jumlah_kelas' => $arr['category_count'][$i],
                'avgModul' => $arr['avg_modul'][$i],
                'avgUAP' => $arr['avg_uap'][$i]
            ]);
        }

        return Excel::download(new EvaluasiPraktikumSemesterExport($attributes), 'Evaluasi Praktikum.csv');
    }
}
