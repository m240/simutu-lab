<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Ilab\ilab_class;
use App\Model\Ilab\ilab_class_category;
use App\Model\Ilab\ilab_user_instructor;
use App\Exports\PraktikumAllInstructorExport;
use App\Exports\PraktikumDetailInstructorExport;
use Maatwebsite\Excel\Facades\Excel;

class PraktikumInstrukturController extends Controller
{
    public function index()
    {
        $data = ilab_class_category::where('parent', "0")->orderBy('id', 'desc')->where('deleted_at', null)->get();
        return view('pages.admin.praktikum_periode_instruktur_index', compact('data'));
    }

    public function detail($id)
    {

        $idclass = $id;
        $var = 0;
        $var2 = 0;
        $temp = 0;

        $selectPeriode = ilab_class_category::find($idclass);
        $selectPeriode = $selectPeriode->category_name;

        $selectSemester = strtoupper(explode(' ', $selectPeriode)[1]);
        $selectTahunMulai = explode('-', explode(' ', $selectPeriode)[2])[0];
        $selectTahunAkhir = explode('-', explode(' ', $selectPeriode)[2])[1];

        $startRekap = '';
        $endRekap = '';
        $selectTahun = '';
        if ($selectSemester == 'GENAP') {
            $startRekap = $selectTahunMulai . "-09-31";
            $endRekap = $selectTahunMulai . "-01-31";
            $selectTahun = $selectTahunMulai;
        } else {
            $startRekap = $selectTahunAkhir . "-02-01";
            $endRekap = $selectTahunAkhir . "-08-31";
            $selectTahun = $selectTahunAkhir;
        };
        $allinstructor = ilab_user_instructor::orderBy('created_at', 'asc')->get();

        $avgKeh[] = array();
        $avgKehAsc[] = array();
        $avgKehDesc[] = array();
        $nama[] = array();
        $namaAsc[] = array();
        $namaDesc[] = array();


        $instructorPresence = array();
        $instructorName = array();

        foreach ($allinstructor as $instructor) {
            if (count($instructor->ilab_class) > 0) {
                $instructorPresence[] = round(count($instructor->ilab_user->ilab_presence_assistant_instructor) / count($instructor->ilab_class), 2);

                $instructorName[] = $instructor->ilab_user->full_name;
            }
        }

        $arrayCombine = array();
        foreach ($instructorName as $i => $ass) {
            $arrayCombine[] = array("result" => $instructorPresence[$i], "name" => $instructorName[$i]);
        }

        // Sort
        $arrayCombineAsc = $this->bubbleSortAsc($arrayCombine);
        $arrayCombineDesc = $this->bubbleSortDesc($arrayCombine);

        // Split Array Ascending
        $nameAsc = array();
        $resultAsc = array();
        foreach ($arrayCombineAsc as $i => $key) {
            if ($i <= 5) {
                $resultAsc[] = $arrayCombineAsc[$i]['result'];
                $nameAsc[] = $arrayCombineAsc[$i]['name'];
            }
        }

        // Split Array Descending
        $nameDesc = array();
        $resultDesc = array();
        foreach ($arrayCombineDesc as $i => $key) {
            if ($i <= 5) {
                $resultDesc[] = $arrayCombineDesc[$i]['result'];
                $nameDesc[] = $arrayCombineDesc[$i]['name'];
            }
        }



        return view('pages.admin.praktikum_periode_instruktur_detail_index', compact('selectTahunAkhir', 'allinstructor', 'selectPeriode', 'var', 'var2', 'startRekap', 'endRekap', 'idclass', 'instructorPresence', 'nameAsc', 'resultAsc', 'nameDesc', 'resultDesc', 'selectSemester', 'selectTahun', 'temp', 'avgKehAsc', 'avgKehDesc', 'namaAsc', 'namaDesc', 'nama', 'avgKeh'));
    }

    function bubbleSortAsc($arr)
    {
        $n = sizeof($arr);

        for ($i = 0; $i < $n; $i++) {
            for ($j = 0; $j < $n - $i - 1; $j++) {
                if ($arr[$j] < $arr[$j + 1]) {
                    $t = $arr[$j];
                    $arr[$j] = $arr[$j + 1];
                    $arr[$j + 1] = $t;
                }
            }
        }
        return $arr;
    }

    function bubbleSortDesc($arr)
    {
        $n = sizeof($arr);

        for ($i = 0; $i < $n; $i++) {
            for ($j = 0; $j < $n - $i - 1; $j++) {
                if ($arr[$j] > $arr[$j + 1]) {
                    $t = $arr[$j];
                    $arr[$j] = $arr[$j + 1];
                    $arr[$j + 1] = $t;
                }
            }
        }
        return $arr;
    }

    public function instructordetail($tahun, $ins, $id)
    {


        $selectPeriode = ilab_class_category::find($tahun);
        $selectPeriode = $selectPeriode->category_name;

        $selectSemester = strtoupper(explode(' ', $selectPeriode)[1]);
        $selectTahunMulai = explode('-', explode(' ', $selectPeriode)[2])[0];
        $selectTahunAkhir = explode('-', explode(' ', $selectPeriode)[2])[1];

        $startRekap = '';
        $endRekap = '';
        $selectTahun = '';
        if ($selectSemester == 'GANJIL') {
            $startRekap = $selectTahunMulai . "-09-01";
            $endRekap = $selectTahunMulai . "-01-31";
            $selectTahun = $selectTahunMulai;
        } else {
            $startRekap = $selectTahunAkhir . "-02-01";
            $endRekap = $selectTahunAkhir . "-08-31";
            $selectTahun = $selectTahunAkhir;
        }

        $var = 0;
        $var2 = 0;
        $var3 = 0;
        $count = 0;
        $count2 = array();
        $temp = 0;
        $temp2 = 0;





        $data = ilab_class::where('user_instructor_id', $id)->where('deleted_at', null)
            ->whereBetween('updated_at', [$startRekap, $endRekap])->get();

        $instructorPresence = array();
        $instructorName = array();


        $instructor = ilab_user_instructor::with([
            'ilab_user_instructor.ilab_class' => function ($query) use ($selectTahun, $startRekap, $endRekap) {
                $query->where('deleted_at', null);
                $query->whereYear('updated_at', $selectTahun);
                $query->whereBetween('updated_at', [$startRekap, $endRekap]);
            },
        ])->where('user_id', $ins);


        $avgModul = array();
        $avgUAP = array();
        $instructorClass = array();

        $UAP = array();
        $instructorUAP = array();



        return view('pages.admin.praktikum_periode_instruktur_detail_instruktur_index', compact('data', 'selectPeriode', 'instructor', 'endRekap', 'startRekap', 'var', 'var2', 'var3', 'count', 'count2', 'temp', 'temp2', 'instructorClass', 'selectPeriode', 'selectTahun', 'selectSemester', 'avgModul', 'avgUAP', 'tahun', 'ins', 'instructorUAP', 'UAP'));
    }

    public function exportAllInstruktur(Request $request)
    {

        $arr = $request->all();
        unset($arr['_token']);
        $attributes = collect();
        for ($i = 0; $i < sizeof($arr['nama']); $i++) {
            $attributes->push([
                '#' => $i + 1,
                'Instruktur' => $arr['nama'][$i],
                'Jumlah Kelas Diampu' => $arr['jumlah'][$i],
                'Average Kehadiran' => $arr['avgKehadiran'][$i]
            ]);
        }

        $fileName = 'Rekap Instruktur ' . $arr['title'] . '.csv';

        return Excel::download(new PraktikumAllInstructorExport($attributes), $fileName);
    }

    public function exportDetailInstructor(Request $request)
    {

        $arr = $request->all();
        unset($arr['_token']);
        $attributes = collect();
        for ($i = 0; $i < sizeof($arr['kelas']); $i++) {
            $attributes->push([
                '#' => $i + 1,
                'Kelas Instruktur' => $arr['kelas'][$i],
                'Jumlah Praktikan' => $arr['jumlah'][$i],
                'Jumlah Kehadiaran' => $arr['kehadiran'][$i],
                'AverageNilaiModul' => $arr['avgModul'][$i],
                'AverageFinalGrade' => $arr['avgUAP'][$i]
            ]);
        }

        //        return $attributes;
        $fileName = $arr['title'] . '.csv';

        return Excel::download(new PraktikumDetailInstructorExport($attributes), $fileName);
    }
}
