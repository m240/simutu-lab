<?php

namespace App\Http\Controllers\Admin;

use App\Exports\EvaluasiModulExport;
use App\Http\Controllers\Controller;
use App\Model\Ilab\ilab_class;
use App\Model\Ilab\ilab_class_category;
use App\Model\Ilab\ilab_task_detail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class EvaluasiModulController extends Controller
{
    public function index()
    {
        $classCategories = $classCategories = ilab_class_category::where('parent', '0')->orderBy('id', 'desc')->get();

        return view('pages.admin.evaluasi_modul', compact('classCategories'));
    }

    public function semester($idx)
    {
        $classCat = ilab_class_category::where('id', $idx)->first();
        $class_list = ilab_class_category::with(['ilab_class' => function ($query) {
            $query->where('deleted_at', null);
        }])->where([
            ['parent', $idx],
            ['is_teori_class', '0']
        ])->get();

        return view('pages.admin.evaluasi_modul_semester', compact('idx', 'classCat', 'class_list'));
    }

    public function category($idx, $idCategory)
    {
        $classes = ilab_class::with([
            'ilab_task' => function ($query) {
                $query->where('deleted_at', null);
                $query->withCount([
                    'ilab_task_detail as modul_1' => function ($query) {
                        $query->select(DB::raw('avg(grade)'))->where([
                            ['grade', '!=', 0],
                            ['title', 'like', '%Modul 1%']
                        ]);
                    },
                    'ilab_task_detail as modul_2' => function ($query) {
                        $query->select(DB::raw('avg(grade)'))->where([
                            ['grade', '!=', 0],
                            ['title', 'like', '%Modul 2%']
                        ]);
                    },
                    'ilab_task_detail as modul_3' => function ($query) {
                        $query->select(DB::raw('avg(grade)'))->where([
                            ['grade', '!=', 0],
                            ['title', 'like', '%Modul 3%']
                        ]);
                    },
                    'ilab_task_detail as modul_4' => function ($query) {
                        $query->select(DB::raw('avg(grade)'))->where([
                            ['grade', '!=', 0],
                            ['title', 'like', '%Modul 4%']
                        ]);
                    },
                    'ilab_task_detail as modul_5' => function ($query) {
                        $query->select(DB::raw('avg(grade)'))->where([
                            ['grade', '!=', 0],
                            ['title', 'like', '%Modul 5%']
                        ]);
                    },
                    'ilab_task_detail as modul_6' => function ($query) {
                        $query->select(DB::raw('avg(grade)'))->where([
                            ['grade', '!=', 0],
                            ['title', 'like', '%Modul 6%']
                        ]);
                    },
                    'ilab_task_detail as final_exam_avg' => function ($query) {
                        $query->select(DB::raw('avg(grade)'))->where([
                            ['grade', '!=', 0],
                            ['final_exam', '1']
                        ]);
                    },
                    'ilab_task_detail as extra_avg' => function ($query) {
                        $query->select(DB::raw('avg(grade)'))->where([
                            ['grade', '!=', 0],
                            ['extra', '1']
                        ]);
                    },
                ]);
            }
        ])->where([
            ['class_category_id', $idCategory],
            ['deleted_at', null]
        ])->get();

        $modul1 = array();
        $modul2 = array();
        $modul3 = array();
        $modul4 = array();
        $modul5 = array();
        $modul6 = array();
        $finalexam = array();
        $extra = array();
        foreach ($classes as $class) {
            $modul1[] = $class->ilab_task->avg('modul_1');
            $modul2[] = $class->ilab_task->avg('modul_2');
            $modul3[] = $class->ilab_task->avg('modul_3');
            $modul4[] = $class->ilab_task->avg('modul_4');
            $modul5[] = $class->ilab_task->avg('modul_5');
            $modul6[] = $class->ilab_task->avg('modul_6');
            $finalexam[] = $class->ilab_task->avg('final_exam_avg');
            $extra[] = $class->ilab_task->avg('extra_avg');
        }

        $classArr = collect();
        $classArr->push([
            array(
                "name" => "Modul 1",
                "value" => count($modul1) == 0 ? 0 : array_sum($modul1) / count($modul1)
            ),
            array(
                "name" => "Modul 2",
                "value" => count($modul2) == 0 ? 0 : array_sum($modul2) / count($modul2)
            ),
            array(
                "name" => "Modul 3",
                "value" => count($modul3) == 0 ? 0 : array_sum($modul3) / count($modul3)
            ),
            array(
                "name" => "Modul 4",
                "value" => count($modul4) == 0 ? 0 : array_sum($modul4) / count($modul4)
            ),
            array(
                "name" => "Modul 5",
                "value" => count($modul5) == 0 ? 0 : array_sum($modul5) / count($modul5)
            ),
            array(
                "name" => "Modul 6",
                "value" => count($modul6) == 0 ? 0 : array_sum($modul6) / count($modul6)
            ),
            array(
                "name" => "Final Exam",
                "value" => count($finalexam) == 0 ? 0 : array_sum($finalexam) / count($finalexam)
            ),
            array(
                "name" => "Extra",
                "value" => count($extra) == 0 ? 0 : array_sum($extra) / count($extra)
            ),
        ]);
        $classArr = $classArr[0];

        $semester = ilab_class_category::where('id', $idx)->first();
        $classCategory = ilab_class_category::where('id', $idCategory)->first();

        // START LOGIC CHARTJS
        $className = array();
        $classValue = array();
        foreach ($classArr as $class){
            $className[] = $class['name'];
            $classValue[] = round($class['value'], 2);
        }
        // END CHARTJS

//        return  $className;

        return view('pages.admin.evaluasi_modul_semester_category', compact('idx', 'semester',
            'classCategory', 'classArr', 'className', 'classValue'));
    }

    public function exportModul(Request $request){

        $arr = $request->all();
        unset($arr['_token']);
        $attributes = collect();
        for ($i = 0; $i < sizeof($arr['name_modul']); $i++) {
            $attributes->push([
                '#' => $i + 1,
                'NamaModul' => $arr['name_modul'][$i],
                'AverageModul' => $arr['value_modul'][$i],
            ]);
        }

        $fileName = $arr['title'] . " " . $arr['semester'] . '.csv';

        return Excel::download(new EvaluasiModulExport($attributes), $fileName);
    }
}
