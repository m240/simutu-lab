<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\data_mengisi_penilaian;
use Illuminate\Http\Request;

class DataMengisiPenilaianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\data_mengisi_penilaian  $data_mengisi_penilaian
     * @return \Illuminate\Http\Response
     */
    public function show(data_mengisi_penilaian $data_mengisi_penilaian)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\data_mengisi_penilaian  $data_mengisi_penilaian
     * @return \Illuminate\Http\Response
     */
    public function edit(data_mengisi_penilaian $data_mengisi_penilaian)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\data_mengisi_penilaian  $data_mengisi_penilaian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, data_mengisi_penilaian $data_mengisi_penilaian)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\data_mengisi_penilaian  $data_mengisi_penilaian
     * @return \Illuminate\Http\Response
     */
    public function destroy(data_mengisi_penilaian $data_mengisi_penilaian)
    {
        //
    }
}
