<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\data_kriteria_processed;
use App\Model\Admin\detail_kriteria;
use App\Model\Admin\periode_penilaian;
use App\Model\Ilab\ilab_user;
use Illuminate\Http\Request;

class FeedbackFasilabController extends Controller
{
    public function idx()
    {
        $data = array();
        $data['periode'] = periode_penilaian::where('processed', "1")->where('jenis_penilaians', "fasil_lab")->get();
        return view('pages.admin.feedback_fasilab_index', compact("data"));
    }

    public function detail($id)
    {
        $data['periode'] = periode_penilaian::find($id);
        $data['detail_kriteria'] = detail_kriteria::where('kriteria_id', $data['periode']->kriteria_id)->get();
        $data['data_mod_proc'] = $data['periode']->data_fasilab_proc()->get()[0];
        $data['nilai'] = array();
        foreach ($data['detail_kriteria'] as $d) {
            array_push($data['nilai'], data_kriteria_processed::where('detail_kriteria_id', $d->id)->where('data_fasilab_processed_id', $data['data_mod_proc']->id)->pluck('total_nilai_kriteria')[0]);
        }
        $data['msg'] = $data['periode']->data_msg_penilaians()->get();
        $data['user'] = array();
        $i = 0;
        $data['nim'] = array();
        foreach ($data['msg'] as $d) {
            array_push($data['user'], ilab_user::whereId($d->ilab_praktikkan_id)->pluck('user_name')[0]);
            array_push($data['nim'], $data['user'][$i]);
            $data['user'][$i] = "https://krs.umm.ac.id/Poto/" . substr($data['user'][$i], 0, 4)  . "/" . $data['user'][$i] . ".JPG";

            $i++;
        }
        return view('pages.admin.feedback_fasilab_detail', compact("data"));
    }
}
