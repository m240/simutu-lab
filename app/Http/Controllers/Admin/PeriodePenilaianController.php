<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\periode_penilaian;
use App\Model\Admin\kriteria;
use App\Model\Admin\predikat;
use App\Model\Ilab\ilab_class_category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\data_fasilab_processed;
use App\Model\Admin\data_kriteria_processed;
use App\Model\Admin\data_modul_processed;
use App\Model\Admin\data_penilaian;
use App\Model\Admin\data_user_processed;
use App\Model\Admin\detail_predikat;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;

class PeriodePenilaianController extends Controller
{

    public function index()
    {
        $data['periode'] = periode_penilaian::where('processed', "0")->orderBy('id', 'desc')->get();

        $data['kriteria']['id'] = kriteria::pluck('id')->sortByDesc('id');
        $data['kriteria']['nama'] = kriteria::pluck('nama_kriteria')->sortByDesc('id');
        $data['temp'] = array();
        foreach ($data['kriteria']['id'] as $d) {
            array_push($data['temp'], $d);
        }
        $data['kriteria']['id'] = $data['temp'];

        $data['temp'] = array();
        $data['predikat']['id'] = predikat::pluck('id')->sortByDesc('id');
        $data['predikat']['nama'] = predikat::pluck('nama_predikat')->sortByDesc('id');
        foreach ($data['predikat']['id'] as $d) {
            array_push($data['temp'], $d);
        }
        $data['predikat']['id'] = $data['temp'];

        $data['temp'] = array();
        $data['class_category']['id'] = ilab_class_category::where(
            [
                ['active', '=', '1'],
                ['is_teori_class', '=', '0' ],
                ['parent', '!=', '0' ],
            ]
        )->pluck('id');
        $data['class_category']['category_name'] = ilab_class_category::where(
            [
                ['active', '=', '1'],
                ['is_teori_class', '=', '0' ],
                ['parent', '!=', '0' ],
            ]
        )->pluck('category_name');
        foreach ($data['class_category']['id'] as $d) {
            array_push($data['temp'], $d);
        }
        $data['class_category']['id'] = $data['temp'];
        return view('pages.admin.periode', compact('data'));
    }

    //fungsi terhubung ke ajax aktifkan periode
    public function aktif(Request $request)
    {
        $period = periode_penilaian::find($request->id);
        $id = $period->id;
        $jenis = $period->jenis_penilaians;
        if ($jenis == "modul") {
            $ilab_class = $period->ilab_class_category_id;
            periode_penilaian::where([
                ['id', '!=', $id],
                ['jenis_penilaians', '=', $jenis],
                ['ilab_class_category_id', '=', $ilab_class]
            ])->update(['active' => 0]);
        } else {
            periode_penilaian::where([
                ['id', '!=', $id],
                ['jenis_penilaians', '=', $jenis]
            ])->update(['active' => 0]);
        }
        $period->update(['active' => 1]);
    }

    //fungsi terhubung ke ajax NONaktifkan periode
    public function nonAktif(Request $request)
    {
        periode_penilaian::find($request->id)->update(
            [
                'active' => 0
            ]
        );
    }

    public function hapus(Request $request)
    {
        periode_penilaian::find($request->id)->delete();
        Session::flash('berhasil', 'Periode telah terhapus');
    }

    public function tambah(Request $request)
    {
        $semester = $request->semester;
        if ($semester == 1) {
            $semester = "ganjil";
        } else {
            $semester = "genap";
        }
        $jenis = $request->jenis_penilaian;
        $title = '';
        $prm = [
            "tahun" => $request->tahun,
            "semester" => $semester,
            "active" => 0,
            "processed" => 0,
            "kriteria_id" => $request->kriteria
        ];
        if ($jenis == 0) {
            $jenis = "asisten";
            $title = "Asisten";
            $nama = $title . ' ' . ucfirst($semester) . ' ' . $request->tahun;
            $prm += ["jenis_penilaians" => $jenis, "nama_penilaian" => $nama, "predikat_id" => $request->predikat];
        } else if ($jenis == 1) {
            $jenis = "instruktur";
            $title = "Instruktur";
            $nama = $title . ' ' . ucfirst($semester) . ' ' . $request->tahun;
            $prm += ["jenis_penilaians" => $jenis, "nama_penilaian" => $nama, "predikat_id" => $request->predikat];
        } else if ($jenis == 2) {
            $jenis = "fasil_lab";
            $title = "Fasilitas Lab.";
            $nama = $title . ' ' . ucfirst($semester) . ' ' . $request->tahun;
            $prm += ["jenis_penilaians" => $jenis, "nama_penilaian" => $nama];
        } else {
            $jenis = "modul";
            $title = "Modul Praktikum";
            $nama = $title . ' ' . ilab_class_category::where('id', $request->classcategory)->first('category_name')->category_name . ' ' . ucfirst($semester) . ' ' . $request->tahun;
            $prm += ["jenis_penilaians" => $jenis, "ilab_class_category_id" => $request->classcategory, "nama_penilaian" => $nama];
        }
        $semester = ilab_class_category::where('parent',0)->orderBy('id','desc')->first();
        $prm["semester_id"] = $semester->id;
        periode_penilaian::insert($prm);
        Session::flash('berhasil', 'Periode Berhasil Ditambahkan.');
        return redirect(route('adm1n_dashboard_periode_idx'));
    }

    public function edit(Request $request)
    {
        // return $request;
        $semester = $request->semester;
        if ($semester == 1) {
            $semester = "ganjil";
        } else {
            $semester = "genap";
        }
        $jenis = $request->jenis_penilaian;
        $title = '';
        $prm = [
            "tahun" => $request->tahun,
            "semester" => $semester,
            "kriteria_id" => $request->kriteria
        ];
        if ($jenis == 0) {
            $jenis = "asisten";
            $title = "Asisten";
            $nama = $title . ' ' . ucfirst($semester) . ' ' . $request->tahun;
            $prm += ["jenis_penilaians" => $jenis, "nama_penilaian" => $nama, "predikat_id" => $request->predikat];
        } else if ($jenis == 1) {
            $jenis = "instruktur";
            $title = "Instruktur";
            $nama = $title . ' ' . ucfirst($semester) . ' ' . $request->tahun;
            $prm += ["jenis_penilaians" => $jenis, "nama_penilaian" => $nama, "predikat_id" => $request->predikat];
        } else if ($jenis == 2) {
            $jenis = "fasil_lab";
            $title = "Fasilitas Lab.";
            $nama = $title . ' ' . ucfirst($semester) . ' ' . $request->tahun;
            $prm += ["jenis_penilaians" => $jenis, "nama_penilaian" => $nama];
        } else {
            $jenis = "modul";
            $title = "Modul Praktikum";
            $nama = $title . ' ' . ilab_class_category::where('id', $request->classcategory)->first('category_name')->category_name . ' ' . ucfirst($semester) . ' ' . $request->tahun;
            $prm += ["jenis_penilaians" => $jenis, "ilab_class_category_id" => $request->classcategory, "nama_penilaian" => $nama];
        }

        periode_penilaian::where('id', $request->id)->update($prm);
        Session::flash('berhasil', 'Berhasil Merubah Periode.');
        return redirect(route('adm1n_dashboard_periode_idx'));
    }

    public function processPeriodePenilaian($id)
    {
        $periode = periode_penilaian::find($id);
        if ($periode->jenis_penilaians == "modul" || $periode->jenis_penilaians == "fasil_lab") {
            if ($periode->jenis_penilaians == "modul") {
                $data_modul = data_modul_processed::create([
                    "total_nilai_modul" => 0,
                    "periode_penilaian_id" => $id
                ]);
            } else {
                $data_modul = data_fasilab_processed::create([
                    "total_nilai_fasilab" => 0,
                    "periode_penilaian_id" => $id
                ]);
            }

            $kriteria = $periode->kriteris()->get();
            $detail_kriteria = $kriteria[0]->detail_kriterias()->get();
            if (count($detail_kriteria) == 0) {
                $data_modul->delete();
                return back()->with('gagal', 'Tidak Dapat Memproses Periode Penilaian Karena Tidak Ada Detail Kriteria.');
            }
            $total_nilai = 0;
            foreach ($detail_kriteria as $d) {
                $nilai = 0;
                $nilai_arr =  data_penilaian::where('periode_penilaian_id', $id)->where('detail_kriteria_id', $d->id)->pluck('nilai');
                if (count($nilai_arr) == 0) {
                    $data_modul->delete();
                    return back()->with('gagal', 'Tidak Dapat Memproses Periode Penilaian Karena Belum Ada Data Penilai.');
                }
                foreach ($nilai_arr as $n) {
                    $nilai += $n;
                }
                $nilai = ($nilai / count($nilai_arr)) * ($d->persentase / 100);
                if ($periode->jenis_penilaians == "modul") {
                    data_kriteria_processed::create([
                        "detail_kriteria_id" => $d->id,
                        "data_modul_processed_id" => $data_modul->id,
                        "total_nilai_kriteria" => $nilai
                    ]);
                } else {
                    data_kriteria_processed::create([
                        "detail_kriteria_id" => $d->id,
                        "data_fasilab_processed_id" => $data_modul->id,
                        "total_nilai_kriteria" => $nilai
                    ]);
                }
                $total_nilai += $nilai;
            }
            if ($periode->jenis_penilaians == "modul") {
                $data_modul->total_nilai_modul = $total_nilai;
            } else {
                $data_modul->total_nilai_fasilab = $total_nilai;
            }
            $data_modul->save();
            periode_penilaian::find($id)->update([
                "active" => 0,
                "processed" => 1
            ]);
            if ($periode->jenis_penilaians == "modul") {
                return redirect(route('admin_feedback_modul_idx'))->with('berhasil', 'Berhasil Memproses Periode Penilaian.');
            } else {
                return redirect(route('admin_feedback_fasilab_idx'))->with('berhasil', 'Berhasil Memproses Periode Penilaian.');
            }
        } else if ($periode->jenis_penilaians == "asisten" || $periode->jenis_penilaians == "instruktur") {
            $kriteria = $periode->kriteris()->get();
            $detail_kriteria = $kriteria[0]->detail_kriterias()->get();
            if (count($detail_kriteria) == 0) {
                return back()->with('gagal', 'Tidak Dapat Memproses Periode Penilaian Karena Tidak Ada Detail Kriteria.');
            }
            $arr_id_asisten = data_penilaian::where('periode_penilaian_id', $id)->distinct('ilab_user_id')->pluck('ilab_user_id');
            if (count($arr_id_asisten) == 0) {
                return back()->with('gagal', 'Tidak Dapat Memproses Periode Penilaian Karena Belum Ada Data Penilai.');
            }
            foreach ($arr_id_asisten as $d) {
                $data_user = data_user_processed::create([
                    'total_nilai_user' => 0,
                    'periode_penilaian_id' => $id,
                    'ilab_user_id' => $d
                ]);
                $total_nilai = 0;
                foreach ($detail_kriteria as $k) {
                    $nilai = 0;
                    $nilai_arr =  data_penilaian::where('periode_penilaian_id', $id)->where('detail_kriteria_id', $k->id)->where('ilab_user_id', $d)->pluck('nilai');

                    foreach ($nilai_arr as $n) {
                        $nilai += $n;
                    }
                    $nilai = ($nilai / count($nilai_arr)) * ($k->persentase / 100);

                    data_kriteria_processed::create([
                        "detail_kriteria_id" => $k->id,
                        "total_nilai_kriteria" => $nilai,
                        "data_user_processed_id" => $data_user->id
                    ]);

                    $total_nilai += $nilai;
                }
                $data_user->total_nilai_user = $total_nilai;
                $detail_predikat = detail_predikat::where('predikat_id', $periode->predikat_id)->get();
                foreach ($detail_predikat as $d) {
                    if ($d->min_nilai <= $total_nilai && $d->max_nilai >= $total_nilai) {
                        $data_user->detail_predikat_id = $d->id;
                    }
                }
                $data_user->save();
            }
            periode_penilaian::find($id)->update([
                "active" => 0,
                "processed" => 1
            ]);
            return redirect(route('admin_feedback_asisten_instruktur_idx'))->with('berhasil', 'Berhasil Memproses Periode Penilaian.');
        }
    }

    public function nonAktifSebagian(Request $request)
    {
        periode_penilaian::whereIn('id', $request->data)->update([
            "active" => 0
        ]);
        return "ok";
    }

    public function aktifSebagian(Request $request)
    {
        foreach ($request->data as $r) {
            $period = periode_penilaian::find($r);
            $id = $period->id;
            $jenis = $period->jenis_penilaians;
            if ($jenis == "modul") {
                $ilab_class = $period->ilab_class_category_id;
                periode_penilaian::where([
                    ['id', '!=', $id],
                    ['jenis_penilaians', '=', $jenis],
                    ['ilab_class_category_id', '=', $ilab_class]
                ])->update(['active' => 0]);
            } else {
                periode_penilaian::where([
                    ['id', '!=', $id],
                    ['jenis_penilaians', '=', $jenis]
                ])->update(['active' => 0]);
            }
            $period->update(['active' => 1]);
        }
        return "ok";
    }

    public function get_temp_comment($id){
        $periods = periode_penilaian::where('id',$id)->with(['data_msg_penilaians','data_msg_penilaians.ilab_praktikkan'])->get()[0];
        return view('pages.admin.feedback_comment_temp',compact('periods'));
    }
}
