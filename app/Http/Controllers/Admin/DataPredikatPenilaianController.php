<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Model\Admin\data_predikat_penilaian;
use Illuminate\Http\Request;

class DataPredikatPenilaianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\data_predikat_penilaian  $data_predikat_penilaian
     * @return \Illuminate\Http\Response
     */
    public function show(data_predikat_penilaian $data_predikat_penilaian)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\data_predikat_penilaian  $data_predikat_penilaian
     * @return \Illuminate\Http\Response
     */
    public function edit(data_predikat_penilaian $data_predikat_penilaian)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\data_predikat_penilaian  $data_predikat_penilaian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, data_predikat_penilaian $data_predikat_penilaian)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\data_predikat_penilaian  $data_predikat_penilaian
     * @return \Illuminate\Http\Response
     */
    public function destroy(data_predikat_penilaian $data_predikat_penilaian)
    {
        //
    }
}
