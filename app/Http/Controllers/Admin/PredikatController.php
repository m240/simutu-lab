<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\predikat;
use App\Model\Admin\periode_penilaian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class PredikatController extends Controller
{
    public function idx()
    {
        $data = array();
        $data['predikat'] = predikat::all()->sortByDesc('id');
        $data['jml_detail'] = array();
        $data['jml_periode'] = array();
        $i = 0;
        foreach ($data['predikat'] as $d) {
            $data['jml_detail'][$i] = $d->detail_predikats()->count();
            $data['jml_periode'][$i] = $d->periode_penilaians()->count();
            $i++;
        }
        return view('pages.admin.predikat', compact('data'));
    }

    public function tambah(Request $request)
    {
        predikat::insert([
            "nama_predikat" => $request->nama
        ]);
        Session::flash('berhasil', 'Predikat Berhasil Ditambahkan.');
        return redirect(route('adm1n_dashboard_predikat'));
    }

    public function edit(Request $request)
    {
        predikat::where('id', $request->id)->update([
            "nama_predikat" => $request->nama
        ]);
        Session::flash('berhasil', 'Predikat Berhasil Diedit.');
        return redirect(route('adm1n_dashboard_predikat'));
    }

    public function hapus(Request $request)
    {
        $temp = predikat::where('id', $request->id)->first()->detail_predikats()->pluck('logo');
        foreach ($temp as $t) {
            File::delete($t);
        }
        predikat::where('id', $request->id)->delete();
        Session::flash('berhasil','Predikat telah terhapus');
    }
}
