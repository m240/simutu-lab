<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\data_kriteria_processed;
use App\Model\Admin\detail_kriteria;
use App\Model\Admin\periode_penilaian;
use App\Model\Ilab\ilab_class_category;
use App\Model\Ilab\ilab_user;

class FeedbackModulController extends Controller
{
    // urut atas ke bawah
    
    public function index()
    {
        $periodes_id = periode_penilaian::where('processed', "1")->where('jenis_penilaians', "modul")->get()->pluck('semester_id');
        $classCategories = ilab_class_category::where('parent',"0")->whereIn("id", $periodes_id)->get();
        return view('pages.admin.feedback_modul_semester', compact("classCategories"));
    }

    public function idx($semester_id)
    {
        $data = array();
        $data['periode'] = periode_penilaian::where('processed', "1")->where('jenis_penilaians', "modul")->where("semester_id", $semester_id)->get();
        $data['periode_proc'] = array();
        foreach ($data['periode'] as $d) {
            array_push($data['periode_proc'], $d->data_modul_proc()->get()[0]);
        }
        $semester = ilab_class_category::find($semester_id);
        return view('pages.admin.feedback_modul_index', compact("data","semester"));
    }

    public function detail($semester_id, $id)
    {
        $data['periode'] = periode_penilaian::find($id);
        $data['detail_kriteria'] = detail_kriteria::where('kriteria_id', $data['periode']->kriteria_id)->get();
        $data['data_mod_proc'] = $data['periode']->data_modul_proc()->get()[0];
        $data['nilai'] = array();
        foreach ($data['detail_kriteria'] as $d) {
            array_push($data['nilai'], data_kriteria_processed::where('detail_kriteria_id', $d->id)->where('data_modul_processed_id', $data['data_mod_proc']->id)->pluck('total_nilai_kriteria')[0]);
        }
        $data['msg'] = $data['periode']->data_msg_penilaians()->get();
        $data['user'] = array();
        $i = 0;
        $data['nim'] = array();
        foreach ($data['msg'] as $d) {
            array_push($data['user'], ilab_user::whereId($d->ilab_praktikkan_id)->pluck('user_name')[0]);
            array_push($data['nim'], $data['user'][$i]);
            $data['user'][$i] = "https://krs.umm.ac.id/Poto/" . substr($data['user'][$i], 0, 4)  . "/" . $data['user'][$i] . ".JPG";

            $i++;
        }
        $semester = ilab_class_category::find($semester_id);
        return view('pages.admin.feedback_modul_detail', compact("data","semester"));
    }
}
