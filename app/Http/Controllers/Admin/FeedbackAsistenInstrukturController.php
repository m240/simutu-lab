<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\data_kriteria_processed;
use App\Model\Admin\data_user_processed;
use App\Model\Admin\detail_kriteria;
use App\Model\Admin\detail_predikat;
use App\Model\Admin\periode_penilaian;
use App\Model\Ilab\ilab_user;
use Illuminate\Http\Request;

class FeedbackAsistenInstrukturController extends Controller
{
    public function idx()
    {
        $data = array();
        $data['periode'] = periode_penilaian::where('processed', "1")->whereIn('jenis_penilaians', ["asisten", "instruktur"])->get();
        // return $data;
        return view('pages.admin.feedback_asisten_instruktur_index', compact("data"));
    }

    public function detail($id)
    {
        $data = array();
        $data['periode'] = periode_penilaian::find($id);
        $data['data_user'] = $data['periode']->data_user_proc()->get();

        $data['user_info'] = array();
        $i = 0;
        foreach ($data['data_user'] as $d) {
            array_push($data['user_info'], ilab_user::whereId($d->ilab_user_id)->first(['full_name', 'user_name']));
            $data['user_info'][$i]->user_name = "https://krs.umm.ac.id/Poto/" . substr($data['user_info'][$i]->user_name, 0, 4)  . "/" . $data['user_info'][$i]->user_name . ".JPG";
            $i++;
        }
        $data['predikat'] = detail_predikat::where('predikat_id', $data['periode']->predikat_id)->pluck('nama_predikat', 'id');
        // return $data;
        return view('pages.admin.feedback_asisten_instruktur_detail', compact("data"));
    }

    public function detailuser($id, $usr)
    {
        $data = array();
        $data['periode'] = periode_penilaian::find($id);
        $data['detail_kriteria'] = detail_kriteria::where('kriteria_id', $data['periode']->kriteria_id)->get();
        $data['nilai'] = array();
        foreach ($data['detail_kriteria'] as $d) {
            array_push($data['nilai'], data_kriteria_processed::where('detail_kriteria_id', $d->id)->where('data_user_processed_id', $usr)->pluck('total_nilai_kriteria')[0]);
        }
        $data['data_user_proc'] = data_user_processed::find($usr);
        $data['msg'] = $data['periode']->data_msg_penilaians()->where('ilab_user_id', $data['data_user_proc']->ilab_user_id)->get();
        $data['user'] = array();
        $i = 0;
        $data['nim'] = array();
        foreach ($data['msg'] as $d) {
            array_push($data['user'], ilab_user::whereId($d->ilab_praktikkan_id)->pluck('user_name')[0]);
            array_push($data['nim'], $data['user'][$i]);
            $data['user'][$i] = "https://krs.umm.ac.id/Poto/" . substr($data['user'][$i], 0, 4)  . "/" . $data['user'][$i] . ".JPG";

            $i++;
        }
        $data['usernya'] = ilab_user::whereId($data['data_user_proc']->ilab_user_id)->pluck('full_name')[0];
        // return $data;
        return view('pages.admin.feedback_asisten_instruktur_detail_user', compact("data"));
    }
}
