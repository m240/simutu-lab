<?php

namespace App\Http\Controllers\Admin;

use App\Exports\EvaluasiSemesterExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class EvaluasiSemesterController extends Controller
{
    public function index()
    {
        // TAHUN UNTUK FILTER
        $year = array();
        foreach (\App\Model\Ilab\ilab_class_category::where('parent', 0)
            ->where('is_teori_class', 0)
            ->get() as $category) {
            $q = explode('-', explode(' ', $category->category_name)[2])[0];
            if (!in_array($q, $year)) {
                array_push($year, $q);
            }
        }
        sort($year);
        // END TAHUN UNTUK FILTER

        // TAHUN AJAR
        $subYear = \Carbon\Carbon::now()->subYears(1)->format('Y');
        $semester = \App\Model\Ilab\ilab_class_category::where('parent', 0)
            ->where('category_name', 'like', '%' . \Carbon\Carbon::now()->format('Y') . '%')
            ->where('is_teori_class', 0)
            ->orWhere('category_name', 'like', 'Semester Ganjil' . $subYear . '%')
            ->orWhere('category_name', 'like', 'Semester Genap' . $subYear . '%')
            ->get();
        // END TAHUN AJAR

        // TOTAL PRAKTIKUM
        $total_praktikum = array();
        foreach ($semester as $category) {
            $q = \App\Model\Ilab\ilab_class_category::where('deleted_at', null)
                ->where('parent', $category->id)
                ->where('is_teori_class', 0)
                ->count();
            array_push($total_praktikum, $q);
        }
        // END TOTAL PRAKTIKUM

        // RATA-RATA NILAI PRAKTIKUM MODUL & UAP SELURUH KELAS
        $avgModul = array();
        $avgUap = array();
        $countModul = array();
        $countUap = array();
        $total_mhs_modul = array();
        $total_mhs_uap = array();
        $idx = 0;
        foreach ($semester as $category) {
            array_push($countModul, 0);
            array_push($total_mhs_modul, 0);
            array_push($countUap, 0);
            array_push($total_mhs_uap, 0);
            $class = \App\Model\Ilab\ilab_class_category::where('deleted_at', null)
                ->where('parent', $category->id)
                ->where('is_teori_class', 0)
                ->get();
            if ($class != null) {
                foreach ($class as $classes) {
                    $detail_class = \App\Model\Ilab\ilab_class::where('deleted_at', null)
                        ->where('class_category_id', $classes->id)
                        ->get();
                    foreach ($detail_class as $detail_classes) {
                        $task = \App\Model\Ilab\ilab_task::where('deleted_at', null)
                            ->where('class_id', $detail_classes->id)
                            ->where('final_exam', 0)
                            ->where('extra', 0)
                            ->get();
                        $taskUap = \App\Model\Ilab\ilab_task::where('deleted_at', null)
                            ->where('class_id', $detail_classes->id)
                            ->where('final_exam', 1)
                            ->get();

                        // AVGMODUL
                        foreach ($task as $tasks) {
                            $detail_task = \App\Model\Ilab\ilab_task_detail::where('deleted_at', null)
                                ->where('task_id', $tasks->id)
                                ->where('grade', '!=', 0)
                                ->get();
                            foreach ($detail_task as $detail_tasks) {
                                $countModul[$idx] += $detail_tasks->grade;
                                $total_mhs_modul[$idx]++;
                            }
                        }

                        // AVGUAP
                        foreach ($taskUap as $tasks) {
                            $detail_task = \App\Model\Ilab\ilab_task_detail::where('deleted_at', null)
                                ->where('task_id', $tasks->id)
                                ->where('grade', '!=', 0)
                                ->get();
                            foreach ($detail_task as $detail_tasks) {
                                $countUap[$idx] += $detail_tasks->grade;
                                $total_mhs_uap[$idx]++;
                            }
                        }
                    }
                }
            }
            // SAVE COUNT avgMODUL & avgUAP
            if ($countModul[$idx] != null || $total_mhs_modul[$idx] != null) {
                array_push($avgModul, number_format($countModul[$idx] / $total_mhs_modul[$idx], 2));
            } else {
                array_push($avgModul, 'Nilai modul tidak ditemukan');
            }

            if ($countUap[$idx] != null || $total_mhs_uap[$idx] != null) {
                array_push($avgUap, number_format($countUap[$idx] / $total_mhs_uap[$idx], 2));
            }{
                array_push($avgUap, 'Nilai UAP tidak ditemukan');
            }
            $idx++;
        }
        // END RATA-RATA NILAI PRAKTIKUM MODUL & UAP SELURUH KELAS

        Session::flash('filterStart', $subYear);
        Session::flash('filterEnd', \Carbon\Carbon::now()->format('Y'));

        return view('pages.admin.evaluasi_semester', compact('semester', 'year', 'total_praktikum', 'avgModul', 'avgUap'));
    }

    public function filter(Request $request)
    {
        // TAHUN UNTUK FILTER
        $year = array();
        foreach (\App\Model\Ilab\ilab_class_category::where('parent', 0)
            ->where('is_teori_class', 0)
            ->get() as $category) {
            $q = explode('-', explode(' ', $category->category_name)[2])[0];
            if (!in_array($q, $year)) {
                array_push($year, $q);
            }
        }
        sort($year);
        // END TAHUN UNTUK FILTER

        // TAHUN AJAR & PROSES FILTER
        $semester = \App\Model\Ilab\ilab_class_category::where('parent', 0)
            ->where('is_teori_class', 0)
            ->get();

        $z = array();
        foreach ($semester as $x) {
            $explode = explode('-', explode(' ', $x->category_name)[2])[0];
            for ($i = $request->start; $i <= $request->end; $i++) {
                if ($explode == $i) {
                    array_push($z, $x);
                }
            }
        }
        $semester = $z;
        // END TAHUN AJAR & PROSES FILTER

        // TOTAL PRAKTIKUM
        $total_praktikum = array();
        foreach ($semester as $category) {
            $q = \App\Model\Ilab\ilab_class_category::where('deleted_at', null)
                ->where('parent', $category->id)
                ->where('is_teori_class', 0)
                ->count();
            array_push($total_praktikum, $q);
        }
        // END TOTAL PRAKTIKUM

        // RATA-RATA NILAI PRAKTIKUM MODUL & UAP SELURUH KELAS
        $avgModul = array();
        $avgUap = array();
        $countModul = array();
        $countUap = array();
        $total_mhs_modul = array();
        $total_mhs_uap = array();
        $idx = 0;
        foreach ($semester as $category) {
            array_push($countModul, 0);
            array_push($total_mhs_modul, 0);
            array_push($countUap, 0);
            array_push($total_mhs_uap, 0);
            $class = \App\Model\Ilab\ilab_class_category::where('deleted_at', null)
                ->where('parent', $category->id)
                ->where('is_teori_class', 0)
                ->get();
            if ($class != null) {
                foreach ($class as $classes) {
                    $detail_class = \App\Model\Ilab\ilab_class::where('deleted_at', null)
                        ->where('class_category_id', $classes->id)
                        ->get();
                    foreach ($detail_class as $detail_classes) {

                        $task = \App\Model\Ilab\ilab_task::where('deleted_at', null)
                            ->where('class_id', $detail_classes->id)
                            ->where('final_exam', 0)
                            ->where('extra', 0)
                            ->get();

                        $taskUap = \App\Model\Ilab\ilab_task::where('deleted_at', null)
                            ->where('class_id', $detail_classes->id)
                            ->where('final_exam', 1)
                            ->get();

                        // AVGMODUL
                        foreach ($task as $tasks) {
                            $detail_task = \App\Model\Ilab\ilab_task_detail::where('deleted_at', null)
                                ->where('task_id', $tasks->id)
                                ->where('grade', '!=', 0)
                                ->get();
                            foreach ($detail_task as $detail_tasks) {
                                $countModul[$idx] += $detail_tasks->grade;
                                $total_mhs_modul[$idx]++;
                            }
                        }

                        // AVGUAP
                        foreach ($taskUap as $tasks) {
                            $detail_task = \App\Model\Ilab\ilab_task_detail::where('deleted_at', null)
                                ->where('task_id', $tasks->id)
                                ->where('grade', '!=', 0)
                                ->get();
                            foreach ($detail_task as $detail_tasks) {
                                $countUap[$idx] += $detail_tasks->grade;
                                $total_mhs_uap[$idx]++;
                            }
                        }

                    }
                }
            }

            // SAVE COUNT avgMODUL & avgUAP
            if ($countModul[$idx] != null && $total_mhs_modul[$idx] != null) {
                array_push($avgModul, number_format($countModul[$idx] / $total_mhs_modul[$idx], 2));
            } else {
                array_push($avgModul, 'Nilai modul tidak ditemukan');
            }

            if ($countUap[$idx] != null && $total_mhs_uap[$idx] != null) {
                array_push($avgUap, number_format($countUap[$idx] / $total_mhs_uap[$idx], 2));
            } else {
                array_push($avgUap, 'Nilai UAP tidak ditemukan');
            }
            $idx++;

        }
        // END RATA-RATA NILAI PRAKTIKUM MODUL & UAP SELURUH KELAS

        Session::flash('filterStart', $request->start);
        Session::flash('filterEnd', $request->end);

        return view('pages.admin.evaluasi_semester', compact('semester', 'year', 'total_praktikum', 'avgModul', 'avgUap'));
    }

    public function export(Request $request)
    {
        if($request->semester==null){
            return redirect()->back()->with('failed', 'Data tidak ada');
        }

        // ARRAY TO COLLECTION
        $arr = $request->all();
        unset($arr['_token']);
        $attributes = collect();
        for ($i = 0; $i < sizeof($arr['semester']); $i++) {
            $attributes->push([
                '#' => $i + 1,
                'semester' => $arr['semester'][$i],
                'total_praktikum' => $arr['total_praktikum'][$i],
                'avgModul' => $arr['avgModul'][$i],
                'avgUap' => $arr['avgUap'][$i],
            ]);
        }
        // END ARRAY TO COLLECTION

        return Excel::download(new EvaluasiSemesterExport($attributes), 'Evaluasi Semester.csv');
    }

}
