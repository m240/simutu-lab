<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Model\Admin\kriteria;
use App\Model\Admin\detail_kriteria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DetailKriteriaController extends Controller
{
    public function index($id)
    {
        $data = array();
        $data['id'] = $id;
        $data['kriteria'] = kriteria::find($id);
        $data['detail'] = $data['kriteria']->detail_kriterias()->get();
        $data['persentase'] = 0;
        foreach ($data['detail'] as $d) {
            $data['persentase'] += $d->persentase;
        }
        $data['persentase'] = 100 - $data['persentase'];
        // $data['jml_kriteria'] = count($data['kriteria']);
        // $data['jml_penilaian'] = $data['periode']->data_penilaians()->count();
        // $data['jml_pengisi'] = $data['periode']->data_mengisi_penilaians()->count();
        return view('pages.admin.detail_kriteria', compact('data'));
    }

    public function tambah($id, Request $request)
    {
        detail_kriteria::insert([
            'desc' => $request->desc,
            'persentase' => $request->pers,
            'kriteria_id' => $id
        ]);

        Session::flash('berhasil', 'Berhasil Menambahkan Detail Kriteria.');
        return redirect('/adm1n/dashboard/kriteria/detail/' . $id);
    }

    public function hapus(Request $request)
    {
        detail_kriteria::find($request->id)->delete();
        Session::flash('berhasil', 'Berhasil Menghapus Detail Kriteria.');
        return back();
    }

    public function edit($id, Request $request)
    {
        detail_kriteria::find($id)->update([
            "desc" => $request->desc
        ]);
        Session::flash('berhasil', 'Berhasil Mengubah Detail Kriteria.');
        return back();
    }
}
