<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Model\Admin\kriteria;
use App\Model\Admin\periode_penilaian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class KriteriaController extends Controller
{
    public function index()
    {
        $data['kriteria'] = kriteria::all()->sortByDesc('id');
        $data['jml_detail'] = array();
        $data['jml_periode'] = array();
        $i = 0;
        foreach ($data['kriteria'] as $d) {
            $data['jml_detail'][$i] = $d->detail_kriterias()->count();
            $data['jml_periode'][$i] = $d->periode_penilaians()->count();
            $i++;
        }
        return view('pages.admin.kriteria', compact('data'));
    }

    public function tambah(Request $request)
    {
        kriteria::insert([
            "nama_kriteria" => $request->nama
        ]);
        Session::flash('berhasil', 'Kriteria Berhasil Ditambahkan.');
        return redirect(route('adm1n_dashboard_kriteria'));
    }

    public function edit(Request $request)
    {
        kriteria::where('id', $request->id)->update([
            "nama_kriteria" => $request->nama
        ]);
        Session::flash('berhasil', 'Kriteria Berhasil Diedit.');
        return redirect(route('adm1n_dashboard_kriteria'));
    }

    public function hapus(Request $request)
    {
        kriteria::where('id', $request->id)->delete();
        Session::flash('berhasil','Kriteria telah terhapus');
    }
}
