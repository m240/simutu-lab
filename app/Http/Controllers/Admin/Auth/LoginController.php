<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Model\Ilab\ilab_user;
use App\Model\Ilab\ilab_user_admin;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */

    public function index(){
        if (Auth::guard('admin')->check()) {
            return redirect()->route('adm1n.dashboard.index');
        }
        return view('pages.admin.login');
    }

    public function login(Request $request)
    {
        $username = $request->input('username');
        $password = sha1(md5($request->input('password')."asjkdfh")."shsadfgkj");
        $ilab_user = ilab_user::where('user_name', $username)->first();
        if($ilab_user == null){
            return redirect()->back()->with('status','Username or Password is wrong!');
        }
        $user_admin = $ilab_user->ilab_user_admin()->first();
        if($ilab_user == null || $user_admin == null){
            return redirect()->back()->with('status','Username or Password is wrong!');
        }
        if($ilab_user->password == $password && $user_admin->section=='Super Admin'){
            Auth::guard('admin')->login($user_admin);
            return redirect()->route('adm1n.dashboard.index');
        }else{
            return redirect()->back()->with('status','Username or Password is wrong!');
        }
    }

    public function logout(){
        Auth::guard('admin')->logout();
        return view('pages.admin.login');
    }
}