<?php

namespace App\Http\Controllers\Admin;

use App\Exports\PraktikumAllAssistantExport;
use App\Exports\PraktikumDetailAssistantExport;
use App\Http\Controllers\Controller;
use App\Model\Admin\periode_penilaian;
use App\Model\Ilab\ilab_class_category;
use App\Model\Ilab\ilab_user_assistant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class PraktikumAsistenController extends Controller
{
    public function index()
    {
        $classCategories = ilab_class_category::where('parent', '0')->orderBy('id', 'desc')->get();

//        return $classCategories;

        return view('pages.admin.praktikum_periode_asisten_index', compact('classCategories'));
    }

    public function show($idx)
    {
        $index = $idx;

        $selectPeriode = ilab_class_category::find($index);
        $selectPeriode = $selectPeriode->category_name;

        $selectSemester = strtoupper(explode(' ', $selectPeriode)[1]);
        $selectTahunMulai = explode('-', explode(' ', $selectPeriode)[2])[0];
        $selectTahunAkhir = explode('-', explode(' ', $selectPeriode)[2])[1];

        $startRekap = '';
        $endRekap = '';
        $selectTahun = '';
        if ($selectSemester == 'GANJIL') {
            $startRekap = $selectTahunMulai . "-09-01";
            $endRekap = $selectTahunMulai + 1 . "-01-31";
            $selectTahun = $selectTahunMulai;
        } else {
            $startRekap = $selectTahunAkhir . "-02-01";
            $endRekap = $selectTahunAkhir . "-08-31";
            $selectTahun = $selectTahunAkhir;
        }

        $allAssistants = ilab_user_assistant::with([
            'ilab_user.ilab_presence_assistant_instructor' => function ($query) use ($selectTahun, $startRekap, $endRekap) {
//                $query->whereYear('created_at', $selectTahun);
                $query->whereBetween('created_at', [$startRekap, $endRekap]);
                $query->withCount(['ilab_presence' => function ($query) use ($selectTahun, $startRekap, $endRekap) {
                    $query->where('deleted_at', null);
                    $query->whereBetween('created_at', [$startRekap, $endRekap]);
                }]);
            },
            'ilab_task_detail' => function ($query) use ($selectTahun, $startRekap, $endRekap) {
                $query->with(['ilab_task' => function ($query) {
                    $query->where('extra', 0);
                    $query->where('final_exam', 0);
                }]);
                $query->where('grade', '!=', 0);
//                $query->whereYear('created_at', $selectTahun);
                $query->whereBetween('created_at', [$startRekap, $endRekap]);
            },
            'ilab_user_assistant_class' => function ($query) use ($selectTahun, $startRekap, $endRekap) {
                $query->where('deleted_at', null);
//                $query->whereYear('created_at', $selectTahun);
                $query->whereBetween('created_at', [$startRekap, $endRekap]);
            },
        ])->orderBy('created_at', 'desc')->get();


        $assistantPresence = array();
        $assistantName = array();

        foreach ($allAssistants as $assistant) {
            if (count($assistant->ilab_user_assistant_class) > 0) {
                $assistantPresence[] = round(count($assistant->ilab_user->ilab_presence_assistant_instructor
                        ->where('ilab_presence_count', '!=', 0)) / count($assistant->ilab_user_assistant_class), 2);

                $assistantName[] = $assistant->ilab_user->full_name;

            }
        }

        // Merge name and result
        $arrayCombine = array();
        foreach ($assistantName as $i => $ass) {
            $arrayCombine[] = array("result" => $assistantPresence[$i], "name" => $assistantName[$i]);
        }

        // Sort
        $arrayCombineAsc = $this->bubbleSortAsc($arrayCombine);
        $arrayCombineDesc = $this->bubbleSortDesc($arrayCombine);

        // Split Array Ascending
        $nameAsc = array();
        $resultAsc = array();
        foreach ($arrayCombineAsc as $i => $key) {
            if ($i <= 5) {
                $resultAsc[] = $arrayCombineAsc[$i]['result'];
                $nameAsc[] = $arrayCombineAsc[$i]['name'];
            }
        }

        // Split Array Descending
        $nameDesc = array();
        $resultDesc = array();
        foreach ($arrayCombineDesc as $i => $key) {
            if ($i <= 5) {
                $resultDesc[] = $arrayCombineDesc[$i]['result'];
                $nameDesc[] = $arrayCombineDesc[$i]['name'];
            }
        }

        return view('pages.admin.praktikum_periode_asisten_detail_index', compact('index', 'allAssistants',
            'selectTahun', 'selectSemester', 'nameAsc', 'resultAsc', 'nameDesc', 'resultDesc'));
    }

    function bubbleSortAsc($arr)
    {
        $n = sizeof($arr);

        for ($i = 0; $i < $n; $i++) {
            for ($j = 0; $j < $n - $i - 1; $j++) {
                if ($arr[$j] < $arr[$j + 1]) {
                    $t = $arr[$j];
                    $arr[$j] = $arr[$j + 1];
                    $arr[$j + 1] = $t;
                }
            }
        }
        return $arr;
    }

    function bubbleSortDesc($arr)
    {
        $n = sizeof($arr);

        for ($i = 0; $i < $n; $i++) {
            for ($j = 0; $j < $n - $i - 1; $j++) {
                if ($arr[$j] > $arr[$j + 1]) {
                    $t = $arr[$j];
                    $arr[$j] = $arr[$j + 1];
                    $arr[$j + 1] = $t;
                }
            }
        }
        return $arr;
    }

    public function detail($idxperiode, $idxasisten, $idxuser)
    {
        $periode = periode_penilaian::where('jenis_penilaians', '=', 'asisten')->orderBy('tahun', 'desc')->find($idxperiode);

        $selectPeriode = ilab_class_category::find($idxperiode);
        $selectPeriode = $selectPeriode->category_name;

        $selectSemester = strtoupper(explode(' ', $selectPeriode)[1]);
        $selectTahunMulai = explode('-', explode(' ', $selectPeriode)[2])[0];
        $selectTahunAkhir = explode('-', explode(' ', $selectPeriode)[2])[1];

        $startRekap = '';
        $endRekap = '';
        $selectTahun = '';
        if ($selectSemester == 'GANJIL') {
            $startRekap = $selectTahunMulai . "-09-01";
            $endRekap = $selectTahunMulai + 1 . "-01-31";
            $selectTahun = $selectTahunMulai;
        } else {
            $startRekap = $selectTahunAkhir . "-02-01";
            $endRekap = $selectTahunAkhir . "-08-31";
            $selectTahun = $selectTahunAkhir;
        }

        $detailAssistant = ilab_user_assistant::with([
            'ilab_user',
            'ilab_user_assistant_class.ilab_class.ilab_task' => function ($query) use ($idxasisten) {
                $query->withCount(['ilab_task_detail' => function ($query) use ($idxasisten) {
                    $query->where('grade', '!=', 0);
                    $query->where('user_assistant_id', $idxasisten);
                }])->where('extra', '==', 0);
                $query->withCount(['ilab_task_detail as avg_modul' => function ($query) use ($idxasisten) {
                    $query->select(DB::raw('avg(grade)'));
                    $query->where('grade', '!=', 0);
                    $query->where('user_assistant_id', $idxasisten);
                }]);
            },
            'ilab_user_assistant_class' => function ($query) use ($selectTahun, $startRekap, $endRekap) {
                $query->where('deleted_at', null);
                $query->whereYear('created_at', $selectTahun);
                $query->whereBetween('created_at', [$startRekap, $endRekap]);
            },
            'ilab_user_assistant_class.ilab_class.ilab_presence' => function ($query) use ($idxuser) {
                $query->where('deleted_at', null);
                $query->withCount(['ilab_presence_assistant_instructor' => function ($query) use ($idxuser) {
                    $query->where('user_id', $idxuser);
                }]);
            },
        ])->find($idxasisten);

//        return $detailAssistant;

        $avgModul = array();
        $avgUAP = array();
        $assistantClass = array();

        foreach ($detailAssistant->ilab_user_assistant_class as $class) {
            $assistantClass[] = $class->ilab_class->full_name;
            $avgModul[] = round($class->ilab_class->ilab_task->where('final_exam', 0)->avg('avg_modul'), 2);
            $avgUAP[] = round($class->ilab_class->ilab_task->where('final_exam', 1)->avg('avg_modul'), 2);
        }

        // dd($detailAssistant);

        return view('pages.admin.praktikum_periode_asisten_detail_user_index', compact('idxperiode',
            'detailAssistant', 'assistantClass', 'avgModul', 'avgUAP'));
    }

    public function exportAllAssistant(Request $request)
    {

        $arr = $request->all();
        unset($arr['_token']);
        $attributes = collect();
        for ($i = 0; $i < sizeof($arr['nim']); $i++) {
            $attributes->push([
                '#' => $i + 1,
                'NIM' => $arr['nim'][$i],
                'Asisten' => $arr['nama'][$i],
                'Jumlah Asistensi Kelas' => $arr['jumlah'][$i],
                'Average Nilai Modul' => $arr['avgModul'][$i],
                'Average Kehadiran' => $arr['avgKehadiran'][$i]
            ]);
        }

        $fileName = 'Rekap Asisten ' . $arr['title'] . '.csv';

        return Excel::download(new PraktikumAllAssistantExport($attributes), $fileName);
    }

    public function exportDetailAssistant(Request $request)
    {

        $arr = $request->all();
        unset($arr['_token']);
        $attributes = collect();
        for ($i = 0; $i < sizeof($arr['kelas']); $i++) {
            $attributes->push([
                '#' => $i + 1,
                'KelasAsisten' => $arr['kelas'][$i],
                'JumlahAsistensi' => $arr['jumlahasistensi'][$i],
                'AverageKehadiran' => $arr['jumlahkehadiran'][$i],
                'AverageNilaiModul' => $arr['avgModul'][$i],
                'AverageFinalGrade' => $arr['avgFinalGrade'][$i]
            ]);
        }

//        return $attributes;
        $fileName = $arr['title'] . '.csv';

        return Excel::download(new PraktikumDetailAssistantExport($attributes), $fileName);
    }
}
