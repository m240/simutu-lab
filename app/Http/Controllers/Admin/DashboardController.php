<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\Admin;
use App\Model\Admin\data_fasilab_processed;
use App\Model\Admin\data_kriteria_processed;
use App\Model\Admin\data_modul_processed;
use App\Model\Admin\periode_penilaian;
use App\Model\Ilab\ilab_class_category;
use App\Model\Ilab\ilab_user_assistant;
use App\Model\Ilab\ilab_user_instructor;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Request;

class DashboardController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */

    public function index()
    {
        $stats = [];
        $stats['modul'] = periode_penilaian::where('jenis_penilaians', 'modul')->count();
        $stats['asisten'] = periode_penilaian::where('jenis_penilaians', 'asisten')->count();
        $stats['instruktur'] = periode_penilaian::where('jenis_penilaians', 'instruktur')->count();
        $stats['lab'] = periode_penilaian::where('jenis_penilaians', 'fasil_lab')->count();
        $vis_lab = ['kriteria' => [], 'nilai' => []];
        $lab = periode_penilaian::where([
            ['jenis_penilaians', '=', 'fasil_lab'],
            ['processed', '=', '1'],
        ])->orderBy('id', 'desc')->first();
        if ($lab != null) {
            $vis_lab['kriteria'] = $lab->kriteris()->first()->detail_kriterias()->get()->pluck('desc');
            $kriterias = $lab->kriteris()->first()->detail_kriterias()->get();
            foreach ($kriterias as $d) {
                $nilai = data_kriteria_processed::where('detail_kriteria_id', $d->id)->where('data_fasilab_processed_id', $lab->data_fasilab_proc->id)->pluck('total_nilai_kriteria')[0];
                array_push($vis_lab['nilai'], 100 * $nilai / $d->persentase);
            }
        }
        $asistant_timeline = $this->get_presence_assistant();
        $instructor_timeline = $this->get_presence_instructor();
        $modul_timeline = $this->get_timeline_modul_lab();
        $timeline = $this->get_timeline_grade_lab();
        $modul = $this->modul_grade();
        return view('pages.admin.dashboard', [
            'stats' => $stats,
            'vis_lab' => $vis_lab,
            'timeline' => $timeline,
            'modul_timeline' => $modul_timeline,
            'instructor_timeline' => $instructor_timeline,
            'asistant_timeline' => $asistant_timeline,
            'modul' => $modul,
        ]);
    }

    public function get_presence_assistant()
    {
        $ilab_classes = ilab_class_category::where('parent', 0)->orderBy('id', 'asc')->take(8)->latest()->get();
        $array_recap = array();
        foreach ($ilab_classes as $class_period) {
            $selectPeriode = $class_period;
            $selectPeriode = $class_period->category_name;
            $selectSemester = strtoupper(explode(' ', $selectPeriode)[1]);
            $selectTahunMulai = explode('-', explode(' ', $selectPeriode)[2])[0];
            $selectTahunAkhir = explode('-', explode(' ', $selectPeriode)[2])[1];

            $startRekap = '';
            $endRekap = '';
            $selectTahun = '';
            if ($selectSemester == 'GANJIL') {
                $startRekap = $selectTahunMulai . "-09-01";
                $endRekap = $selectTahunMulai + 1 . "-01-31";
                $selectTahun = $selectTahunMulai;
            } else {
                $startRekap = $selectTahunAkhir . "-02-01";
                $endRekap = $selectTahunAkhir . "-08-31";
                $selectTahun = $selectTahunAkhir;
            }

            $allAssistants = ilab_user_assistant::with([
                'ilab_user.ilab_presence_assistant_instructor' => function ($query) use ($selectTahun, $startRekap, $endRekap) {
                    //                $query->whereYear('created_at', $selectTahun);
                    $query->whereBetween('created_at', [$startRekap, $endRekap]);
                    $query->withCount(['ilab_presence' => function ($query) use ($startRekap, $endRekap) {
                        $query->where('deleted_at', null);
                        $query->whereBetween('created_at', [$startRekap, $endRekap]);
                    }]);
                },
                'ilab_user_assistant_class' => function ($query) use ($startRekap, $endRekap) {
                    $query->where('deleted_at', null);
                    //                $query->whereYear('created_at', $selectTahun);
                    $query->whereBetween('created_at', [$startRekap, $endRekap]);
                },
            ])->orderBy('created_at', 'desc')->get();
            $assistantPresence = array();
            foreach ($allAssistants as $assistant) {
                if (count($assistant->ilab_user_assistant_class) > 0) {
                    $assistantPresence[] = round(count($assistant->ilab_user->ilab_presence_assistant_instructor
                        ->where('ilab_presence_count', '!=', 0)) / count($assistant->ilab_user_assistant_class), 2);
                }
            }
            if(count($assistantPresence)==0){
                $array_recap[$class_period->category_name] = 0;
            }else{
                $array_recap[$class_period->category_name] = array_sum($assistantPresence)/count($assistantPresence);
            }
        }
        return $array_recap;
    }

    public function get_presence_instructor(){
        $ilab_classes = ilab_class_category::where('parent', 0)->orderBy('id', 'asc')->take(8)->latest()->get();
        $array_recap = array();
        foreach ($ilab_classes as $class_period) {
            $selectPeriode = $class_period;
            $selectPeriode = $class_period->category_name;
            $selectSemester = strtoupper(explode(' ', $selectPeriode)[1]);
            $selectTahunMulai = explode('-', explode(' ', $selectPeriode)[2])[0];
            $selectTahunAkhir = explode('-', explode(' ', $selectPeriode)[2])[1];

            $startRekap = '';
            $endRekap = '';
            $selectTahun = '';
            if ($selectSemester == 'GANJIL') {
                $startRekap = $selectTahunMulai . "-09-01";
                $endRekap = $selectTahunMulai + 1 . "-01-31";
                $selectTahun = $selectTahunMulai;
            } else {
                $startRekap = $selectTahunAkhir . "-02-01";
                $endRekap = $selectTahunAkhir . "-08-31";
                $selectTahun = $selectTahunAkhir;
            }

            $allInstructor = ilab_user_instructor::with([
                'ilab_user.ilab_presence_assistant_instructor' => function ($query) use ($selectTahun, $startRekap, $endRekap) {
                    //                $query->whereYear('created_at', $selectTahun);
                    $query->whereBetween('created_at', [$startRekap, $endRekap]);
                    $query->withCount(['ilab_presence' => function ($query) use ($startRekap, $endRekap) {
                        $query->where('deleted_at', null);
                        $query->whereBetween('created_at', [$startRekap, $endRekap]);
                    }]);
                },
                'ilab_class' => function ($query) use ($startRekap, $endRekap) {
                    // $query->where('deleted_at', null);
                },
            ])->orderBy('created_at', 'desc')->get();
            // dd($allInstructor);
            $instructorPresence = array();
            foreach ($allInstructor as $instructor) {
                if (count($instructor->ilab_class) > 0) {
                    $instructorPresence[] = round(count($instructor->ilab_user->ilab_presence_assistant_instructor) / count($instructor->ilab_class), 2);
                }
            }
            $instructorPresence = array_filter($instructorPresence, function ($v, $k){
                return $v != 0.0;
            }, ARRAY_FILTER_USE_BOTH);
            if(count($instructorPresence)==0){
                $array_recap[$class_period->category_name] = 0;
            }else{
                $array_recap[$class_period->category_name] = array_sum($instructorPresence)/count($instructorPresence)*4.5;
            }
        }
        return $array_recap;
    }

    public function get_timeline_modul_lab()
    {
        $end_y = date('Y');
        $start_y = $end_y - 3;
        $periodes = periode_penilaian::where(
            [
                ['jenis_penilaians', '=', 'modul'],
                ['processed', '=', '1']
            ]
        )->whereBetween('tahun', [$start_y, $end_y])->with(['data_modul_proc'])
            ->orderBy('tahun', 'asc')->get();
        $periodes_processed = [];
        foreach ($periodes as $period) {
            $key = $period->tahun . ' ' . $period->semester;
            $grade_v = $period->data_modul_proc->total_nilai_modul;
            if (array_key_exists($key, $periodes_processed)) {
                if ($grade_v != 0) {
                    $periodes_processed[$key] = ($periodes_processed[$key] + $grade_v) / 2;
                }
            } else {
                $periodes_processed[$key] = $grade_v;
            }
        }
        return $periodes_processed;
    }

    public function get_timeline_grade_lab()
    {
        $fasil_lab = data_fasilab_processed::orderBy('id', 'desc')->take(10)->get();
        $period = array();
        foreach ($fasil_lab as $lab) {
            array_push($period, $lab->periode_penilaian()->nama_penilaian);
        }
        $timeline = [
            'grade' => $fasil_lab->pluck('total_nilai_fasilab')->toArray(),
            'period' => $period
        ];
        return $timeline;
    }

    public function setting()
    {
        $admins = Admin::find(1);
        return view('pages.admin.settings', compact('admins'));
    }

    public function setting_submit(Request $request)
    {
        $user = Admin::find(1);
        // $pass_req = $request->input('password');
        // if (Hash::check($pass_req, $user->password)) {

        // }else{
        //     return redirect(route('adm1n.dashboard.setting'))->with('status','Konfirmasi Password Salah');
        // }
        $user->update([
            'desc' => $request->input('desc'),
            'email' => $request->input('email'),
            'nomor_wa' => $request->input('nomor_wa'),
            'nomor_telp' => $request->input('nomor_telp'),
        ]);
        return redirect(route('adm1n.dashboard.setting'))->with('status', 'Update berhasil dilakukan');
    }

    public function modul_grade()
    {
        $new_tahun = periode_penilaian::where([
            ['jenis_penilaians', '=', 'modul'],
            ['processed', '=', '1'],
        ])->orderBy('tahun', 'desc')->first();
        $most_hight = [];
        $most_low = [];
        if ($new_tahun != NULL) {
            $new_tahun = $new_tahun->tahun;
            $period = periode_penilaian::where([
                ['tahun', '=', $new_tahun],
                ['jenis_penilaians', '=', 'modul'],
                ['processed', '=', '1'],
            ])->get()->pluck('id')->toArray();
            $most_hight = data_modul_processed::whereIn('periode_penilaian_id', $period)->orderBy('total_nilai_modul', 'desc')->take(5)->get();
            $most_low = data_modul_processed::whereIn('periode_penilaian_id', $period)->orderBy('total_nilai_modul', 'asc')->take(5)->get();
        }
        return [
            'high' => $most_hight,
            'low' => $most_low
        ];
    }

    // public function password_submit(Request $request){
    //     $user = Admin::find(1);
    //     $pass = $request->input('password');
    //     if (Hash::check($pass, $user->password)) {
    //         $user->update([
    //             'password'=>  Hash::make($request->input('password_new')),
    //         ]);
    //         return redirect(route('adm1n.dashboard.setting'))->with('status','Update Password berhasil dilakukan');
    //     }else{
    //         return redirect(route('adm1n.dashboard.setting'))->with('status','Konfirmasi Password Lama Salah');
    //     }
    // }
}
