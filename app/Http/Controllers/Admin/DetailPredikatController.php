<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\predikat;
use App\Model\Admin\detail_predikat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;

class DetailPredikatController extends Controller
{
    public function index_detail($id)
    {
        $data = array();
        $data['predikat'] = predikat::find($id);
        $data['detail'] = $data['predikat']->detail_predikats()->get();
        $data['id'] = $id;
        return view('pages.admin.detail_predikat', compact('data'));
    }

    public function hapus_detail(Request $request)
    {
        File::delete(detail_predikat::find($request->id)->logo);
        detail_predikat::find($request->id)->delete();
    }

    public function tambah_idx($id)
    {
        $data['id'] = $id;
        return view('pages.admin.detail_predikat_tambah_edit', compact('data'));
    }

    public function tambah_detail($id, Request $request)
    {
        $minmax = explode(";", $request->range);
        Session::flash('desc', $request->desc);
        Session::flash('nama', $request->nama);
        Session::flash('min', $minmax[0]);
        Session::flash('max', $minmax[1]);

        $validatedData = $request->validate([
            'logo' => 'required|mimes:jpeg,png,jpg|max:2048'
        ]);

        if ($minmax[0] == $minmax[1]) {
            Session::flash('desc', $request->desc);
            Session::flash('nama', $request->nama);
            Session::flash('min', $minmax[0]);
            Session::flash('max', $minmax[1]);
            Session::flash('gagal', 'Batas Nilai Tidak Boleh Sama.');
            return back();
        }
        $id_predikat = detail_predikat::insertGetId([
            "desc" => $request->desc,
            "nama_predikat" => $request->nama,
            "max_nilai" => $minmax[1],
            "min_nilai" => $minmax[0],
            "logo" => "temp",
            "predikat_id" => $id
        ]);

        $file = $request->file('logo');
        $nama_file =  $id . '_logo_' . $id_predikat . '.' . $file->getClientOriginalExtension();
        $tujuan_upload = 'media/photos/logo/';
        $lengkap = $tujuan_upload . $nama_file;
        $file->move($tujuan_upload, $nama_file);

        detail_predikat::find($id_predikat)->update([
            "logo" => $lengkap
        ]);
        Session::flash('berhasil', 'Predikat Berhasil Ditambahkan.');
        return redirect('/adm1n/dashboard/predikat/detail/' . $id);
    }

    //id_P = id_predikat
    //id_C = id_detail_predikat
    public function edit_idx($id_P, $id_C)
    {
        $data['id'] = $id_P;
        $data['detail'] = detail_predikat::find($id_C);

        return view('pages.admin.detail_predikat_tambah_edit', compact('data'));
    }

    //id_P = id_predikat
    //id_C = id_detail_predikat
    public function edit_detail($id_P, $id_C, Request $request)
    {
        $minmax = explode(";", $request->range);
        if ($minmax[0] == $minmax[1]) {
            Session::flash('gagal', 'Batas Nilai Tidak Boleh Sama.');
            return back();
        }

        if ($request->file('logo') == NULL) {
            detail_predikat::find($id_C)->update([
                "desc" => $request->desc,
                "nama_predikat" => $request->nama,
                "max_nilai" => $minmax[1],
                "min_nilai" => $minmax[0]
            ]);
        } else {
            $validatedData = $request->validate([
                'logo' => 'required|mimes:jpeg,png,jpg|max:2048'
            ]);
            File::delete(detail_predikat::find($id_C)->logo);
            $file = $request->file('logo');
            $nama_file =  $id_P . '_logo_' . $id_C . '.' . $file->getClientOriginalExtension();
            $tujuan_upload = 'media/photos/logo/';
            $lengkap = $tujuan_upload . $nama_file;
            $file->move($tujuan_upload, $nama_file);

            detail_predikat::find($id_C)->update([
                "desc" => $request->desc,
                "nama_predikat" => $request->nama,
                "max_nilai" => $minmax[1],
                "min_nilai" => $minmax[0],
                "logo" => $lengkap
            ]);
        }
        Session::flash('berhasil', 'Predikat Berhasil Diedit.');
        return redirect('/adm1n/dashboard/predikat/detail/' . $id_P);
    }
}
