<?php

namespace App\Http\Controllers\Admin;

use App\Exports\EvaluasiKelasExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Excel;

class EvaluasiKelasController extends Controller
{
    public function index()
    {
        $all_semester = \App\Model\Ilab\ilab_class_category::where('parent', 0)->where('deleted_at', null)->orderby('id')->get();
        foreach ($all_semester as $data) {
            $id = $data->id;
        }
        $class = \App\Model\Ilab\ilab_class_category::all();
        $semester = \App\Model\Ilab\ilab_class_category::where('id', $id)->get();
        Session::flash('filter', $id);
        return view('pages.admin.evaluasi_kelas', compact('class', 'semester', 'all_semester'));
    }

    public function filter(Request $request)
    {
        $all_semester = \App\Model\Ilab\ilab_class_category::where('parent', 0)->orderby('id')->get();
        $semester = \App\Model\Ilab\ilab_class_category::where('id', $request->filter)->get();
        $class = \App\Model\Ilab\ilab_class_category::all();
        Session::flash('filter', $request->filter);
        return view('pages.admin.evaluasi_kelas', compact('class', 'semester', 'all_semester'));
    }

    public function post($id)
    {
        $kelas_praktikum = \App\Model\Ilab\ilab_class::where('class_category_id', $id)
            ->where('deleted_at', null)
            ->get();

        $barsChart = [
            0 => 0,
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
            7 => 0,
        ];
        $pieChart = array();

        // COUNT BARS CHART
        $jumlah_mhs = [
            0 => 0,
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
            7 => 0,
        ];

        foreach ($kelas_praktikum as $class) {
            $modul = 0;
            foreach (\App\Model\Ilab\ilab_task::where('deleted_at', null)->where('class_id', $class->id)->where('final_exam', 0)->where('extra', 0)->get() as $task) {
                $count = 0;
                foreach (\App\Model\Ilab\ilab_task_detail::where('deleted_at', null)->where('task_id', $task->id)->where('grade', '!=', '0')->get() as $task_details) {
                    $count += $task_details->grade;
                    $jumlah_mhs[$modul] += 1;
                }
                $barsChart[$modul++] += $count;
            }
        }

        for ($i = 0; $i < sizeof($barsChart); $i++) {
            if ($barsChart[$i] != 0 && $jumlah_mhs[$i] != 0) {
                $barsChart[$i] = number_format($barsChart[$i] / $jumlah_mhs[$i], 2);
            }
        }
        // END COUNT BARS CHART

        // COUNT PIE CHART
        foreach ($kelas_praktikum as $class) {
            $count = 0;
            $jumlah_mhs = 0;
            $class_task = \App\Model\Ilab\ilab_task::where('deleted_at', null)->where('class_id', $class->id)->where('final_exam', 1)->get();
            if ($class_task->isNotEmpty()) {
                foreach ($class_task as $task) {
                    foreach (\App\Model\Ilab\ilab_task_detail::where('deleted_at', null)->where('task_id', $task->id)->where('grade', '!=', '0')->get() as $task_details) {
                        $count += $task_details->grade;
                        $jumlah_mhs++;
                    }
                }
                array_push($pieChart, number_format($count / $jumlah_mhs, 2));
            } else {
                array_push($pieChart, 0);
            }
        }
        // END COUNT PIE CHART

        // PENGAMPU
        $pengampu = array();
        foreach ($kelas_praktikum as $data) {
            if ($data->user_instructor_id != null) {
                $q = \App\Model\Ilab\ilab_user::where('id', \App\Model\Ilab\ilab_user_instructor::where('id', $data->user_instructor_id)->first()->user_id)->get();
                array_push($pengampu, $q[0]->full_name);
            } else {
                array_push($pengampu, "Dosen pengampu tidak ditemukan");
            }
        }
        // END PENGAMPU

        // TOTAL PRAKTIKAN
        $total_praktikan = array();
        foreach ($kelas_praktikum as $data) {
            $q = \App\Model\Ilab\ilab_user_student_class::where('class_id', $data->id)->where('deleted_at', null)->count();
            array_push($total_praktikan, $q);
        }
        // END TOTAL PRAKTIKAN

        // RATA-RATA NILAI SEMUA MODUL
        $avgModul = array();
        foreach ($kelas_praktikum as $data) {
            $count = 0;
            $jumlah_mhs = 0;
            $task = \App\Model\Ilab\ilab_task::where('class_id', $data->id)->where('deleted_at', null)->where('final_exam', 0)->where('extra', 0)->get();
            foreach ($task as $data2) {
                $task_details = \App\Model\Ilab\ilab_task_detail::where('deleted_at', null)->where('task_id', $data2->id)->where('grade', '!=', '0')->get();
                foreach ($task_details as $data3) {
                    $count += $data3->grade;
                    $jumlah_mhs++;
                }
            }
            if ($jumlah_mhs != 0) {
                array_push($avgModul, number_format($count / $jumlah_mhs, 2));
            } else {
                array_push($avgModul, 'Nilai modul tidak ditemukan');
            }
        }
        //  END RATA-RATA NILAI SEMUA MODUL

        // RATA-RATA UAP
        $avgUap = array();
        foreach ($kelas_praktikum as $data) {
            $count = 0;
            $jumlah_mhs = 0;
            $task = \App\Model\Ilab\ilab_task::where('class_id', $data->id)->where('deleted_at', null)->where('final_exam', 1)->where('extra', 0)->get();
            foreach ($task as $data2) {
                $task_details = \App\Model\Ilab\ilab_task_detail::where('deleted_at', null)->where('task_id', $data2->id)->where('grade', '!=', '0')->get();
                foreach ($task_details as $data3) {
                    $count += $data3->grade;
                    $jumlah_mhs++;
                }
            }
            if ($jumlah_mhs != 0) {
                array_push($avgUap, number_format($count / $jumlah_mhs, 2));
            } else {
                array_push($avgUap, 'Nilai UAP tidak ditemukan');
            }
        }
        //
        

        Session::PUT('kelas_praktikum', $kelas_praktikum);
        Session::PUT('pengampu', $pengampu);
        Session::PUT('total_praktikan', $total_praktikan);
        Session::PUT('avgModul', $avgModul);
        Session::PUT('avgUap', $avgUap);

        return view('pages.admin.evaluasi_kelas_detail', compact('kelas_praktikum', 'pengampu', 'total_praktikan', 'avgModul', 'avgUap', 'barsChart', 'pieChart'));
    }

    public function export()
    {
        if (Session('kelas_praktikum')) {
            $tahun_ajaran = \App\Model\Ilab\ilab_class_category::where('id', Session('kelas_praktikum')['0']->class_category_id)
            ->first();
            $tahun_ajaran = \App\Model\Ilab\ilab_class_category::where('id', $tahun_ajaran->parent)
            ->get()['0']->category_name;

            $attributes = collect();
            for ($i = 0; $i < sizeof(Session('kelas_praktikum')); $i++) {
                $attributes->push([
                    'no' => $i + 1,
                    'tahun_ajaran' => $tahun_ajaran,
                    'kelas_praktikum' => Session('kelas_praktikum')[$i]->full_name,
                    'dosen_pengampu' => Session('pengampu')[$i],
                    'total_praktikan' => Session('total_praktikan')[$i],
                    'avgModul' => Session('avgModul')[$i],
                    'avgUap' => Session('avgUap')[$i],
                ]);
            }
        } else {
            return redirect()->back()->with('failed', 'Session habis :(');
        }

        return Excel::download(new EvaluasiKelasExport($attributes), 'Evaluasi Kelas.csv');
    }
}
