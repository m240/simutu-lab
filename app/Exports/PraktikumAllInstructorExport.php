<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PraktikumAllInstructorExport implements FromCollection, WithHeadings
{
    //
    protected $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        return $this->request;
    }

    public function headings(): array
    {
        return [
            'No',
            'Instruktur',
            'Jumlah Kelas Diampu',
            'Rata-Rata Kehadiran',
        ];
    }
}
