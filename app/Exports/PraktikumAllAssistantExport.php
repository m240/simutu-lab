<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PraktikumAllAssistantExport implements FromCollection, WithHeadings
{
    //
    protected $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        return $this->request;
    }

    public function headings(): array
    {
        return [
            'No',
            'NIM',
            'Asisten',
            'Jumlah Asistensi Kelas',
            'Rata-Rata Nilai Modul',
            'Rata-Rata Kehadiran',
        ];
    }
}
