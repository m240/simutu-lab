<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EvaluasiSemesterExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        return $this->request;
    }

    public function headings(): array
    {
        return [
            'No',
            'Tahun Ajaran',
            'Total Praktikum',
            'Rata-Rata Nilai Modul Seluruh Kelas',
            'Rata-Rata Nilai UAP Seluruh Kelas',
        ];
    }
}
