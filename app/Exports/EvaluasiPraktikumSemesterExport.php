<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EvaluasiPraktikumSemesterExport implements FromCollection, WithHeadings
{
    //
    protected $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        return $this->request;
    }

    public function headings(): array
    {
        return [
            'No',
            'Kategori',
            'Jumlah Kelas',
            'Rata-Rata Nilai Modul',
            'Rata-Rata Nilai UAP',
        ];
    }
}
