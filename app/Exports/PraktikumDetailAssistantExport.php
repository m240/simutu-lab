<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PraktikumDetailAssistantExport implements FromCollection, WithHeadings
{
    //
    protected $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        return $this->request;
    }

    public function headings(): array
    {
        return [
            'No',
            'Kelas Asisten',
            'Jumlah Asistensi',
            'Jumlah Kehadiran',
            'Rata-Rata Nilai Modul',
            'Rata-Rata Final Grade',
        ];
    }
}
